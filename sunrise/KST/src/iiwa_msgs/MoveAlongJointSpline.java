package iiwa_msgs;

public interface MoveAlongJointSpline extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/MoveAlongJointSpline";
  static final java.lang.String _DEFINITION = "# Goal\niiwa_msgs/JointSpline joint_spline\n\n---\n# Result\nbool success\nstring error\n\n---\n# Feedback\n";
}
