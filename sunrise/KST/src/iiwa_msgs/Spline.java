package iiwa_msgs;

public interface Spline extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/Spline";
  static final java.lang.String _DEFINITION = "# The describes a motion along a spline\n\nSplineSegment[] segments";
  java.util.List<iiwa_msgs.SplineSegment> getSegments();
  void setSegments(java.util.List<iiwa_msgs.SplineSegment> value);
}
