package iiwa_msgs;

public interface SetWorkpiece extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetWorkpiece";
  static final java.lang.String _DEFINITION = "# This service allows to attach a workpiece to the robots tool.\n# Setting a new workpiece automatically detaches the old one.\n\n# Id of Sunrise workpiece template. Leave emtpy to detach current workpiece.\nstring workpiece_id\n---\nbool success\nstring error";
}
