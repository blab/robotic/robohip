package iiwa_msgs;

public interface SetSmartServoLinSpeedLimitsResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetSmartServoLinSpeedLimitsResponse";
  static final java.lang.String _DEFINITION = "\nbool success\nstring error";
  boolean getSuccess();
  void setSuccess(boolean value);
  java.lang.String getError();
  void setError(java.lang.String value);
}
