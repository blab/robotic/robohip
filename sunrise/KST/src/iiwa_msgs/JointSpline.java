package iiwa_msgs;

public interface JointSpline extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointSpline";
  static final java.lang.String _DEFINITION = "# The describes a motion along a spline in joint space\n\nJointPosition[] segments\n";
  java.util.List<iiwa_msgs.JointPosition> getSegments();
  void setSegments(java.util.List<iiwa_msgs.JointPosition> value);
}
