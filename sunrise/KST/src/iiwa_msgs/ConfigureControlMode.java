package iiwa_msgs;

public interface ConfigureControlMode extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/ConfigureControlMode";
  static final java.lang.String _DEFINITION = "# This service allows to set a control mode at runtime.\n# The user can select control modes defined in iiwa_msgs/ControlMode and set the respective parameters accordingly to the selected mode.\n\nint32 control_mode\niiwa_msgs/JointImpedanceControlMode joint_impedance\niiwa_msgs/CartesianImpedanceControlMode cartesian_impedance\niiwa_msgs/DesiredForceControlMode desired_force\niiwa_msgs/SinePatternControlMode sine_pattern\niiwa_msgs/CartesianControlModeLimits limits\n---\nbool success\nstring error";
}
