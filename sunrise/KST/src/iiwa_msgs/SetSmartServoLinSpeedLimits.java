package iiwa_msgs;

public interface SetSmartServoLinSpeedLimits extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetSmartServoLinSpeedLimits";
  static final java.lang.String _DEFINITION = "# Translational and rotational speed in m/s and rad/s\ngeometry_msgs/Twist max_cartesian_velocity\n\n---\n\nbool success\nstring error\n";
}
