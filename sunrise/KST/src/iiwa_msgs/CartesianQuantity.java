package iiwa_msgs;

public interface CartesianQuantity extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianQuantity";
  static final java.lang.String _DEFINITION = "float32 x\nfloat32 y\nfloat32 z\nfloat32 a\nfloat32 b\nfloat32 c";
  float getX();
  void setX(float value);
  float getY();
  void setY(float value);
  float getZ();
  void setZ(float value);
  float getA();
  void setA(float value);
  float getB();
  void setB(float value);
  float getC();
  void setC(float value);
}
