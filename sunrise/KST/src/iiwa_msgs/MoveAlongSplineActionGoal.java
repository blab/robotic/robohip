package iiwa_msgs;

public interface MoveAlongSplineActionGoal extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/MoveAlongSplineActionGoal";
  static final java.lang.String _DEFINITION = "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\nHeader header\nactionlib_msgs/GoalID goal_id\niiwa_msgs/MoveAlongSplineGoal goal";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  actionlib_msgs.GoalID getGoalId();
  void setGoalId(actionlib_msgs.GoalID value);
  iiwa_msgs.MoveAlongSplineGoal getGoal();
  void setGoal(iiwa_msgs.MoveAlongSplineGoal value);
}
