package iiwa_msgs;

public interface ConfigureControlModeResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/ConfigureControlModeResponse";
  static final java.lang.String _DEFINITION = "bool success\nstring error";
  boolean getSuccess();
  void setSuccess(boolean value);
  java.lang.String getError();
  void setError(java.lang.String value);
}
