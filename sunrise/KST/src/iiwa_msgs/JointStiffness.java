package iiwa_msgs;

public interface JointStiffness extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointStiffness";
  static final java.lang.String _DEFINITION = "Header header\nJointQuantity stiffness";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.JointQuantity getStiffness();
  void setStiffness(iiwa_msgs.JointQuantity value);
}
