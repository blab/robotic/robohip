package iiwa_msgs;

public interface JointPosition extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointPosition";
  static final java.lang.String _DEFINITION = "Header header\nJointQuantity position";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.JointQuantity getPosition();
  void setPosition(iiwa_msgs.JointQuantity value);
}
