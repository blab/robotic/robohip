package iiwa_msgs;

public interface CartesianWrench extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianWrench";
  static final java.lang.String _DEFINITION = "# Cartesian Wrench\nHeader header\ngeometry_msgs/Wrench wrench\ngeometry_msgs/Wrench inaccuracy\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  geometry_msgs.Wrench getWrench();
  void setWrench(geometry_msgs.Wrench value);
  geometry_msgs.Wrench getInaccuracy();
  void setInaccuracy(geometry_msgs.Wrench value);
}
