package iiwa_msgs;

public interface SetWorkpieceResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetWorkpieceResponse";
  static final java.lang.String _DEFINITION = "bool success\nstring error";
  boolean getSuccess();
  void setSuccess(boolean value);
  java.lang.String getError();
  void setError(java.lang.String value);
}
