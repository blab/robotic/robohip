package iiwa_msgs;

public interface MoveAlongSpline extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/MoveAlongSpline";
  static final java.lang.String _DEFINITION = "# Goal\nSpline spline\n\n---\n# Result\nbool success\nstring error\n\n---\n# Feedback\n";
}
