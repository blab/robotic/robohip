package iiwa_msgs;

public interface SplineSegment extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SplineSegment";
  static final java.lang.String _DEFINITION = "# This message describes a segment of a spline path\n\nint32 SPL = 0\nint32 LIN =  1\nint32 CIRC = 2\n\n# The type of the spline segment\nint32 type\n\n# The endpoint of the current segment\nCartesianPose point\n\n# Auxiliary point. Only used for circular segments\nCartesianPose point_aux";
  static final int SPL = 0;
  static final int LIN = 1;
  static final int CIRC = 2;
  int getType();
  void setType(int value);
  iiwa_msgs.CartesianPose getPoint();
  void setPoint(iiwa_msgs.CartesianPose value);
  iiwa_msgs.CartesianPose getPointAux();
  void setPointAux(iiwa_msgs.CartesianPose value);
}
