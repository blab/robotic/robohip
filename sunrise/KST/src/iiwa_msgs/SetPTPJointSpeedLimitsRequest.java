package iiwa_msgs;

public interface SetPTPJointSpeedLimitsRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetPTPJointSpeedLimitsRequest";
  static final java.lang.String _DEFINITION = "# This service allows to set relative joint velocity and acceleration of the robot.\n# Set a parameter to -1 to ignore it\n\n# Relative velocity of the individual joints, 0.0 < value <= 1\nfloat64 joint_relative_velocity\n\n# Relative acceleration of the individual joints, 0.0 < value <= 1.0\nfloat64 joint_relative_acceleration\n\n";
  double getJointRelativeVelocity();
  void setJointRelativeVelocity(double value);
  double getJointRelativeAcceleration();
  void setJointRelativeAcceleration(double value);
}
