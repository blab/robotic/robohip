package iiwa_msgs;

public interface TimeToDestinationResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/TimeToDestinationResponse";
  static final java.lang.String _DEFINITION = "float64 remaining_time";
  double getRemainingTime();
  void setRemainingTime(double value);
}
