package iiwa_msgs;

public interface JointImpedanceControlMode extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointImpedanceControlMode";
  static final java.lang.String _DEFINITION = "# Stiffness values in [Nm/rad]. Stiffness values must be >= 0. \nJointQuantity joint_stiffness\n\n# Damping values. Damping values must be between 0 and 1. \nJointQuantity joint_damping\n";
  iiwa_msgs.JointQuantity getJointStiffness();
  void setJointStiffness(iiwa_msgs.JointQuantity value);
  iiwa_msgs.JointQuantity getJointDamping();
  void setJointDamping(iiwa_msgs.JointQuantity value);
}
