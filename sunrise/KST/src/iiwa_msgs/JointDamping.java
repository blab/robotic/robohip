package iiwa_msgs;

public interface JointDamping extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointDamping";
  static final java.lang.String _DEFINITION = "Header header\nJointQuantity damping";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.JointQuantity getDamping();
  void setDamping(iiwa_msgs.JointQuantity value);
}
