package iiwa_msgs;

public interface ControlMode extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/ControlMode";
  static final java.lang.String _DEFINITION = "int32 POSITION_CONTROL = 0\nint32 JOINT_IMPEDANCE =  1\nint32 CARTESIAN_IMPEDANCE = 2\nint32 DESIRED_FORCE = 3\nint32 SINE_PATTERN = 4";
  static final int POSITION_CONTROL = 0;
  static final int JOINT_IMPEDANCE = 1;
  static final int CARTESIAN_IMPEDANCE = 2;
  static final int DESIRED_FORCE = 3;
  static final int SINE_PATTERN = 4;
}
