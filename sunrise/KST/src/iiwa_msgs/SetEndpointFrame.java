package iiwa_msgs;

public interface SetEndpointFrame extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetEndpointFrame";
  static final java.lang.String _DEFINITION = "# This service allows to select the frame that is used for cartesian positioning\n\n# Id of Sunrise frame\nstring frame_id\n\n---\n\nbool success\nstring error";
}
