package iiwa_msgs;

public interface JointPositionVelocity extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointPositionVelocity";
  static final java.lang.String _DEFINITION = "Header header\nJointQuantity position\nJointQuantity velocity";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.JointQuantity getPosition();
  void setPosition(iiwa_msgs.JointQuantity value);
  iiwa_msgs.JointQuantity getVelocity();
  void setVelocity(iiwa_msgs.JointQuantity value);
}
