package iiwa_msgs;

public interface CartesianPose extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianPose";
  static final java.lang.String _DEFINITION = "# Target Pose including redundancy information.\ngeometry_msgs/PoseStamped poseStamped\n\n# If you have issues with the robot not executing the motion copy this value from the Cartesian Position Tab of the\n# robot SmartPad. Set both parameters to -1 to disable them.\nRedundancyInformation redundancy\n";
  geometry_msgs.PoseStamped getPoseStamped();
  void setPoseStamped(geometry_msgs.PoseStamped value);
  iiwa_msgs.RedundancyInformation getRedundancy();
  void setRedundancy(iiwa_msgs.RedundancyInformation value);
}
