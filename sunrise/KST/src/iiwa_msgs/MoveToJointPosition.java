package iiwa_msgs;

public interface MoveToJointPosition extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/MoveToJointPosition";
  static final java.lang.String _DEFINITION = "# Goal\niiwa_msgs/JointPosition joint_position\n\n---\n# Result\nbool success\nstring error\n\n---\n# Feedback\n";
}
