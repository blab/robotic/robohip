package iiwa_msgs;

public interface SetPTPCartesianSpeedLimitsRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetPTPCartesianSpeedLimitsRequest";
  static final java.lang.String _DEFINITION = "# This service allows to set the speed limits for cartesian PTP motions.\n# Set the parameters you do not want to set to -1 to ignore them.\n\n# Maximum translational speed in m/s\nfloat64 maxCartesianVelocity\n\n# Maximum rotational speed in rad/s\nfloat64 maxOrientationVelocity\n\n# Maximum translational acceleration in m/s^2\nfloat64 maxCartesianAcceleration\n\n# Maximum rotational acceleration in rad/s^2\nfloat64 maxOrientationAcceleration\n\n# Maximum allowed translational jerk in m/s^3\nfloat64 maxCartesianJerk\n\n# Maximum allowed rotational jerk in rad/s^3\nfloat64 maxOrientationJerk\n\n";
  double getMaxCartesianVelocity();
  void setMaxCartesianVelocity(double value);
  double getMaxOrientationVelocity();
  void setMaxOrientationVelocity(double value);
  double getMaxCartesianAcceleration();
  void setMaxCartesianAcceleration(double value);
  double getMaxOrientationAcceleration();
  void setMaxOrientationAcceleration(double value);
  double getMaxCartesianJerk();
  void setMaxCartesianJerk(double value);
  double getMaxOrientationJerk();
  void setMaxOrientationJerk(double value);
}
