package iiwa_msgs;

public interface SetEndpointFrameRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetEndpointFrameRequest";
  static final java.lang.String _DEFINITION = "# This service allows to select the frame that is used for cartesian positioning\n\n# Id of Sunrise frame\nstring frame_id\n\n";
  java.lang.String getFrameId();
  void setFrameId(java.lang.String value);
}
