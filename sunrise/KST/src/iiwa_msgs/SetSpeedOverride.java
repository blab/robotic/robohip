package iiwa_msgs;

public interface SetSpeedOverride extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetSpeedOverride";
  static final java.lang.String _DEFINITION = "# Override the velocity factor for all motions (relative between 0.0 and 1.0)\nfloat64 override_reduction\n\n---\n\nbool success\nstring error";
}
