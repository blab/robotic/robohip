package iiwa_msgs;

public interface CartesianControlModeLimits extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianControlModeLimits";
  static final java.lang.String _DEFINITION = "# Sets the maximum cartesian deviation accepted. If the deviation is exceeded a stop condition occurs. \n# [x, y, z] in [mm]. Precondition: value > 0.0.\n# [a, b, c] in [rad]. Precondition: value > 0.0.\nCartesianQuantity max_path_deviation\n\n# The maximum control force parameter.\n# [x, y, z] in [N]. Precondition: value > 0.0.\n# [a, b, c] in [Nm]. Precondition: value > 0.0.\nCartesianQuantity max_control_force\n\n# Indicates whether a stop condition is fired if the maximum control force is exceeded. \nbool max_control_force_stop\n\n# Maximum Cartesian velocity parameter \n# [x, y, z] in [mm/s]. Precondition: value > 0.0.\n# [a, b, c] in [rad/s]. Precondition: value > 0.0.\nCartesianQuantity max_cartesian_velocity \n";
  iiwa_msgs.CartesianQuantity getMaxPathDeviation();
  void setMaxPathDeviation(iiwa_msgs.CartesianQuantity value);
  iiwa_msgs.CartesianQuantity getMaxControlForce();
  void setMaxControlForce(iiwa_msgs.CartesianQuantity value);
  boolean getMaxControlForceStop();
  void setMaxControlForceStop(boolean value);
  iiwa_msgs.CartesianQuantity getMaxCartesianVelocity();
  void setMaxCartesianVelocity(iiwa_msgs.CartesianQuantity value);
}
