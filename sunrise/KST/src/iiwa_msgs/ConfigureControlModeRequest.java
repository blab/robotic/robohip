package iiwa_msgs;

public interface ConfigureControlModeRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/ConfigureControlModeRequest";
  static final java.lang.String _DEFINITION = "# This service allows to set a control mode at runtime.\n# The user can select control modes defined in iiwa_msgs/ControlMode and set the respective parameters accordingly to the selected mode.\n\nint32 control_mode\niiwa_msgs/JointImpedanceControlMode joint_impedance\niiwa_msgs/CartesianImpedanceControlMode cartesian_impedance\niiwa_msgs/DesiredForceControlMode desired_force\niiwa_msgs/SinePatternControlMode sine_pattern\niiwa_msgs/CartesianControlModeLimits limits\n";
  int getControlMode();
  void setControlMode(int value);
  iiwa_msgs.JointImpedanceControlMode getJointImpedance();
  void setJointImpedance(iiwa_msgs.JointImpedanceControlMode value);
  iiwa_msgs.CartesianImpedanceControlMode getCartesianImpedance();
  void setCartesianImpedance(iiwa_msgs.CartesianImpedanceControlMode value);
  iiwa_msgs.DesiredForceControlMode getDesiredForce();
  void setDesiredForce(iiwa_msgs.DesiredForceControlMode value);
  iiwa_msgs.SinePatternControlMode getSinePattern();
  void setSinePattern(iiwa_msgs.SinePatternControlMode value);
  iiwa_msgs.CartesianControlModeLimits getLimits();
  void setLimits(iiwa_msgs.CartesianControlModeLimits value);
}
