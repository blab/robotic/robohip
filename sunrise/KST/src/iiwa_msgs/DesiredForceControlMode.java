package iiwa_msgs;

public interface DesiredForceControlMode extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/DesiredForceControlMode";
  static final java.lang.String _DEFINITION = "# The degree of freedom on which the desired force\nint32 cartesian_dof\n\n# The value of the desired force. In [N].\nfloat64 desired_force\n\n# The value of the stiffness. In [N/m].\nfloat64 desired_stiffness";
  int getCartesianDof();
  void setCartesianDof(int value);
  double getDesiredForce();
  void setDesiredForce(double value);
  double getDesiredStiffness();
  void setDesiredStiffness(double value);
}
