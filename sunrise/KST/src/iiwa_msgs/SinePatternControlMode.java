package iiwa_msgs;

public interface SinePatternControlMode extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SinePatternControlMode";
  static final java.lang.String _DEFINITION = "# The degree of freedom for performing the sine oscillation.\nint32 cartesian_dof\n\n# The value of the frequency for the sine oscillation [Hz].\nfloat64 frequency\n\n# The value of the amplitude. In [N].\nfloat64 amplitude\n\n# The value of the stiffness. In [N/m].\nfloat64 stiffness";
  int getCartesianDof();
  void setCartesianDof(int value);
  double getFrequency();
  void setFrequency(double value);
  double getAmplitude();
  void setAmplitude(double value);
  double getStiffness();
  void setStiffness(double value);
}
