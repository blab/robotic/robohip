package iiwa_msgs;

public interface CartesianImpedanceControlMode extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianImpedanceControlMode";
  static final java.lang.String _DEFINITION = "\n# Stiffness values [x, y, z, a, b, c] for the cartesian impedance, x, y, z in [N/m], a, b, c in [Nm/rad]. \n# Precondition: 0.0 <= x, y, z <= 5000.0 and 0.0 <= a, b, c <= 300.0. \nCartesianQuantity cartesian_stiffness\n\n# Dimensionless damping values for the cartesian impedance control, for all degrees of freedom : [x, y, z, a, b, c].\n# Precondition: 0.1 <= x, y, z, a, b, c <= 1.0. \nCartesianQuantity cartesian_damping\n\n# The stiffness value for null space [Nm/rad]. \n# Precondition: 0.0 <= value. \nfloat64 nullspace_stiffness\n\n# The damping parameter for null space [Nm*s/rad]. \n# Precondition: value >= 0.3 and value <= 1.0. - A good damping value is 0.7. \nfloat64 nullspace_damping";
  iiwa_msgs.CartesianQuantity getCartesianStiffness();
  void setCartesianStiffness(iiwa_msgs.CartesianQuantity value);
  iiwa_msgs.CartesianQuantity getCartesianDamping();
  void setCartesianDamping(iiwa_msgs.CartesianQuantity value);
  double getNullspaceStiffness();
  void setNullspaceStiffness(double value);
  double getNullspaceDamping();
  void setNullspaceDamping(double value);
}
