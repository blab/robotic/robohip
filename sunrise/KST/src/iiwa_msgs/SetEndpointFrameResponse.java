package iiwa_msgs;

public interface SetEndpointFrameResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetEndpointFrameResponse";
  static final java.lang.String _DEFINITION = "\nbool success\nstring error";
  boolean getSuccess();
  void setSuccess(boolean value);
  java.lang.String getError();
  void setError(java.lang.String value);
}
