package iiwa_msgs;

public interface JointQuantity extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointQuantity";
  static final java.lang.String _DEFINITION = "float32 a1\nfloat32 a2\nfloat32 a3\nfloat32 a4\nfloat32 a5\nfloat32 a6\nfloat32 a7";
  float getA1();
  void setA1(float value);
  float getA2();
  void setA2(float value);
  float getA3();
  void setA3(float value);
  float getA4();
  void setA4(float value);
  float getA5();
  void setA5(float value);
  float getA6();
  void setA6(float value);
  float getA7();
  void setA7(float value);
}
