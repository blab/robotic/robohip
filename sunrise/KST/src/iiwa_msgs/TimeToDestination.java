package iiwa_msgs;

public interface TimeToDestination extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/TimeToDestination";
  static final java.lang.String _DEFINITION = "---\nfloat64 remaining_time\n";
}
