package iiwa_msgs;

public interface DOF extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/DOF";
  static final java.lang.String _DEFINITION = "int32 X = 1\nint32 Y = 2\nint32 Z = 3\nint32 A = 4\nint32 B = 5\nint32 C = 6\nint32 ROT = 7\nint32 TRANSL = 8\nint32 ALL = 9\n";
  static final int X = 1;
  static final int Y = 2;
  static final int Z = 3;
  static final int A = 4;
  static final int B = 5;
  static final int C = 6;
  static final int ROT = 7;
  static final int TRANSL = 8;
  static final int ALL = 9;
}
