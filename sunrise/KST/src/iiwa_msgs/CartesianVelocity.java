package iiwa_msgs;

public interface CartesianVelocity extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianVelocity";
  static final java.lang.String _DEFINITION = "Header header\nCartesianQuantity velocity";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.CartesianQuantity getVelocity();
  void setVelocity(iiwa_msgs.CartesianQuantity value);
}
