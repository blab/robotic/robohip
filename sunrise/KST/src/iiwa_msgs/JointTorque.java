package iiwa_msgs;

public interface JointTorque extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointTorque";
  static final java.lang.String _DEFINITION = "Header header\nJointQuantity torque";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.JointQuantity getTorque();
  void setTorque(iiwa_msgs.JointQuantity value);
}
