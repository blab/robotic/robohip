package iiwa_msgs;

public interface MoveToCartesianPose extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/MoveToCartesianPose";
  static final java.lang.String _DEFINITION = "# Goal\nCartesianPose cartesian_pose\n\n---\n# Result\nbool success\nstring error\n\n---\n# Feedback\n";
}
