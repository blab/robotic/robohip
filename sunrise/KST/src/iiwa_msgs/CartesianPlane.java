package iiwa_msgs;

public interface CartesianPlane extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianPlane";
  static final java.lang.String _DEFINITION = "int32 XY = 1\nint32 XZ = 2\nint32 YZ = 3";
  static final int XY = 1;
  static final int XZ = 2;
  static final int YZ = 3;
}
