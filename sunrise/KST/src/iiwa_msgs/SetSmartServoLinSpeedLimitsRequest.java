package iiwa_msgs;

public interface SetSmartServoLinSpeedLimitsRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetSmartServoLinSpeedLimitsRequest";
  static final java.lang.String _DEFINITION = "# Translational and rotational speed in m/s and rad/s\ngeometry_msgs/Twist max_cartesian_velocity\n\n";
  geometry_msgs.Twist getMaxCartesianVelocity();
  void setMaxCartesianVelocity(geometry_msgs.Twist value);
}
