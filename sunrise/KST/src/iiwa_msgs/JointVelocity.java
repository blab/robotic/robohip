package iiwa_msgs;

public interface JointVelocity extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/JointVelocity";
  static final java.lang.String _DEFINITION = "Header header\nJointQuantity velocity";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.JointQuantity getVelocity();
  void setVelocity(iiwa_msgs.JointQuantity value);
}
