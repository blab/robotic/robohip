package iiwa_msgs;

public interface SetSpeedOverrideRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/SetSpeedOverrideRequest";
  static final java.lang.String _DEFINITION = "# Override the velocity factor for all motions (relative between 0.0 and 1.0)\nfloat64 override_reduction\n\n";
  double getOverrideReduction();
  void setOverrideReduction(double value);
}
