package iiwa_msgs;

public interface CartesianEulerPose extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/CartesianEulerPose";
  static final java.lang.String _DEFINITION = "Header header\nCartesianQuantity pose";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  iiwa_msgs.CartesianQuantity getPose();
  void setPose(iiwa_msgs.CartesianQuantity value);
}
