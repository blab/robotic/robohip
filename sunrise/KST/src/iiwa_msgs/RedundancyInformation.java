package iiwa_msgs;

public interface RedundancyInformation extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "iiwa_msgs/RedundancyInformation";
  static final java.lang.String _DEFINITION = "# E1 parameter\n# Specifies Angle of the elbow joint.\nfloat64 e1\n\n# Status parameter\n#   Bit 0: 1 - The robot is working above its head\n#          0 - The robot is working in the ground area\n#   Bit 1: 1 - Angle A4 < 0\uFFFD\uFFFD\n#          0 - Angle A4 >= 0\uFFFD\uFFFD\n#   Bit 2: 1 - Angle A6 <= 0\n#          0 - Angle A6 > 0\nint32 status\n\n# Turn parameter\n# According to Sunrise handbook this is not used for the iiwa.\nint32 turn\n";
  double getE1();
  void setE1(double value);
  int getStatus();
  void setStatus(int value);
  int getTurn();
  void setTurn(int value);
}
