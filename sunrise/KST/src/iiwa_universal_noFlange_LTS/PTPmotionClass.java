package iiwa_universal_noFlange_LTS;

//Copyright: Mohammad SAFEEA, 9th-April-2018

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.lang.Math;
import com.kuka.generated.ioAccess.Signaux_profinetIOGroup;
import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.executionModel.ExecutionState;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.ObjectFrame;
import com.kuka.roboticsAPI.geometricModel.math.Matrix;
import com.kuka.roboticsAPI.geometricModel.math.Transformation;
import com.kuka.roboticsAPI.motionModel.IMotionContainer;
import com.kuka.roboticsAPI.motionModel.PTP;
import com.kuka.roboticsAPI.motionModel.SplineJP;
import com.kuka.roboticsAPI.motionModel.SplineMotionJP;


public class PTPmotionClass {
	private LBR _lbr;
	private IMotionContainer currentMotion = null;
	public ArrayList<ArrayList<Double>> lastRecords = new ArrayList<ArrayList<Double>>();
	public ArrayList<Double> record = new ArrayList<Double>();
	
	PTPmotionClass(LBR _lbr)
	{
		this._lbr = _lbr;
	}
	
	public IMotionContainer GetCurrentMotion()
	{
		return this.currentMotion;
	}
	
	/**
	 * Determine if the current motion has finished
	 */
	public boolean IsCurrentMotionFinished()
	{
		if (this.currentMotion != null) {
			ExecutionState state = this.currentMotion.getState();
			return (state == ExecutionState.ApplicationExit) ||
					(state == ExecutionState.Cancelled) ||
					(state == ExecutionState.Finished) ||
					(state == ExecutionState.Invalid) ||
					(state == ExecutionState.Recovering);
		}
		return true;
	}
	
	///
	/// Retrieve the plan (in joint space) inside the command and
	/// execute it. The interpolation between each point is a spline.
	/// The execution is synchronous.
	/// Speed value is a factor between 0 and 1
	///
	public void PTPmotionJointSpaceTrajectoryAsync(String plan, Double velocity, int recordHerz, int profinetUpdateHerz, int stopSignalHerz, Signaux_profinetIOGroup profinet_signal, int botaUse, int botaThreshold)
	{
		ArrayList<ArrayList<Double>> positions = ParseTrajectoryPlan(plan);
		List<SplineMotionJP<?>> splineSegments = new ArrayList<SplineMotionJP<?>>();
		
		for (ArrayList<Double> position : positions)
	    {
	        JointPosition jptmp = new JointPosition(_lbr.getJointCount());
	        jptmp.set(
	        		position.get(0), position.get(1), position.get(2), position.get(3),
	        		position.get(4), position.get(5), position.get(6)
	        );
	        SplineMotionJP<?> segment = new PTP(jptmp);
	        splineSegments.add(segment);
		}
		
		SplineJP spline = new SplineJP(splineSegments.toArray(new SplineMotionJP<?>[splineSegments.size()]));
	    spline.setJointVelocityRel(velocity);
	    spline.setJointAccelerationRel(velocity);
	    
	    this.currentMotion = this._lbr.getFlange().moveAsync(spline);
	   	this.RecordMovementAsync(recordHerz);
	   	MatlabToolboxServer.botaIssue = false;
	   	if(botaUse > 0) {
		   	//this.UpdateProfinetVariablesAsync(profinetUpdateHerz, profinet_signal);
		   	this.StopOnProfinetMomentSignalAsync(stopSignalHerz, profinet_signal, botaThreshold);
	   	}
	}
	
	///
	/// Force the robot to release the brake to avoid jerking during movement start.
	/// We do this by simply making a trajectory where the robot is already there.
	///
	public void PTPmotionReleaseBrakeAsync()
	{
		List<SplineMotionJP<?>> splineSegments = new ArrayList<SplineMotionJP<?>>();
	    splineSegments.add(new PTP(new JointPosition(_lbr.getCurrentJointPosition())));
		SplineJP spline = new SplineJP(splineSegments.toArray(new SplineMotionJP<?>[splineSegments.size()]));
		spline.setJointVelocityRel(0.1);
	    spline.setJointAccelerationRel(0.1);
	    this._lbr.getFlange().moveAsync(spline);
	}
	
	///
	/// Parse a plan in a string format into a usable array of doubles.
	/// It is used with Cartesian and Joint path, so the second dimension may differ in shape.
	///
	/// Example string format with 4 number per point (3 points):
	/// 1.2,4.5,9.27,19.38;9.1,5,7.2,94.4;8.8,26,3.4,1.0
	///
	/// The comma separate each value of a point and 
	/// the semicolon separate each point of the path.
	///
	private static ArrayList<ArrayList<Double>> ParseTrajectoryPlan(String plan)
	{
		ArrayList<ArrayList<Double>> positions = new ArrayList<ArrayList<Double>>();
		String[] trajectory = plan.split(";");
		
		for (int i = 0; i < trajectory.length; i++)
		{
			String[] positionStr = trajectory[i].split(",");
			ArrayList<Double> position = new ArrayList<Double>();
			for (int j = 0; j < positionStr.length; j++)
			{
				position.add(Double.parseDouble(positionStr[j]));
			}
			positions.add(position);
		}
		
		return positions;
	}
	
	/**
	 * Start a thread that will records the movement of the current motion.
	 * @param recordHerz
	 */
	private void RecordMovementAsync(final int recordHerz)
	{
		this.lastRecords.clear();
    	this.record.clear();
		final ObjectFrame flangeFrame = this._lbr.getFlange();
		final long start_time_ms = System.currentTimeMillis();
		final Timer record_timer = new Timer();
		final TimerTask record_timer_task = new TimerTask() {
		    @Override
		    public void run() {
				ArrayList<Double> record = new ArrayList<Double>();
				record.add((double)(System.currentTimeMillis() - start_time_ms));
				
				JointPosition jpos = new JointPosition(_lbr.getCurrentJointPosition());
				for (int i = 0; i < 7; i++)
				{
					record.add(jpos.get(i));
				}
				
				
				Frame cartesianPos = PTPmotionClass.this._lbr.getCurrentCartesianPosition(flangeFrame);
				Transformation worldTransform = cartesianPos.transformationFromWorld();
				record.add(cartesianPos.getX() / 1000.0);
				record.add(cartesianPos.getY() / 1000.0);
				record.add(cartesianPos.getZ() / 1000.0);
				record.add(worldTransform.getGammaRad());
				record.add(worldTransform.getBetaRad());
				record.add(worldTransform.getAlphaRad());
				Matrix rotMat = worldTransform.getRotationMatrix();
				for (int r = 0; r < 3; ++r)
				{
					for (int c = 0; c < 3; ++c)
					{
						record.add(rotMat.get(r, c));
					}
				}
				
				double[] torques = PTPmotionClass.this._lbr.getMeasuredTorque().getTorqueValues();
				for (int i = 0; i < torques.length; i++)
			    {
			    	record.add(torques[i]);
			    }
				
				double[] externalTorques = PTPmotionClass.this._lbr.getExternalTorque().getTorqueValues();
				for (int i = 0; i < externalTorques.length; i++)
			    {
			    	record.add(externalTorques[i]);
			    }
				
				PTPmotionClass.this.lastRecords.add(record);
				PTPmotionClass.this.record = record;
				
				if (PTPmotionClass.this.IsCurrentMotionFinished())
			    {
					this.cancel();
			    }
		    };
		};
		record_timer.scheduleAtFixedRate(record_timer_task, 0, 1000 / recordHerz);
	}
	
	private long convertJointValue(double originalJointValue, int conversionMult, double offset)
	{
		return Double.valueOf(originalJointValue * conversionMult + offset).longValue();
	}
	
	
	/**
	 * Start a thread that will update the PROfiNet variables.
	 * @param updateHerz
	 * @param profinet_signal
	 */
	public void UpdateProfinetVariablesAsync(final int updateHerz, final Signaux_profinetIOGroup profinet_signal)
	{
		final int convertion_mult = 1000000;
		final double offset = Math.pow(2, 32);
		final Timer profinet_timer = new Timer();
		final TimerTask profinet_timer_task = new TimerTask() {
		    @Override
		    public void run() {
		    	if (MatlabToolboxServer.stop)
			    {
					this.cancel();
			    }
		    	if(!MatlabToolboxServer.stop) {		    	
		    		JointPosition jpos = new JointPosition(_lbr.getCurrentJointPosition());
			    	try {
				    	profinet_signal.setJoint1(convertJointValue(jpos.get(0), convertion_mult, offset));
				    	profinet_signal.setJoint2(convertJointValue(jpos.get(1), convertion_mult, offset));
				    	profinet_signal.setJoint3(convertJointValue(jpos.get(2), convertion_mult, offset));
				    	profinet_signal.setJoint4(convertJointValue(jpos.get(3), convertion_mult, offset));
				    	profinet_signal.setJoint5(convertJointValue(jpos.get(4), convertion_mult, offset));
				    	profinet_signal.setJoint6(convertJointValue(jpos.get(5), convertion_mult, offset));
				    	profinet_signal.setJoint7(convertJointValue(jpos.get(6), convertion_mult, offset));
			    	} catch (IllegalStateException e) {
			    		MatlabToolboxServer.Logger.warn(this.getClass().getSimpleName() + ": An error occured during BOTA communications, are you sure the system is activated ?");
			    		this.cancel();
			    	}
		    	}
		    };
		};
		profinet_timer.scheduleAtFixedRate(profinet_timer_task, 0, 1000 / updateHerz);
	}
	
	/**
	 * Start a thread that will stop the movement if a moment if greater than a given threshold
	 * @param updateHerz
	 * @param profinet_signal
	 * @param botaThreshold
	 */
	private void StopOnProfinetMomentSignalAsync(final int updateHerz, final Signaux_profinetIOGroup profinet_signal, final int botaThreshold)
	{
		final Timer profinet_timer = new Timer();
		final TimerTask profinet_timer_task = new TimerTask() {
		    @Override
		    public void run() {
		    	if (PTPmotionClass.this.IsCurrentMotionFinished())
			    {
					this.cancel();
			    }
		    	
		    	try {

			    	
			    	/**
			    	 * All moments can be positive or negative but profinet does not allow negative values. 
					 * The BOTA adds 2^16 to all values to avoid that, so we need to substract it
			    	 */
			    	double momentZ = profinet_signal.getMomentZ();
		    		momentZ =  Math.abs(momentZ - Math.pow(2, 16));

		    		double momentY = profinet_signal.getMomentY();
		    		momentY =  Math.abs(momentY - Math.pow(2, 16));

		    		double momentX = profinet_signal.getMomentX();
		    		momentX = Math.abs(momentX - Math.pow(2, 16));

		    		if(momentZ > botaThreshold){
			    		MatlabToolboxServer.botaIssue = true;
			    		Stop();
			    	}
			    	if(momentY > botaThreshold) {		    		
			    		MatlabToolboxServer.botaIssue = true;
			    		Stop();
			    	}
			    	if(momentX > botaThreshold) {
			    		MatlabToolboxServer.botaIssue = true;
			    		Stop();
			    	}
		    	} catch (IllegalStateException e) {
		    		MatlabToolboxServer.Logger.warn(this.getClass().getSimpleName() + ": An error occured during BOTA communications, are you sure the system is activated ? The threshold is : "+botaThreshold);
		    		this.cancel();
		    	}
		    };
		};
		profinet_timer.scheduleAtFixedRate(profinet_timer_task, 0, 1000 / updateHerz);
	}
	
	
	/**
	 * Stop the current movement if any.
	 */
	public void Stop()
	{
		if (this.currentMotion != null)
		{
			MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": canceling motion...");
			this.currentMotion.cancel();
			MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": canceled motion...");
		}
	}
}
