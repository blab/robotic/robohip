package iiwa_universal_noFlange_LTS;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class CommandServer {
	public final int PORT = 30001;
	public final int ACCEPT_TIMEOUT_MS = 60*1000;
	public final int READ_TIMEOUT_S = 10;
	public final boolean DEBUG = false;
	
	private ServerSocket ss = null;
	private Socket soc = null;
	
	public boolean initSocket()
    {
    	try {
	    	this.ss = new ServerSocket(PORT);
	    	
			try
			{
				this.ss.setSoTimeout(ACCEPT_TIMEOUT_MS);
				MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": awaiting client for " + (ACCEPT_TIMEOUT_MS/1000) + "s...");
				this.soc = this.ss.accept();
				MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": client received");
			}
			catch (SocketTimeoutException e)
			{
				if (this.ss != null)
				{
					this.ss.close();
					this.ss = null;
				}
				MatlabToolboxServer.Logger.warn(this.getClass().getSimpleName() + ": timeout, no client received");
				return false;
			}
			catch (Exception e)
			{
				if (this.ss != null)
				{
					this.ss.close();
					this.ss = null;
				}
				MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": socket error", e);
				return false;
			}
			
			// Short timeout necessary to handle error from MatlabToolboxServer.
			// When the main thread quit after an error, we have 15 seconds to quit in the thread.
			this.soc.setSoTimeout(READ_TIMEOUT_S * 1000);
    	} catch (IOException e1) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": socket error", e1);
			return false;
		} catch (Exception e1) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": error", e1);
			return false;
		}
    	
    	return true;
    }
    
    public String receiveCommand()
    {
    	String daCommand = null;
    	
    	try {
    		if (DEBUG) {
    			MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": awaiting command for " + READ_TIMEOUT_S + "s...");
    		}

			String command_buffer = "";
			int read_char = -1;
			try {
				while (true) {
					read_char = this.soc.getInputStream().read(); 
					if ((read_char == -1) || (read_char == '\n')) {
						break;
					}
					command_buffer += ((char)read_char) + "";
				};
			} 
			catch (SocketTimeoutException e) {
				if (DEBUG) {
					MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": command read timeout");
				}
				read_char = 0; // Avoid quitting on timeout, since it is managed
				daCommand = "";
			}
			
			// If the byte read is -1, it means the stream is invalid, so we quit
			if (read_char == -1) {
				MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": client disconnected abruptly");
			}
			else if (!command_buffer.isEmpty()) {
				if (DEBUG) {
					MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": command received");
				}
				daCommand = command_buffer;
			}
		} catch (IOException e1) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": read socket error", e1);
		} catch (Exception e1) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": read socket error", e1);
		}
    	
    	return daCommand;
    }

	public boolean sendCommand(String s)
	{
		try {
			return this.sendCommand(s.getBytes("US-ASCII"));
		} catch (UnsupportedEncodingException e) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": error while sending string data", e);
		}
		return false;
	}
	
	public boolean sendCommand(byte[] s)
	{
		if (ss==null) {
			return false;
		}
		if (soc==null) {
			return false;
		}
		if (soc.isClosed()) {
			return false;
		}
		
		try {
			OutputStream soc_out = soc.getOutputStream();
			soc_out.write(s);
			soc_out.flush();
			return true;
		} catch (IOException e) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": error while sending binary data", e);
		}
		
		return false;
	}
    
    public void terminateSocket()
    {
    	MatlabToolboxServer.Logger.info(this.getClass().getSimpleName() + ": terminating socket...");
    	
    	try {
			if (this.soc != null) {
				this.soc.close();
				this.soc = null;
			}
    	} catch (IOException e) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": client socket close error", e);
		} catch (Exception e) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": client socket close error", e);
		}
    	
    	try {
			if (this.ss != null) {
				this.ss.close();
				this.ss = null;
			}
		} catch (IOException e) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": server socket close error", e);
		} catch (Exception e) {
			MatlabToolboxServer.Logger.error(this.getClass().getSimpleName() + ": server socket close error", e);
		}
    }
}
