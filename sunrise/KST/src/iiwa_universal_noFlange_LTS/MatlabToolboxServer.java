package iiwa_universal_noFlange_LTS;
/* By Mohammad SAFEEA: Coimbra University-Portugal, 
 * Ensam University-France
 * 
 * KST 1.7
 * 
 * First upload 07-May-2017
 * 
 * Final update 26th-06-2018 
 * 
 * This is a multi-threaded server program that is meant to be used with both
 *    KUKA iiwa 7 R 800
 * or KUKA iiwa 14 R 820.
 * The robot shall be provided with a pneumatic flange, 
 * the server of this application listens on the port 30001.
 * 
 * */

import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;
import com.kuka.connectivity.motionModel.directServo.DirectServo;
import com.kuka.connectivity.motionModel.directServo.IDirectServoRuntime;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import com.kuka.roboticsAPI.controllerModel.Controller;
import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.Tool;
import com.kuka.roboticsAPI.geometricModel.math.Transformation;
import com.kuka.roboticsAPI.sensorModel.ForceSensorData;
import com.kuka.roboticsAPI.sensorModel.TorqueSensorData;
import com.kuka.task.ITaskLogger;
import com.kuka.generated.ioAccess.*;

public class MatlabToolboxServer extends RoboticsAPIApplication
{
	public final int JOINT_COUNT = 7;
	
	private static MatlabToolboxServer instance = null;
	public static MatlabToolboxServer GetInstance()
	{
		return MatlabToolboxServer.instance;
	}
	
	public static ITaskLogger Logger = null;
    private LBR _lbr = null;
	private Controller kuka_Sunrise_Cabinet_1 = null;
	private Tool _toolAttachedToLBR = null;
    private PTPmotionClass ptpm = null;
    private CommandServer cmdSrv = new CommandServer();
    private Signaux_profinetIOGroup _profinetSignal = null;
        
    private double jDispMax[] = new double[JOINT_COUNT];
    private double updateCycleJointPos[] = new double[JOINT_COUNT];
    
    //PTP variables
    private double jRelVel = 0.0;
    private double EEFVel = 0.0;
    private double EEFRotVel = 0.0;
    
    public double jpos[] = new double[JOINT_COUNT];
    private double EEFpos[] = new double[JOINT_COUNT];
    private double EEFposCirc1[] = new double[JOINT_COUNT];
    private double EEFposCirc2[] = new double[JOINT_COUNT];
    public boolean directSmart_ServoMotionFlag = false;
    private double EEfServoPos[] = new double[JOINT_COUNT];
    private double jvel[] = new double[JOINT_COUNT];
    private double jvelOld[] = new double[JOINT_COUNT];
    private int recordHerz = 100;
    private int profinetUpdateHerz = 125;
    private int stopSignalHerz = 125;

	private final String STOPCHAR = Character.toString((char)(10));
	private final String ACK = "done" + STOPCHAR;
	private final String NAK = "nak" + STOPCHAR;
	
	private int botaUse = 0;
	private int botaThreshold = 0;
	public static boolean botaIssue = false;
	public static boolean stop = false;

    @Override
    public void initialize()
    {
    	Logger = this.getLogger();
        Logger.info(this.getClass().getSimpleName() + ": initialising...");
        
        this._lbr = this.getContext().getDeviceFromType(LBR.class);
        this.kuka_Sunrise_Cabinet_1 = this.getController("KUKA_Sunrise_Cabinet_1");
		this.ptpm = new PTPmotionClass(this._lbr);
		
		
		this._profinetSignal = new Signaux_profinetIOGroup(this.kuka_Sunrise_Cabinet_1);
		stop = false;
		botaIssue = false;
		
		
		for (int i = 0; i < this.JOINT_COUNT; i++)
        {
        	this.jpos[i]=0;
        	this.EEFpos[i]=0;
        	this.EEFposCirc1[i]=0;
            this.EEFposCirc2[i]=0;
        	this.jDispMax[i]=0;
        	this.updateCycleJointPos[i]=0;
        	this.EEfServoPos[i]=0;
        	this.jvel[i]=0;
        	this.jvelOld[i]=0;
        }
		
		Logger.info(this.getClass().getSimpleName() + ": initialised");
		
		this.ptpm.UpdateProfinetVariablesAsync(this.profinetUpdateHerz, this._profinetSignal);
		Logger.info(this.getClass().getSimpleName() + " Profinet variable update initialised");
    }

    /**
     * Main Application Routine
     */
    @Override
    public void run()
    {
    	Logger.info(this.getClass().getSimpleName() + ": started");
    	
    	if (this.cmdSrv.initSocket())
        {
	    	while (true)
	        {
	    		try
	    		{
			        if (!this.executeCommand(this.cmdSrv.receiveCommand()))
			        {
			        	break;
			        }
	    		}
	    		catch (Throwable t)
	    		{
	    			Logger.error(this.getClass().getSimpleName() + ": command error - ", t);
	    			this.cmdSrv.sendCommand(NAK);
	    		}
	    		
	        }
        }

    	Logger.info(this.getClass().getSimpleName() + ": terminating...");
    	stop = true;
    	this.directSmart_ServoMotionFlag = false;
    	this.ptpm.Stop();
    	this.cmdSrv.terminateSocket();
    	Logger.info(this.getClass().getSimpleName() + ": terminated");
    }
    
    private boolean executeCommand(final String daCommand)
    {
    	if (daCommand == null) {
    		return false;
    	}
    	
    	if(daCommand.startsWith("jf_"))
       	{
       		boolean tempBool=getTheJointsf(daCommand);
       		if(tempBool==false)
       		{
       			this.directSmart_ServoMotionFlag=false;
       		}
    		// this.cmdSrv.sendCommand(ACK); no acknowledgement in fast execution mode
    	}
		// If the signal is equal to end, you shall turn off the server.
		else if(daCommand.startsWith("end"))
		{
			return false;
		}
		// Put the direct_servo joint angles command in the joint variable
		else if(daCommand.startsWith("jp"))
    	{
			if(daCommand.startsWith("jp_"))
			{
				boolean tempBool=getTheJoints(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				this.cmdSrv.sendCommand(this.ACK);
			}
			else if(daCommand.startsWith("jpExT_"))
			{
				boolean tempBool=getTheJoints(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsExternalTorquesToClient();
			}
			else if(daCommand.startsWith("jpMT_"))
			{
				boolean tempBool=getTheJoints(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsMeasuredTorquesToClient();
			}
			else if(daCommand.startsWith("jpEEfP_"))
			{
				boolean tempBool=getTheJoints(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendEEfPositionToClient(); // "modified 12th-May-2019"
			}
			else if(daCommand.startsWith("jpJP_"))
			{
				boolean tempBool=getTheJoints(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsPositionsToClient();
			}
			else if(daCommand.startsWith("jpEEfFrelEEF_"))
			{
				// "newly-added 12th-May-2019"
				boolean tempBool=getTheJoints(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendEEFforcesToClient();
			}
    	}
		else if(daCommand.startsWith("vel"))
    	{
			if(daCommand.startsWith("velJDC_"))
			{
				boolean tempBool=getJointsVelocitiesForVelocityContrtolMode(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				this.cmdSrv.sendCommand(this.ACK);
			}
			else if(daCommand.startsWith("velJDCExT_"))
			{
				boolean tempBool=getJointsVelocitiesForVelocityContrtolMode(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsExternalTorquesToClient();
			}
			else if(daCommand.startsWith("velJDCMT_"))
			{
				boolean tempBool=getJointsVelocitiesForVelocityContrtolMode(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsMeasuredTorquesToClient();
			}
			else if(daCommand.startsWith("velJDCEEfP_"))
			{
				boolean tempBool=getJointsVelocitiesForVelocityContrtolMode(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendEEfPositionToClient(); // "modified 12th-May-2019"
			}
			else if(daCommand.startsWith("velJDCJP_"))
			{
				boolean tempBool=getJointsVelocitiesForVelocityContrtolMode(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsPositionsToClient();
			}
       	}
		else if(daCommand.startsWith("cArtixanPosition"))
       	{
			if(daCommand.startsWith("cArtixanPositionCirc1"))
			{
        		boolean tempBool=getEEFposCirc1(daCommand);
        		if(tempBool==false)
        		{
        			//this.directServoMotionFlag=false;
        		}
        		this.cmdSrv.sendCommand(this.ACK);
			}
			else if(daCommand.startsWith("cArtixanPositionCirc2"))
			{
        		boolean tempBool=getEEFposCirc2(daCommand);
        		if(tempBool==false)
        		{
        			//this.directServoMotionFlag=false;
        		}
        		this.cmdSrv.sendCommand(this.ACK);
			}
			else
			{
        		boolean tempBool=getEEFpos(daCommand);
        		if(tempBool==false)
        		{
        			//this.directServoMotionFlag=false;
        		}
        		this.cmdSrv.sendCommand(this.ACK);
			}
    	}
		// This instruction is used to turn_off the direct_servo controller
       	else if(daCommand.startsWith("stopDirectServoJoints"))
       	{
       		this.directSmart_ServoMotionFlag=false;
       		this.cmdSrv.sendCommand(this.ACK);
       	}
		else if(daCommand.startsWith("DcSe"))
        {
			if(daCommand.startsWith("DcSeCar_"))
			{
				boolean tempBool=getThePositions(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				// this.cmdSrv.sendCommand(ACK);
			}
			else if(daCommand.startsWith("DcSeCarExT_"))
			{
				boolean tempBool=getThePositions(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsExternalTorquesToClient();
			}
			else if(daCommand.startsWith("DcSeCarMT_"))
			{
				boolean tempBool=getThePositions(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsMeasuredTorquesToClient();
			}
			else if(daCommand.startsWith("DcSeCarEEfP_"))
			{
				boolean tempBool=getThePositions(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendEEfPositionToClient(); // "modified 12th-May-2019"
			}
			else if(daCommand.startsWith("DcSeCarJP_"))
			{
				boolean tempBool=getThePositions(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendJointsPositionsToClient();
			}
			else if(daCommand.startsWith("DcSeCarEEfFrelEEF_")) 
			{
				// "newly-added 12th-May-2019"
				// Update Cartesian position of EEF and returns force force feedback described in EEF reference frame	
				boolean tempBool=getThePositions(daCommand);
				if(tempBool==false)
				{
					this.directSmart_ServoMotionFlag=false;
				}
				sendEEFforcesToClient();
			}
        }
        else if(daCommand.startsWith("getJointsPositions"))
    	{
    		Logger.info(this.getClass().getSimpleName() + ": sending back joint position...");
    		sendJointsPositionsToClient();
    		Logger.info(this.getClass().getSimpleName() + ": joint position sent back");
    	}
    	else if(daCommand.startsWith("getLastRecords"))
    	{
    		Logger.info(this.getClass().getSimpleName() + ": sending back last records...");
    		sendLastRecords(this.ptpm.lastRecords);
    		Logger.info(this.getClass().getSimpleName() + ": last records sent back");
    	}
    	else if(daCommand.startsWith("getLastRecord"))
    	{
    		ArrayList<ArrayList<Double>> recordToSend = new ArrayList<ArrayList<Double>>();
			recordToSend.add(this.ptpm.record);
    		sendLastRecords(new ArrayList<ArrayList<Double>>(recordToSend));
    	}
    	else if (daCommand.startsWith("TFtrans"))
		{
			////////////////////////////////
			// Attach default tool equal to flange
			//double[] tempx1={0,0,0,0,0,0};
			//attachTheToolToFlange("defaultToolEqualFlange",tempx1);
			///////////////////////////////////////////////
			/////////////////////////////////////
			// Attach tool
			/*String st=daCommand;
			double[] tempx2=getTransformDataFromCommand(st);
			if(tempx2.length==6)
			{
				attachTheToolToFlange("KST17Tool",tempx2);
		        this.cmdSrv.sendCommand(ACK);
				Logger.info("The tool is attached successfully");
				String message;
				message="X tool "+Double.toString(tempx2[0]);
				Logger.info(message) ;		
				message="Y tool "+Double.toString(tempx2[1]);
				Logger.info(message) ;	
				message="Z tool "+Double.toString(tempx2[2]);
				Logger.info(message) ;	
				message="A tool "+Double.toString(tempx2[3]);
				Logger.info(message) ;		
				message="B tool "+Double.toString(tempx2[4]);
				Logger.info(message) ;	
				message="C tool "+Double.toString(tempx2[5]);
				Logger.info(message) ;	
			}
			else
			{
		        this.cmdSrv.sendCommand(NAK);
				Logger.error("Could not attach tool to robot, App terminated");
				terminateFlag=true;
				break;
			}*/
		
		}
		/////////////////////////////////////
		else if(daCommand.startsWith("startDirectServoJoints"))
    	{
    		directSmart_ServoMotionFlag=true;
    		this.cmdSrv.sendCommand(ACK);
    		Logger.info("realtime control initiated");
    		directServoStartJoints();
    		Logger.info("realtime control terminated");
    		
    	}
    	else if(daCommand.startsWith("stDcEEf_"))
    	{
    		directSmart_ServoMotionFlag=true;
    		this.cmdSrv.sendCommand(ACK);
    		Logger.info("realtime control in Cartesian space initiated");
    		directServoStartCartezian();
    		Logger.info("realtime control terminated");
    		
    	}
    	// raltime velocity control in joint space
    	else if(daCommand.startsWith("stVelDcJoint_"))
    	{
    		directSmart_ServoMotionFlag=true;
    		this.cmdSrv.sendCommand(ACK);
    		Logger.info("Velocity-mode, realtime control in joint space initiated");
    		directServoStartVelMode();
    		Logger.info("realtime control terminated");
    		
    	}

    	else if(daCommand.startsWith(
    			"startSmartImpedneceJoints"))
    	{
    		// pre-processing step
    		directSmart_ServoMotionFlag=true;
    		double[] variables=
    				SmartServoWithImpedence.
    				getControlParameters(daCommand);
    		if(variables.length>6)
    		{
        		double massOfTool=variables[0];
    			double[] toolsCOMCoordiantes=
    				{variables[1],variables[2],variables[3]};
    			double cStifness=variables[4];
    			double rStifness=variables[5];
    			double nStifness=variables[6];
    			Logger.info("Mass: "+Double.toString(massOfTool));
    			Logger.info("COM X : "+Double.toString(toolsCOMCoordiantes[0]));
    			Logger.info("COM Y : "+Double.toString(toolsCOMCoordiantes[1]));
    			Logger.info("COM Z : "+Double.toString(toolsCOMCoordiantes[2]));
    			Logger.info("Stiffness C: "+Double.toString(cStifness));
    			Logger.info("Stiffness R: "+Double.toString(rStifness));
    			Logger.info("Stiffness N: "+Double.toString(nStifness));
    			this.cmdSrv.sendCommand(ACK);
        		
    			try
        		{
    				Logger.info("realtime control initiated");
	    			SmartServoWithImpedence.startRealTimeWithImpedence(
	    				_lbr,kuka_Sunrise_Cabinet_1, massOfTool, toolsCOMCoordiantes, cStifness, rStifness, nStifness
	    			);
        		}
        		catch (Exception e) {
        			Logger.error(e.toString());
        		}
    			Logger.info("realtime control terminated");
    			this.cmdSrv.sendCommand(ACK);
    		}
    		
    	}
    	// Start the hand guiding mode
    	else if(daCommand.startsWith("handGuiding"))
    	{
    		//FastHandGUiding.handGUiding(_lbr, kuka_Sunrise_Cabinet_1);
    		this.cmdSrv.sendCommand(ACK);
    	}
    	// Start the precise hand guiding mode
    	else if(daCommand.startsWith("preciseHandGuiding"))
    	{
    		/*
			double weightOfTool;
			double[] toolsCOMCoordiantes=
					new double[3];
    		try
    		{
    			// pre-processing step
    			String tempstring=daCommand;
    			double[] toolData=new double[4];
    			toolData=PreciseHandGuidingForUpload2.
    			getWightAndCoordiantesOfCOMofTool
    			(daCommand);
    			tempstring=PreciseHandGuidingForUpload2.LBRiiwa7R800;
    			// separate data of tool to weight+COM
    			weightOfTool=toolData[0];
    			for(int kj=0;kj<3;kj++)
    			{
    				toolsCOMCoordiantes[kj]=
    						toolData[kj+1];	
    			}
    			// start the precise hand guiding
    			PreciseHandGuidingForUpload2.
    			HandGuiding(_lbr, kuka_Sunrise_Cabinet_1
    			,tempstring,weightOfTool,toolsCOMCoordiantes);
    		
    		}
    		catch (Exception e) {
    			Logger.error(e.toString());
    		}*/
    		this.cmdSrv.sendCommand(ACK);
    	} 
    	else if(daCommand.startsWith("doPTPTraj"))
		{
    		//Logger.info(this.getClass().getSimpleName() + ":...:" + daCommand);
    		final String command_name = daCommand.split("_")[0];
    		final String command_trajectory = daCommand.split("_")[1];
    		
    		if (command_name.equals("doPTPTrajJS"))
			{
    			Logger.info(this.getClass().getSimpleName() + ": starting execution trajectory in joint space...");
    			ptpm.PTPmotionReleaseBrakeAsync();
				ptpm.PTPmotionJointSpaceTrajectoryAsync(command_trajectory, this.jRelVel, this.recordHerz, this.profinetUpdateHerz, this.stopSignalHerz, this._profinetSignal, this.botaUse, this.botaThreshold);
				this.cmdSrv.sendCommand(ACK);
			}
		}
    	else if(daCommand.startsWith("doPTPReleaseBrake"))
		{
    		ptpm.PTPmotionReleaseBrakeAsync();
    		this.cmdSrv.sendCommand(ACK);
		}
		else if(daCommand.startsWith("jRelVel"))
		{
			jRelVel=StringManipulationFunctions.jointRelativeVelocity(daCommand);
			Logger.info(this.getClass().getSimpleName() + ": set new relative joint speed (" + jRelVel + ")");
			this.cmdSrv.sendCommand(ACK);
		}
		else if(daCommand.startsWith("botaUse"))
		{
			String[] cmdsplit = daCommand.split("_");
			
			this.botaUse = Integer.valueOf(cmdsplit[1]);
			this.botaThreshold = Integer.valueOf(cmdsplit[2]);
			Logger.info(this.getClass().getSimpleName() + ": set bota usage to (" + this.botaUse + ") with a threshold of : "+this.botaThreshold);
			this.cmdSrv.sendCommand(ACK);
		}
		else if(daCommand.startsWith("EEFVel"))
		{
			String[] commandAndVels = daCommand.split("_");
			if (commandAndVels.length > 1)
			{
				EEFVel = Double.parseDouble(commandAndVels[1]);
			}
			if (commandAndVels.length > 2)
			{
				EEFRotVel = Double.parseDouble(commandAndVels[2]);
			}
			Logger.info(this.getClass().getSimpleName() + ": set new cartesian speed:");
			Logger.info("  position speed: " + EEFVel + " m/s");
			Logger.info("  rotation speed: " + EEFRotVel + " rad/s");
			this.cmdSrv.sendCommand(ACK);
		}
    	// Get torques of joints
		else if(daCommand.startsWith("Torques"))
		{
			if(daCommand.startsWith("Torques_ext_J"))
			{
				sendJointsExternalTorquesToClient();
			}
			else if(daCommand.startsWith("Torques_m_J"))
			{
				sendJointsMeasuredTorquesToClient();
			}		
		}
    	// Get parameters of end effector
		else if(daCommand.startsWith("Eef"))
		{
			if(daCommand.equals("Eef_force"))
			{
				sendEEFforcesToClient();
			}
			else if(daCommand.equals("Eef_moment"))
			{
				sendEEFMomentsToClient();
			}	
			else if(daCommand.equals("Eef_pos"))
			{
				Logger.info(this.getClass().getSimpleName() + ": sending back end-effector pose...");
				sendEEfPositionToClient();
				Logger.info(this.getClass().getSimpleName() + ": end-effector pose sent back");
			}
		}
		else if(daCommand.startsWith("Motion_finished"))
		{
			sendBoolean(this.ptpm.IsCurrentMotionFinished());
		}
		else if(daCommand.startsWith("Bota_Issue"))
		{
			Logger.info(this.getClass().getSimpleName() + ": sending botaIssue value: " + botaIssue);
			sendBoolean(botaIssue);
		}
		else if(daCommand.startsWith("Cancel_motion"))
		{
			this.ptpm.Stop();
			this.cmdSrv.sendCommand(this.ACK);
		}
    	
		else if(daCommand.length()>0)
		{
			Logger.warn("Unrecognized instruction: " + daCommand);
			
		}

    	return true;
    }

	// Attach the tool to the flange functions
	/*private double[] getTransformDataFromCommand(String cmd)
	{
		double[] val=null;
		String thestring=cmd;
		int numericDataCount=0;
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			// First token is the instruction, should be deleted
			String temp=st.nextToken();
			// Following tokens are data of transform from TCP to frame of flange
			val=new double[6];
			int j=0;
			while(st.hasMoreTokens())
			{
				String stringData=st.nextToken();
				if(j<7)
				{
				//Logger.warn(jointString);
				val[j]=
						Double.parseDouble(stringData);
				numericDataCount=numericDataCount+1;
				}
				
				j++;
			}				
		}
		
		if (numericDataCount==6)
		{
			return val;
		}
		else
		{
			double[] ret={1,2};
			return ret;
		}
	}*/
	
	/*private void attachTheToolToFlange(String string,double[] x)
	{
		String TOOL_FRAME = string;
		double[] TRANSFORM_OF_TOOL = x; 
		double[] CENTER_OF_MASS_IN_MILLIMETER = { 0, 0, 0 }; 
		LoadData _loadData = new LoadData();
		double MASS=0;
		_loadData.setMass(MASS);
		_loadData.setCenterOfMass(
		CENTER_OF_MASS_IN_MILLIMETER[0], CENTER_OF_MASS_IN_MILLIMETER[1],
		CENTER_OF_MASS_IN_MILLIMETER[2]);
		
		_toolAttachedToLBR = new Tool(TOOL_FRAME, _loadData);
		XyzAbcTransformation trans = XyzAbcTransformation.ofRad(
		TRANSFORM_OF_TOOL[0], TRANSFORM_OF_TOOL[1],
		TRANSFORM_OF_TOOL[2], TRANSFORM_OF_TOOL[3],
		TRANSFORM_OF_TOOL[4], TRANSFORM_OF_TOOL[5]);
		
		ObjectFrame aTransformation = _toolAttachedToLBR.addChildFrame(TOOL_FRAME + "(TCP)", trans);
		_toolAttachedToLBR.setDefaultMotionFrame(aTransformation);
		// Attach tool to the robot
		_toolAttachedToLBR.attachTo(_lbr.getFlange());
	}*/
	
	///////////////////////////////////
    // Velocity-mode, joint space
	private void directServoStartVelMode()
    {
        boolean doDebugPrints = false;
        
        
        JointPosition initialPosition = new JointPosition(
                _lbr.getCurrentJointPosition());
        // Initiate joints velocities and positions
        double[] jPOS=new double[7];
        for(int i=0;i<7;i++)
        {
        	jvel[i]=0;
        	jvelOld[i]=0;
        	jPOS[i]=initialPosition.get(i);
        }
        
        DirectServo aDirectServoMotion = new DirectServo(initialPosition);

        aDirectServoMotion.setMinimumTrajectoryExecutionTime(40e-3);

        Logger.info("Starting realtime velocity control mode");
        _lbr.moveAsync(aDirectServoMotion);

        Logger.info("Get the runtime of the DirectServo motion");
        IDirectServoRuntime theDirectServoRuntime = aDirectServoMotion
                .getRuntime();
        
        JointPosition destination = new JointPosition(
                _lbr.getJointCount());
        
        try
        {
            // do a cyclic loop
            // Do some timing...
            // in nanosec
        	double dt=0;
        	long t0=System.nanoTime();
        	long t1=t0;
			while(directSmart_ServoMotionFlag==true)
			{

                // ///////////////////////////////////////////////////////
                // Insert your code here
                // e.g Visual Servoing or the like
                // Synchronize with the realtime system
                //theDirectServoRuntime.updateWithRealtimeSystem();

                if (doDebugPrints)
                {
                	Logger.info("Current fifth joint position " + jpos[5]);
                    Logger.info("Current joint destination "
                            + theDirectServoRuntime.getCurrentJointDestination());
                }


                Thread.sleep(1);
                //Logger.warn(Double.toString(jpos[0]));
                //Logger.info(daCommand);
                //JointPosition currentPos = new JointPosition(_lbr.getCurrentJointPosition());
                
                  
                t1=System.nanoTime();
                dt=((double)(t1-t0))/(1e9);
                t0=t1;
                updatejointsPosFromVelAcc(jPOS,dt);
                for (int k = 0; k < destination.getAxisCount(); ++k)
                {            		                		                        
                	destination.set(k, jPOS[k]);
                }                                
	            theDirectServoRuntime.setDestination(destination);
	                
            }
        }
        catch (Exception e)
        {
            Logger.info(e.getLocalizedMessage());
            e.printStackTrace();
            //Print statistics and parameters of the motion
            Logger.info("Simple Cartesian Test \n" + theDirectServoRuntime.toString());
            Logger.info("Stop the realtime velocity control mode");

            
        }

        theDirectServoRuntime.stopMotion();
        Logger.info("Stop realtime velocity control mode");
    }

	// updates joints positions by integration, Midpoint Rieman Sum is utilized
	private void updatejointsPosFromVelAcc(double[] jPOS,double dt)
	{
		for(int i=0;i<7;i++)
		{
			jPOS[i]=jPOS[i]+(jvel[i]+jvelOld[i])*dt/2;
			// update old joints velocities
			jvelOld[i]=jvel[i];
		}		
	}

	private void directServoStartJoints()
    {
        boolean doDebugPrints = false;
        
        
        JointPosition initialPosition = new JointPosition(
                _lbr.getCurrentJointPosition());
        
        
        for(int i=0;i<7;i++)
        {
        	jpos[i]=initialPosition.get(i);
        }
        DirectServo aDirectServoMotion = new DirectServo(initialPosition);

        aDirectServoMotion.setMinimumTrajectoryExecutionTime(40e-3);

        Logger.info("Starting DirectServo motion in position control mode");
        _lbr.moveAsync(aDirectServoMotion);

        Logger.info("Get the runtime of the DirectServo motion");
        IDirectServoRuntime theDirectServoRuntime = aDirectServoMotion
                .getRuntime();
        
        JointPosition destination = new JointPosition(
                _lbr.getJointCount());
        double disp;
        double temp;
        double absDisp;
        
        try
        {
            // do a cyclic loop
            // Do some timing...
            // in nanosec

			while(directSmart_ServoMotionFlag==true)
			{

                // ///////////////////////////////////////////////////////
                // Insert your code here
                // e.g Visual Servoing or the like
                // Synchronize with the realtime system
                //theDirectServoRuntime.updateWithRealtimeSystem();

                if (doDebugPrints)
                {
                	Logger.info("Current fifth joint position " + jpos[5]);
                    Logger.info("Current joint destination "
                            + theDirectServoRuntime.getCurrentJointDestination());
                }


                Thread.sleep(1);
                //Logger.warn(Double.toString(jpos[0]));
                //Logger.info(daCommand);
                JointPosition currentPos = new JointPosition(
                        _lbr.getCurrentJointPosition());
                
                
                
                for (int k = 0; k < destination.getAxisCount(); ++k)
                {
                		
                		
                		double dj=jpos[k]-currentPos.get(k);
                		disp= getTheDisplacment( dj);
                		temp=currentPos.get(k)+disp;
                		absDisp=Math.abs(disp);
                		if(absDisp>jDispMax[k])
                		{
                			jDispMax[k]=absDisp;
                		}
                        destination.set(k, temp);
                        updateCycleJointPos[k]=temp;
                        
                }
                	
                
	            theDirectServoRuntime.setDestination(destination);
	                
            }
        }
        catch (Exception e)
        {
            Logger.info(e.getLocalizedMessage());
            e.printStackTrace();
            //Print statistics and parameters of the motion
            Logger.info("Simple Cartesian Test \n" + theDirectServoRuntime.toString());

            Logger.info("Stop the DirectServo motion");

            
        }

        theDirectServoRuntime.stopMotion();
        Logger.info("Stop the DirectServo motion for the stop instruction was sent");
    }


	private void directServoStartCartezian()
	{
		
		boolean doDebugPrints = false;

        DirectServo aDirectServoMotion = new DirectServo(
                _lbr.getCurrentJointPosition());

        aDirectServoMotion.setMinimumTrajectoryExecutionTime(40e-3);

        Logger.info("Starting DirectServo motion in position control mode");
        this._toolAttachedToLBR.moveAsync(aDirectServoMotion);

        Logger.info("Get the runtime of the DirectServo motion");
        IDirectServoRuntime theDirectServoRuntime = aDirectServoMotion
                .getRuntime();

        Frame aFrame = theDirectServoRuntime.getCurrentCartesianDestination(this._toolAttachedToLBR.getDefaultMotionFrame());
        Frame destFrame = aFrame.copyWithRedundancy();
        // Initiate the initial position 
        EEfServoPos[0]=aFrame.getX();
        EEfServoPos[1]=aFrame.getY();
        EEfServoPos[2]=aFrame.getZ();
        EEfServoPos[3]=aFrame.getAlphaRad();
        EEfServoPos[4]=aFrame.getBetaRad();
        EEfServoPos[5]=aFrame.getGammaRad();
        
        try
        {
            // do a cyclic loop
            // Do some timing...
            // in nanosec

			while(directSmart_ServoMotionFlag==true)
			{

                // ///////////////////////////////////////////////////////
                // Insert your code here
                // e.g Visual Servoing or the like
                // Synchronize with the realtime system
                theDirectServoRuntime.updateWithRealtimeSystem();
                Frame msrPose = theDirectServoRuntime
                        .getCurrentCartesianDestination(this._toolAttachedToLBR.getDefaultMotionFrame());

                if (doDebugPrints)
                {
                	Logger.info("Current cartesian goal " + aFrame);
                    Logger.info("Current joint destination "
                            + theDirectServoRuntime.getCurrentJointDestination());
                }

                Thread.sleep(1);

                // update Cartesian positions
                destFrame.setX(EEfServoPos[0]);
                destFrame.setY(EEfServoPos[1]);
                destFrame.setZ(EEfServoPos[2]);
                destFrame.setAlphaRad(EEfServoPos[3]);
                destFrame.setBetaRad(EEfServoPos[4]);
                destFrame.setGammaRad(EEfServoPos[5]);

                if (doDebugPrints)
                {
                    Logger.info("New cartesian goal " + destFrame);
                    Logger.info("LBR position "
	                            + _lbr.getCurrentCartesianPosition(_lbr
	                                    .getFlange()));
                    Logger.info("Measured cartesian pose from runtime "
	                            + msrPose);
                }

	                theDirectServoRuntime.setDestination(destFrame);
                
            }
        }
        catch (Exception e)
        {
            Logger.info(e.getLocalizedMessage());
            e.printStackTrace();
            //Print statistics and parameters of the motion
            Logger.info("Simple Cartesian Test \n" + theDirectServoRuntime.toString());

            Logger.info("Stop the DirectServo motion");
            
        }
        theDirectServoRuntime.stopMotion();
        Logger.info("Stop the DirectServo motion for the stop instruction was sent");
		
	}


	
	private double getTheDisplacment(double dj)
    {
    	double   a=0.07; 
    	double b=a*0.75; 
		double exponenet=-Math.pow(dj/b, 2);
		return Math.signum(dj)*a*(1-Math.exp(exponenet));
		
    }

	private boolean getThePositions(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			st.nextToken();
				int j=0;
				while(st.hasMoreTokens())
				{
					if(j<6)
					{
						//getLogger().warn(jointString);
						try
						{
							this.EEfServoPos[j]=Double.parseDouble(st.nextToken());
						}
						catch(Exception e)
						{
							return false;
						}						
					}					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
	}

	private boolean getJointsVelocitiesForVelocityContrtolMode(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			st.nextToken();
				int j=0;
				while(st.hasMoreTokens())
				{
					if(j<7)
					{
						//getLogger().warn(jointString);
						try
						{
							this.jvel[j]=
							Double.parseDouble(st.nextToken());
						}
						catch(Exception e)
						{
							return false;
						}						
					}					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
	}
    
	/* The following function is used to extract the 
	 joint angles from the command
	 */
	private boolean getTheJoints(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			String temp=st.nextToken();
			if(temp.startsWith("jp"))
			{
				int j=0;
				while(st.hasMoreTokens())
				{
					String jointString=st.nextToken();
					if(j<7)
					{
						//getLogger().warn(jointString);
						this.jpos[j]=Double.parseDouble(jointString);
					}
					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
		}

		return false;
	}

	/* The following function is used to extract the 
	 joint angles from the command
	 */
	private boolean getTheJointsf(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			String temp=st.nextToken();
			if(temp.startsWith("jf"))
			{
				int j=0;
				while(st.hasMoreTokens())
				{
					String jointString=st.nextToken();
					if(j<7)
					{
						//getLogger().warn(jointString);
						this.jpos[j]=Double.parseDouble(jointString);
					}
					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
		}

		return false;
	}
	
	private boolean getEEFpos(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			String temp=st.nextToken();
			if(temp.startsWith("cArtixanPosition"))
			{
				int j=0;
				while(st.hasMoreTokens())
				{
					String jointString=st.nextToken();
					if(j<6)
					{
						//getLogger().warn(jointString);
						this.EEFpos[j]=Double.parseDouble(jointString);
					}
					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
		}

		return false;
	}

	
	private boolean getEEFposCirc2(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			String temp=st.nextToken();
			if(temp.startsWith("cArtixanPosition"))
			{
				int j=0;
				while(st.hasMoreTokens())
				{
					String jointString=st.nextToken();
					if(j<6)
					{
						//getLogger().warn(jointString);
						this.EEFposCirc2[j]=Double.parseDouble(jointString);
					}
					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
		}

		return false;
	}
	
	
	private boolean getEEFposCirc1(String thestring)
	{
		StringTokenizer st= new StringTokenizer(thestring,"_");
		if(st.hasMoreTokens())
		{
			String temp=st.nextToken();
			if(temp.startsWith("cArtixanPosition"))
			{
				int j=0;
				while(st.hasMoreTokens())
				{
					String jointString=st.nextToken();
					if(j<6)
					{
						//getLogger().warn(jointString);
						this.EEFposCirc1[j]=Double.parseDouble(jointString);
					}
					
					j++;
				}
				return true;
				
			}
			else
			{
				return false;
			}
		}

		return false;
	}
	
	private void sendJointsMeasuredTorquesToClient()
	{
    	
    	TorqueSensorData measuredData= _lbr.getMeasuredTorque();
    	double[] vals=
    			measuredData.getTorqueValues();
    	String s="";
    	for(int i=0;i<vals.length;i++)
    	{
    		s=s+Double.toString(vals[i])+"_";
    	}
		s=s+this.STOPCHAR;
		
		this.cmdSrv.sendCommand(s);
	}
    
	private void sendJointsExternalTorquesToClient()
	{
    	
    	TorqueSensorData measuredData= _lbr.getExternalTorque();
    	double[] vals=
    			measuredData.getTorqueValues();
    	String s="";
    	for(int i=0;i<vals.length;i++)
    	{
    		s=s+Double.toString(vals[i])+"_";
    	}
		s=s+this.STOPCHAR;

		this.cmdSrv.sendCommand(s);
    }
    
	private void sendEEFforcesToClient()
	{
    	
		ForceSensorData cforce = _lbr.getExternalForceTorque(this._lbr.getFlange());
		
		String 	cmdforce=Double.toString(cforce.getForce().getX())
				+"_"+Double.toString(cforce.getForce().getY())
				+"_"+Double.toString(cforce.getForce().getZ())
				+"_"+this.STOPCHAR;

		this.cmdSrv.sendCommand(cmdforce);
    }
    
	private void sendEEFMomentsToClient()
	{
    	
		ForceSensorData cforce = _lbr.getExternalForceTorque(this._lbr.getFlange());
		
		String 	cmdforce=Double.toString(cforce.getTorque().getX())
				+"_"+Double.toString(cforce.getTorque().getY())
				+"_"+Double.toString(cforce.getTorque().getZ())
				+"_"+this.STOPCHAR;

		this.cmdSrv.sendCommand(cmdforce);
    }

	private void sendJointsPositionsToClient()
	{
		// This functions sends the joints positions to the client
    	String s="";
		JointPosition initialPosition = new JointPosition(
                _lbr.getCurrentJointPosition());
		for(int i=0;i<7;i++)
		{
	        s=s+Double.toString(initialPosition.get(i))+"_";       	        
		}
		s=s+this.STOPCHAR;

		this.cmdSrv.sendCommand(s);
	}
    
	private void sendEEfPositionToClient()
	{
		// This functions sends the end effector position to the client
    	String cmdPos="";
		// Read Cartesian position data
		Frame daframe= _lbr.getCurrentCartesianPosition(this._lbr.getFlange());
		Transformation worldTransform = daframe.transformationFromWorld();
		cmdPos= 
		Double.toString(daframe.getX() / 1000)
				+"_"+Double.toString(daframe.getY() / 1000)
				+"_"+Double.toString(daframe.getZ() / 1000)
				+"_"+Double.toString(worldTransform.getGammaRad())
				+"_"+Double.toString(worldTransform.getBetaRad())
				+"_"+Double.toString(worldTransform.getAlphaRad())
				+"_"+this.STOPCHAR;
		//Send the data back to the client

		this.cmdSrv.sendCommand(cmdPos);	
	}
    
	private void sendBoolean(final boolean value)
	{
		String cmdBool = "false" + this.STOPCHAR;
		if (value) {
			cmdBool = "true" + this.STOPCHAR;
		}
		this.cmdSrv.sendCommand(cmdBool);	
	}
    
    // This function convert directly to byte to avoid multiple conversion 
    // since the data can be large
	private void sendLastRecords(ArrayList<ArrayList<Double>> lastRecords)
	{
    	final int lastRecordsSize = lastRecords.size();
    	
    	
    	if (lastRecordsSize > 0)
    	{
    		final int recordSize = lastRecords.get(0).size();
    		// Size of each record in string format:
    		// long data number * (integer part + dot + fractional part + separator) +
    		// double data number * (integer part + dot + fractional part + separator)
    		if(recordSize > 0)
    		{
	    		final int recordStrSize = (1 * (6 + 0 + 0 + 1)) + ((recordSize - 1) * (3 + 1 + 5 + 1));
		    	ByteBuffer cmdRecords = ByteBuffer.allocate((lastRecordsSize * recordStrSize) + 2);
		    	NumberFormat double_formatter = NumberFormat.getInstance(Locale.ENGLISH);
		    	double_formatter.setMaximumIntegerDigits(3);
		    	double_formatter.setMaximumFractionDigits(5);
		    	double_formatter.setGroupingUsed(false);
		    	double_formatter.setRoundingMode(RoundingMode.FLOOR);
		    	NumberFormat long_formatter = NumberFormat.getInstance(Locale.ENGLISH);
		    	long_formatter.setMaximumIntegerDigits(6);
		    	long_formatter.setMaximumFractionDigits(0);
		    	long_formatter.setGroupingUsed(false);
		    	long_formatter.setRoundingMode(RoundingMode.FLOOR);
		
		    	for (int i = 0; i < lastRecordsSize; ++i)
		    	{
		    		ArrayList<Double> record = lastRecords.get(i);
		    		for (int j = 0; j < recordSize; ++j)
		    		{
		    			String number_str = ((j!=0) ? double_formatter : long_formatter).format(record.get(j));
		    			
		    			for (int s = 0; s < number_str.length(); ++s)
		    			{
		    				cmdRecords.put((byte)number_str.charAt(s));
		    			}
		    			if (j < (record.size() - 1))
		    			{
		    				cmdRecords.put((byte)',');
		    			}
		    		}
		    		
		    		if (i < (lastRecordsSize - 1))
					{
						cmdRecords.put((byte)';');
					}
		    	}
	    	
		    	cmdRecords.position(cmdRecords.capacity() - 2);
		    	cmdRecords.putChar(this.STOPCHAR.charAt(0));
		    	this.cmdSrv.sendCommand(cmdRecords.array());
    		} 
    		else 
    		{
    			this.cmdSrv.sendCommand(this.STOPCHAR);
    		}
    	}
    	else
    	{
    		this.cmdSrv.sendCommand(this.STOPCHAR);
    	}
    }

    /**
     * Main routine, which starts the application
     */
    public static void main(String[] args)
    {
        MatlabToolboxServer.instance = new MatlabToolboxServer();
        MatlabToolboxServer.instance.runApplication();
    }

}
