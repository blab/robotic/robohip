package preciseHandGuiding;

import com.kuka.generated.ioAccess.MediaFlangeIOGroup;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import com.kuka.roboticsAPI.controllerModel.Controller;
import com.kuka.roboticsAPI.deviceModel.LBR;

/**
 * Mohammad SAFEEA
 * 
 * University: Coimbra, Ensam.
 * 
 * 27th-september-2017
 *  
 * This is an example on how to implement the precise 
 * hand guiding class into your application.
 * 
 * The precise handguiding class uses the direct servo function
 * make sure to load import it into your project.
 * 
 * 
 */

public class PreciseHandGuiding extends RoboticsAPIApplication {
	private Controller kuka_Sunrise_Cabinet_1;
	private LBR _lbr;
	
	public MediaFlangeIOGroup daIO;

	@Override
	public void initialize() 
	{
		kuka_Sunrise_Cabinet_1 = getController("KUKA_Sunrise_Cabinet_1");
		_lbr = (LBR) getDevice(kuka_Sunrise_Cabinet_1,"LBR_iiwa_14_R820_1");
		daIO = new MediaFlangeIOGroup(kuka_Sunrise_Cabinet_1);
	}

	@Override
	public void run() {
		try
		{
			String s=PreciseHandGuidingImpl.LBRiiwa14R820;
			PreciseHandGuidingImpl.HandGuiding(_lbr, kuka_Sunrise_Cabinet_1,s);
		}
		catch (Exception e) {
			getLogger().error(e.toString());
		}
			
		}

	/**
	 * Auto-generated method stub. Do not modify the contents of this method.
	 */
	public static void main(String[] args) {
		PreciseHandGuiding app = new PreciseHandGuiding();
		app.runApplication();
	}
}
