function [dir] = vecdir(v1, v2, axis)
%VECDIR Compute the direction vector of two points

tmp = nan2zero((v2-v1) / norm(v2-v1));
if exist('axis', 'var')
    dir = tmp(1, axis);
else
    dir = tmp;
end

end

