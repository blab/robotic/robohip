function [cartesian_trajectory] = load_cartesian_trajectory(trajectory_name)
%LOAD_CARTESIAN_TRAJECTORY Load a trajectory into the format x, y, z, rx, ry, rz

loaded_cartesian_trajectory = load(strcat('trajectories/', trajectory_name, '.mat'));
cartesian_trajectory_size = size(loaded_cartesian_trajectory.O_flange, 3);
O_flange = loaded_cartesian_trajectory.O_flange;
if isfield(loaded_cartesian_trajectory, 'Q_flange')
    Q_flange = loaded_cartesian_trajectory.Q_flange;

    % Permute quaterniom rows to fit the Matlab standard
    Q_flange_temp = Q_flange;
    Q_flange = [];
    Q_flange(1,1,:) = Q_flange_temp(4,1,:);
    Q_flange(2,1,:) = Q_flange_temp(1,1,:);
    Q_flange(3,1,:) = Q_flange_temp(2,1,:);
    Q_flange(4,1,:) = Q_flange_temp(3,1,:);
    clear Q_flange_temp;
end

% Quaternion to Euler (x, y, z, rx, ry, rz)
cartesian_trajectory = zeros(cartesian_trajectory_size, 6);
for i = 1:cartesian_trajectory_size
    if exist('Q_flange', 'var')
        cartesian_trajectory(i, 1:6) = [O_flange(1, 1, i), O_flange(2, 1, i), O_flange(3, 1, i), quat2eul(Q_flange(:, 1, i)', 'XYZ')];
    else
        cartesian_trajectory(i, 1:3) = [O_flange(1, 1, i), O_flange(2, 1, i), O_flange(3, 1, i)];
    end
end

end

