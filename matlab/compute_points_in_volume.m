function [n_rounded, volume, points] = compute_points_in_volume(n, volume_minmax_positions, height, height_bottom_offset)
%COMPUTE_POINTS_IN_VOLUME Compute all the points inside the cubic grid,
%   n: number of point on the grid, will be ceiled into the nearest cubic root
%   volume_minmax_positions: various point that define the volume of the grid.
%                            The volume is computed by taking the min/max on each axis.
%   height: height of the volume
%   height_bottom_offset: height that separate the volume and its various positions
%   n_rounded: rounded number of points
%   volume: 2x3 matrix of computed points that represent the computed volume
%   points: nx3 matrix of points in 3D
%   author: David Gonzalez, HEPIA

disp("Compute volume and generate points on grid")
disp("  volume min/max points: " + mat2str(volume_minmax_positions));

% Compute cube with height and bottom offset
marker_height = (min(volume_minmax_positions(:, 3)) + max(volume_minmax_positions(:, 3))) / 2;
volume = [
    min(volume_minmax_positions(:, 1)), min(volume_minmax_positions(:, 2)), marker_height + height_bottom_offset;
    max(volume_minmax_positions(:, 1)), max(volume_minmax_positions(:, 2)), marker_height + height_bottom_offset + height
];
disp("  volume: " + mat2str(volume));

% Round n to avoid non-uniform grid
n_point_per_axis = ceil(nthroot(n, 3));
n_rounded = n_point_per_axis ^ 3;
if (n_rounded ~= n)
    warning("n (which is " + num2str(n) + ") has been ceiled into " + num2str(n_rounded))
end

% Create grid
x_points = linspace(volume(1, 1), volume(2, 1), n_point_per_axis);
y_points = linspace(volume(1, 2), volume(2, 2), n_point_per_axis);
z_points = linspace(volume(1, 3), volume(2, 3), n_point_per_axis);
points = zeros(n_rounded, 3);
i = 1;
for ix = 1:size(x_points, 2)
    for iy = 1:size(y_points, 2)
        for iz = 1:size(z_points, 2)
            points(i, :) = [x_points(ix), y_points(iy), z_points(iz)];
            i = i + 1;
        end
    end
end

end

