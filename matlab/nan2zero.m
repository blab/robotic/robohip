function [y] = nan2zero(x)
%NAN2ZERO Transform all NaN inputs to 0 and keep all other values intact
x(isnan(x)) = 0;
y = x;

end

