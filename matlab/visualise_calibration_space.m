function [] = visualise_calibration_space(n, height, height_bottom_offset, volume_marker_names, working_space_margin, robot, collision_env, motion_capture, axes)
%VISUALISE_CALIBRATION_SPACE Visualise the working space and the volume of calibration
%   n: number of points to test
%   height: Height of the volume (meters)
%   height_bottom_offset: Offset of the start of the volume from the marker (meters)
%   volume_marker_names: list of four marker names to search in marker list for volume position
%   working_space_margin: Working space margin to avoid being to close to hardware limit
%   author: David Gonzalez, HEPIA

[camera_robot_base_positions, ~, ~, ~] = motion_capture.capture(robot.motion_capture_base_marker_names, {}, 100, 1, 1);
save_motion_capture_data(camera_robot_base_positions, [], robot.motion_capture_base_marker_names, {}, 'camera_capture_robot_base');

[camera_volume_marker_positions, ~, ~, ~] = motion_capture.capture(volume_marker_names, {}, 100, 1, 1);
save_motion_capture_data(camera_volume_marker_positions, [], volume_marker_names, {}, 'camera_capture_cal_volume');
transform_c2r = compute_transformation_camera_to_robot(camera_robot_base_positions);
volume_minmax_positions = transform_objects_camera_to_robot(camera_volume_marker_positions, transform_c2r);

if ~exist('axes', 'var')
    axes = visualise_collision_env(working_space_margin, robot.get_home_configuration(), robot.get_rbt(), collision_env);
else
    axes = visualise_collision_env(working_space_margin, robot.get_home_configuration(), robot.get_rbt(), collision_env, axes);
end

[n, volume, points] = compute_points_in_volume(n, volume_minmax_positions, height, height_bottom_offset);
points = move_points_inside_working_space(points, n, volume, working_space_margin);
hold(axes, 'on');
plot3(axes, volume_minmax_positions(:, 1)', volume_minmax_positions(:, 2)', volume_minmax_positions(:, 3)', '.k', 'MarkerSize', 24);
plot3(axes, volume(:, 1)', volume(:, 2)', volume(:, 3)', '.m', 'MarkerSize', 24);
plot3(axes, points(:, 1)', points(:, 2)', points(:, 3)', '.b');
hold(axes, 'off');
axes.Visible = 'on';

end

