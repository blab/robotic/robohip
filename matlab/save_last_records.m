function [filename] = save_last_records(robot)
%SAVE_LAST_RECORDS Get and save the last records
%   Output: nx28
%           1: relative unix timestamp with milliseconds, where 0 is the beginning of the execution;
%           2-8: joint position in radians;
%           9-14: cartesian position (m) and orientation (rad);
%           15-21: torque of each joint (Nm);
%           22-28: external torque of each joint (Nm);

disp("Getting back records...");

records = struct();
records.labels = {
    'timestamp',...
    'joint_position_1', 'joint_position_2', 'joint_position_3', 'joint_position_4',...
    'joint_position_5', 'joint_position_6', 'joint_position_7',...
    'cartesian_position_x', 'cartesian_position_y', 'cartesian_position_z',...
    'cartesian_position_a', 'cartesian_position_b', 'cartesian_position_g',...
    'joint_torque_1', 'joint_torque_2', 'joint_torque_3', 'joint_torque_4',...
    'joint_torque_5', 'joint_torque_6', 'joint_torque_7',...
    'joint_external_torque_1', 'joint_external_torque_2', 'joint_external_torque_3', 'joint_external_torque_4',...
    'joint_external_torque_5', 'joint_external_torque_6', 'joint_external_torque_7'
};
output_filename = robot.get_recording_filename();
records.values = robot.get_last_records();

if isempty(records.values)
    warning("No records was sent back")
    return;
end

if ~isfolder('data')
    mkdir('data');
end

if exist('output_filename', 'var')
    filename = strcat('data/', output_filename, '.mat');
else
    record_date = datetime('now', 'TimeZone', 'local', 'Format', 'yyyy-MM-dd-HH-mm-ss-SSS');
    record_date.TimeZone = 'Z'; % Convert to UTC in place
    filename = strcat('data/records-', string(record_date), '.mat');
end
save(filename, 'records');

disp(strcat('Records saved in "', filename, '"'));

end

