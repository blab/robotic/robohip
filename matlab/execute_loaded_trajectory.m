function [] = execute_loaded_trajectory(joint_trajectory, output_filename, enable_camera_capture, robot, motion_capture)
%EXECUTE_LOADED_TRAJECTORY Execute the given trajectory, optionally record the movement using cameras
%   Input format: nx7 in joint space
%   Joint speed in percent, from 0 to 1
%   author: David Gonzalez, HEPIA

execute_loaded_trajectory_async(joint_trajectory, output_filename, enable_camera_capture, robot, motion_capture);
robot.wait_motion_finished();
save_execution_data(robot, motion_capture)

end

