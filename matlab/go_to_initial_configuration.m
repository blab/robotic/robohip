function [] = go_to_initial_configuration(robot)
%GO_TO_INITIAL_POSITION Make the robot go to the saved initial configuration

disp("Going to saved initial configuration...");

robot.toggle_recording('');
robot.execute_joint_trajectory(get_saved_initial_configuration());

end

