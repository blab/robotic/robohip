function [joint_trajectory] = go_to_initial_configuration_async(robot)
%GO_TO_INITIAL_POSITION Make the robot go to the saved initial configuration

disp("Going to saved initial configuration...");

joint_trajectory = get_saved_initial_configuration();

robot.toggle_recording('');
robot.execute_joint_trajectory_async(joint_trajectory);

end

