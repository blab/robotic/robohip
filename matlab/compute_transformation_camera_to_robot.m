function [transform_c2r] = compute_transformation_camera_to_robot(robot_base_marker_positions)
%COMPUTE_TRANSFORMATION_CAMERA_TO_ROBOT Create the transform matrix used to
%                                       transform markers and 6dofs from
%                                       camera space to robot space
%   author: David Gonzalez, HEPIA

addpath(genpath('vendor'));

robot_base_marker_positions = robot_base_marker_positions * 1000;
robot_base_marker_position_bas01 = robot_base_marker_positions(1, :); % mm
robot_base_marker_position_bas02 = robot_base_marker_positions(2, :); % mm
robot_base_marker_position_bas03 = robot_base_marker_positions(3, :); % mm
robot_base_marker_position_bas04 = robot_base_marker_positions(4, :); % mm

% Set virtual marker positions based on geometric assumptions
x_base          = [1 0 0];
y_base          = [0 1 0];
z_base          = [0 0 1];
o_base          = [0.0 0.0 0.0]; % Based on technical documentation (mm)
diametre_marker = 14; % mm
virtual_marker_position_bas01 = o_base ...
                                + (154.5)*x_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                - (110)*y_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                - (1-1.4258)*z_base ... % Depth of the marker base placement on the flange plate (mm)
                                + diametre_marker/2*z_base;
virtual_marker_position_bas02 = o_base ...
                                - (185.5)*x_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                - (110)*y_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                - (1-1.4258)*z_base ... % Depth of the marker base placement on the flange plate (mm)
                                + diametre_marker/2*z_base;
virtual_marker_position_bas03 = o_base ...
                                - (185.5)*x_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                + (110)*y_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                - (1-1.4258)*z_base ... % Depth of the marker base placement on the flange plate (mm)
                                + diametre_marker/2*z_base;
virtual_marker_position_bas04 = o_base ...
                                + (154.5)*x_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                + (110)*y_base ... % Defined on CAO documentation fromm HEPIA (mm)
                                - (1-1.4258)*z_base ... % Depth of the marker base placement on the flange plate (mm)
                                + diametre_marker/2*z_base;

% Compute the rigid transformation between coordinate systems
[rotation_transform, translation_transform, ~] = soder(...
    [robot_base_marker_position_bas01;...
     robot_base_marker_position_bas02;...
     robot_base_marker_position_bas03;...
     robot_base_marker_position_bas04],...
    [virtual_marker_position_bas01;...
     virtual_marker_position_bas02;...
     virtual_marker_position_bas03;...
     virtual_marker_position_bas04]...
);
translation_transform = translation_transform / 1000;

% transform_c2r = [
%     rotation_transform(1, 1:3), translation_transform(1, 1);
%     rotation_transform(2, 1:3), translation_transform(2, 1);
%     rotation_transform(3, 1:3), translation_transform(3, 1);
%     0, 0, 0, 1
% ];
transform_c2r = [
    1, 0, 0, 0;
    0, 1, 0, 0;
    0, 0, 1, 0;
    0, 0, 0, 1
];

end

