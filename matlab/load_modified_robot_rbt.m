function [iiwaRBT] = load_modified_robot_rbt(joint_length_additions)
%LOAD_MODIFIED_ROBOT Load the robot rigid body tree with modification
%   author: David Gonzalez, HEPIA

DEGREE_TO_REMOVE = 1.0;

if ~exist('joint_length_additions', 'var')
    joint_length_additions = [0, 0, 0, 0, 0, 0, 0, 0.016, 0.041];
end

disp("Loading robot model...");

iiwaRBT = loadrobot("kukaIiwa14");
iiwaRBT.DataFormat = "row";

for i = 2:8
    % Slightly reduce joint limit because the reel robot has trouble going to its hardware limit
    rigid_body = iiwaRBT.Bodies(i);
    rigid_body{1}.Joint.PositionLimits(1) = rigid_body{1}.Joint.PositionLimits(1) + deg2rad(DEGREE_TO_REMOVE);
    rigid_body{1}.Joint.PositionLimits(2) = rigid_body{1}.Joint.PositionLimits(2) - deg2rad(DEGREE_TO_REMOVE);
    replaceJoint(iiwaRBT, rigid_body{1}.Name, rigid_body{1}.Joint);

    % Correct joint length if necessary
    correctComponentLength(iiwaRBT, i, joint_length_additions(1, i-1));
end

% Correct the end effector length to correspond to our flange: 45mm to 71mm
correctComponentLength(iiwaRBT, 10, joint_length_additions(1, 8));

tool_length = joint_length_additions(1, 9);
if tool_length > 0.0
    % Add tool to the end-effector, https://ch.mathworks.com/help/robotics/ref/rigidbody.html
    arm_interface_body = rigidBody('iiwa_link_arm_interface');
    arm_interface_jnt = rigidBodyJoint('iiwa_joint_arm_interface','fixed');
    arm_interface_body.Joint = arm_interface_jnt;
    arm_interface_collision = collisionCylinder(0.1 / 2, tool_length);
    arm_interface_collision.Pose = trvec2tform([0, 0, arm_interface_collision.Length / 2]);
    addCollision(arm_interface_body, arm_interface_collision);
    addVisual(arm_interface_body, "Mesh", "../mesh/interface.stl");

    eff = iiwaRBT.Bodies(10);
    addBody(iiwaRBT, arm_interface_body, eff{1}.Name);

    disp("Tool length added: " + num2str(tool_length));
end

% Remove collision objects for joint 5 since it is causing problem of self-collisions with joint 7,
% and the chance of being in real collision with the environnement is low since it will always be in interior
clearCollision(iiwaRBT.Bodies{6});

end

function correctComponentLength(rbt, componentIndex, lengthAddition)
    component = rbt.Bodies(componentIndex);
    tform = component{1}.Joint.JointToParentTransform;

    % find the length line position in the matrix (it differs following the component)
    col = 4;
    line = 3;
    if (mod(componentIndex, 2) == 1) || (tform(line, col) == 0)
        line = 2;
    end

    old_length = tform(line, col);
    new_length = old_length + lengthAddition;

    disp("Component " + num2str(componentIndex-1) + " length: " + num2str(old_length) + " -> " + num2str(new_length));

    tform(line, col) = new_length;
    setFixedTransform(component{1}.Joint, tform);
    replaceJoint(rbt, component{1}.Name, component{1}.Joint);
end
