function [robot_objects] = transform_objects_camera_to_robot(camera_objects, transform_c2r)
%TRANSFORM_OBJECTS_CAMERA_TO_ROBOT Transform the given markers or 6dofs
%                                  objects in camera space to robot space,
%                                  usign the given transformation matrix.
%   author: David Gonzalez, HEPIA

n_object = size(camera_objects, 1);
n_data = size(camera_objects, 2);
if n_data == 3 % marker (translation)
    robot_objects = zeros(n_object, 3);
    for i = 1:n_object
        robot_objects(i, :) = (transform_c2r(1:3, 1:3) * camera_objects(i, :)' + transform_c2r(1:3, 4))';
    end
elseif n_data == 12 % 6dof (translation followed by rotation matrix, line by line)
    robot_objects = zeros(n_object, 6);
    for i = 1:n_object
        camera_6dof_matrix = [
            camera_objects(i, 4:6), camera_objects(i, 1);
            camera_objects(i, 7:9), camera_objects(i, 2);
            camera_objects(i, 10:12), camera_objects(i, 3);
            0, 0, 0, 1
        ];
        robot_6dof_matrix = transform_c2r * camera_6dof_matrix;
        robot_objects(i, :) = [robot_6dof_matrix(1:3, 4)', rotm2eul(robot_6dof_matrix(1:3, 1:3), "XYZ")];
    end
else
    error("invalid data, only marker (translation) or 6dof (translation and rotation matrix) objects are supported")
end

end
