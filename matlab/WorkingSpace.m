classdef WorkingSpace
% WORKINGSPACE Class representing the working space of the robot
%   author: David Gonzalez, HEPIA
properties (Constant=false)
    InnerSphere = struct("Radius", 0.420, "Position", [0, 0, 0.360])
    OuterSphere = struct("Radius", 0.820, "Position", [0, 0, 0.360])
    MaxMargin = 0.0
end
methods
    function obj = WorkingSpace(margin)
        obj.MaxMargin = ((obj.OuterSphere.Radius - obj.InnerSphere.Radius) / 2) - 0.05;

        if exist('margin', 'var')
            obj.InnerSphere.Radius = obj.InnerSphere.Radius + margin;
            obj.OuterSphere.Radius = obj.OuterSphere.Radius - margin;

            if obj.InnerSphere.Radius >= obj.OuterSphere.Radius
                error("The specified margin of the working space produce a working space of size null. Please reduce the margin.")
            end
        end
    end

    function [X, Y, Z] = get_sphere(~, radius, position)
        [X, Y, Z] = sphere;
        X = (X * radius) + position(1);
        Y = (Y * radius) + position(2);
        Z = (Z * radius) + position(3);
    end
    function [X, Y, Z] = get_inner_sphere(self)
        [X, Y, Z] = self.get_sphere(self.InnerSphere.Radius, self.InnerSphere.Position);
    end
    function [X, Y, Z] = get_outer_sphere(self)
        [X, Y, Z] = self.get_sphere(self.OuterSphere.Radius, self.OuterSphere.Position);
    end

    function [length] = get_points_length(~, points)
        length = sqrt(points(:, 1).^2 + points(:, 2).^2 + points(:, 3).^2);
    end

    function [insides] = is_points_inside_sphere(~, points, sphere_radius, sphere_position)
        centred_points = points(:, 1:3) - sphere_position;
        insides = (centred_points(:, 1).^2 + centred_points(:, 2).^2 + centred_points(:, 3).^2) <= sphere_radius^2;
    end
    function [insides] = is_points_inside(self, points)
        insides = ~self.is_points_inside_sphere(points, self.InnerSphere.Radius, self.InnerSphere.Position) & ...
            self.is_points_inside_sphere(points, self.OuterSphere.Radius, self.OuterSphere.Position);
    end

    function [moved_point] = move_point_on_nearest_surface(self, point, volume)
        centred_point_inner = point(:, 1:3) - self.InnerSphere.Position;
        point_length_inner = self.get_points_length(centred_point_inner);
        point_distance_inner = self.InnerSphere.Radius - point_length_inner;

        centred_point_outer = point(:, 1:3) - self.OuterSphere.Position;
        point_length_outer = self.get_points_length(centred_point_outer);
        point_distance_outer = self.OuterSphere.Radius - point_length_outer;

        if abs(point_distance_inner) < abs(point_distance_outer)
            direction = centred_point_inner ./ point_length_inner;
            point_distance = point_distance_inner;
        else
            direction = centred_point_outer ./ point_length_outer;
            point_distance = point_distance_outer;
        end

        moved_point = point;
        while 1
            for i = 1:3
                moved_point(:, i) = min(max(moved_point(:, i) + ((point_distance / 10) * direction(:, i)), volume(1, i)), volume(2, i));
            end

            if (self.is_points_inside(moved_point))
                break
            end
        end
    end
end

end

