%% author: David Gonzalez, HEPIA
% Test pose to determine if robot pose correspond to command

close all;
clear;
clc;

rng shuffle;

cartesian_trajectory = [
    0.6, 0, 0.6, 0, 0, 0; % RX:0, RY: 0, RZ: 0
    0.6, 0, 0.6, pi/2, 0, 0; % RX: 90°
    0.6, 0, 0.3, pi, 0, 0; % RX: 180°
    0.6, 0, 0.6, pi*1.5, 0, 0; % RX: 270°
    0.6, 0, 0.6, pi*2, 0, 0; % RX: 360°
    0.6, 0, 0.6, 0, pi/2, 0; % RY: 90°
    0.6, 0, 0.6, 0, pi, 0; % RY: 180°
    0.3, 0, 0.6, 0, pi*1.5, 0; % RY: 270°
    0.6, 0, 0.6, 0, pi*2, 0; % RY: 360°
    0.6, 0, 0.6, 0, 0, pi/2; % RZ: 90°
    0.6, 0, 0.6, 0, 0, pi; % RZ: 180°
    0.6, 0, 0.6, 0, 0, pi*1.5; % RZ: 270°
    0.6, 0, 0.6, 0, 0, pi*2; % RZ: 360°
];
cartesian_point_count = size(cartesian_trajectory, 1);
if ~exist('robot', 'var')
    robot = RobotManipulatorKukaLBRIIWA();
    robot.relative_joint_speed = 0.15;
end

initial_configuration = robot.get_current_joint_configuration();

for i = 1:cartesian_point_count
    command_pose = cartesian_trajectory(i, :);
    disp("Checking pose: " + mat2str(command_pose) + "...");

    [joint_trajectory] = solve_trajectory(command_pose, initial_configuration, 0, robot.get_rbt(), get_collision_env());
    [joint_trajectory] = interpolate_trajectory(joint_trajectory, robot.get_rbt(), get_collision_env());

    disp("  executing...")
    robot.execute_joint_trajectory(joint_trajectory);
    disp("  executed")
    last_cartesian_pose = robot.get_current_pose();

    disp(mat2str(command_pose) + " ?= " + mat2str(last_cartesian_pose));

    initial_configuration = joint_trajectory(end, :);
    disp("  done " + num2str(i) + "/" + num2str(cartesian_point_count));
end

disp("  returning home...")
robot.execute_joint_trajectory([0, 0, 0, 0, 0, 0, 0]);
disp("  returned home")
