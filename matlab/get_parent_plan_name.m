function [parent_plan_name] = get_parent_plan_name(trajectory_name)
%GET_PARENT_PLAN_NAME Get the parent plan name of the given trajectory if the plan exists

traj_number = str2double(trajectory_name(1:4));

parent_plan_name = '';
parent_plan_number = -1;
plan_order = fopen('trajectories/planOrder.txt');

tline = fgetl(plan_order);
while ischar(tline)
    val = str2double(split(tline, ','));
    if val(1) == traj_number
        parent_plan_number = val(2);
        break
    end
    tline = fgetl(plan_order);
end
fclose(plan_order);

if val(2) == 0
    parent_plan_name = 'initial';
else
    trajectory_names = get_names_in_directory('trajectories');
    for i = 1:size(trajectory_names, 2)
        traj_name = trajectory_names{i};
        traj_nb = str2double(traj_name(1:4));
        if traj_nb == parent_plan_number
            parent_plan_name = traj_name;
            break
        end
    end
end
end
