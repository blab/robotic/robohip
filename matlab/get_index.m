function [value_index] = get_index(value, arr)
%GET_INDEX Get the index of the given value inside arr, -1 if not found

if iscell(arr)
    if iscellstr(arr)
        value_index = find(strcmp(arr, value), 1);
    else
        value_index = find(cell2mat(arr) == value, 1);
    end
else
    value_index = find(arr == value, 1);
end

if isempty(value_index)
    value_index = -1;
end

end

