function [points] = generate_parametric_trajectory(parametric_equation_x, parametric_equation_y, parametric_equation_z, ...
                                                   t_min, t_max, t_step, data_marker_names, fixed_rotation, ...
                                                   working_space_margin, trajectory_name, robot, motion_capture)
%GENERATE_PARAMETRIC_TRAJECTORY Generate a trajectory of the result of a 
%                               computed parametric equation
%   parametric_equation_x: parametric equation for x axis in string form
%   parametric_equation_y: parametric equation for y axis in string form
%   parametric_equation_z: parametric equation for z axis in string form
%   t_min: start value for t
%   t_max: end value for t
%   t_step: value increase between each t for each point
%   data_marker_names: list of marker names to search in marker list for equation data
%   fixed_rotation: fixed rotation to apply (let it empty to let the solver choose the best)
%   working_space_margin: Working space margin to avoid being to close to hardware limit
%   trajectory_name: name of destination file
%   Return: trajectory poses as nx6
%   authors: David Gonzalez, HEPIA

[camera_robot_base_positions, ~, ~, ~] = motion_capture.capture(robot.motion_capture_base_marker_names, {}, 100, 1, 1);
save_motion_capture_data(camera_robot_base_positions, [], robot.motion_capture_base_marker_names, {}, 'camera_capture_robot_base');

[camera_data_marker_positions, ~, ~, ~] = motion_capture.capture(data_marker_names, {}, 100, 1, 1);
save_motion_capture_data(camera_data_marker_positions, [], data_marker_names, {}, 'camera_capture_trajgen_data');
transform_c2r = compute_transformation_camera_to_robot(camera_robot_base_positions);
data_marker_positions = transform_objects_camera_to_robot(camera_data_marker_positions, transform_c2r);

points = compute_parametric_trajectory(parametric_equation_x, parametric_equation_y, parametric_equation_z, t_min, t_max, t_step, data_marker_positions);
point_count = size(points, 1);

working_space = WorkingSpace(working_space_margin);
trajectory_insides = working_space.is_points_inside(points);

if sum(~trajectory_insides) > 0
    error("Some trajectory points are outside the working space of the robot");
end

O_flange = zeros(3, 1, point_count);
for i = 1:point_count
    O_flange(1, :, i) = points(i, 1);
    O_flange(2, :, i) = points(i, 2);
    O_flange(3, :, i) = points(i, 3);
end

filename = strcat('trajectories/', trajectory_name, '.mat');
if exist('fixed_rotation', 'var') && ~isempty(fixed_rotation)
    fixed_rotation_quat = eul2quat(flip(fixed_rotation), 'XYZ');
    Q_flange = zeros(4, 1, point_count);
    for i = 1:point_count
        Q_flange(1, :, i) = fixed_rotation_quat(1, 1);
        Q_flange(2, :, i) = fixed_rotation_quat(1, 2);
        Q_flange(3, :, i) = fixed_rotation_quat(1, 3);
        Q_flange(4, :, i) = fixed_rotation_quat(1, 4);
    end

    save(filename, 'O_flange', 'Q_flange');
else
    save(filename, 'O_flange');
end

end

