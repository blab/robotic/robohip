function [adiff] = array_diff(arr1, arr2)
%ARRAY_DIFF Output the difference between two arrays using setdiff

if isempty(arr1)
    adiff = [];
elseif isempty(arr2)
    adiff = arr1;
else
    adiff = setdiff(arr1, arr2);
end

end

