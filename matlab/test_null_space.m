%% author: David Gonzalez, HEPIA
% Null space planning test scripts

close all;
clear;
clc;

if ~exist('robot', 'var')
    robot = RobotManipulatorKukaLBRIIWA();
    robot.relative_joint_speed = 0.01;
end
configuration = get_saved_initial_configuration()

robot.execute_joint_trajectory(configuration);

showdetails(robot.get_rbt());

% https://www.mathworks.com/help/robotics/ref/rigidbodytree.geometricjacobian.html
jacobian = geometricJacobian(robot.get_rbt(), configuration, 'iiwa_link_ee_kuka')
% https://www.mathworks.com/help/matlab/ref/double.svd.html
[~, ~, jacobian_svd_v] = svd(jacobian)
% https://www.mathworks.com/help/matlab/ref/rank.html
jacobian_rank = rank(jacobian)

jacobian_svd_col_count = size(jacobian_svd_v, 2)
nullspace_dimension = jacobian_svd_col_count - jacobian_rank

nullspace = zeros(jacobian_svd_col_count, nullspace_dimension)

for i = 1:nullspace_dimension
    nullspace(:, i) = jacobian_svd_v(:, jacobian_rank + i)
end

uifig = uifigure('Name', 'Null space test', 'Units', 'normalized', 'Position', [0.25/2, 0.75/2, 0.75, 0.25]);
uigrid = uigridlayout('Parent', uifig, 'RowHeight', {'1x', '1x'}, 'ColumnWidth', {'1x'});

%uiax = uiaxes(uigrid);
%ax = show(robot.get_rbt(), joint_trajectory(1, :)', 'PreservePlot', false);
%uiax.ZLim = [0, 0.8];
%uiax.YLim = [-1, 1];
%uiax.XLim = [-0.5, 1];
%view(uiax, 45, 5);

for i = 1:nullspace_dimension
    uispinner_nullspace1 = uispinner(uigrid);
    uispinner_nullspace1.Value = 0;
    uispinner_nullspace1.Limits = [-100, 100];
    uispinner_nullspace1.Step = 0.1;
    uispinner_nullspace1.ValueChangedFcn = @(btn, event) update(event.Value, i, configuration, nullspace);
end

function [] = update(value, dim_index, base_configuration, nullspace)
    new_configuration = base_configuration + value * nullspace(:, dim_index)
    robot.execute_joint_trajectory(new_configuration);
end

% https://www.mathworks.com/help/matlab/ref/double.svd.html
%[U,S,V] = svd(jacobian)
%s = diag(S);
%rank_A = nnz(s)
%column_basis = U(:,logical(s))
%null_basis = V(:,~s)
