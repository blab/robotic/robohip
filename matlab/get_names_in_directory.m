function [names] = get_names_in_directory(dirname, ext)
%GET_NAMES_IN_DIRECTORY Get all filename without extension of a given directory

if ~exist('ext', 'var')
    ext = '.mat';
end

names = {};
dirfiles = dir(strcat(dirname, '/*', ext));

for i = 1:size(dirfiles, 1)
    [~, file_name, ~] = fileparts(dirfiles(i, 1).name);
    names{end+1} = file_name;
end

end

