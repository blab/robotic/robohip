function [joint_trajectory] = execute_return_planned_trajectory_async(trajectory_name, output_filename, enable_camera_capture, robot, motion_capture)
%EXECUTE_RETURN_PLANNED_TRAJECTORY_ASYNC Load a planned trajectory and execute it in reverse, accounting for the starting position being in the middle of it.
%   Load a trajectory in joint space and make the robot follow it in reverse, accounting for the starting position being in the middle of it.
%   Also save the movement records from the robot.
%   Input format: nx7 in joint space
%   Joint speed in percent, from 0 to 1
%   author: David Gonzalez, HEPIA

disp("Executing return of planned trajectory for '" + trajectory_name + "'...");

joint_trajectory = load_return_planned_trajectory(trajectory_name, robot);
execute_loaded_trajectory_async(joint_trajectory, output_filename, enable_camera_capture, robot, motion_capture);

end
