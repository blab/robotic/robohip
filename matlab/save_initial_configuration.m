function [] = save_initial_configuration(robot)
%SAVE_INITIAL_CONFIGURATION Get the configuration of the robot and save it for planning
%   Output format: 1x7 in joint space

if ~exist('robot', 'var')
    robot = RobotManipulatorKukaLBRIIWA();
end

initial_configuration = robot.get_current_joint_configuration();

if ~isfolder('etc')
    mkdir('etc');
end
save('etc/initial_configuration.mat', 'initial_configuration');

end
