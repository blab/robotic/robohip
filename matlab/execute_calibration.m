function [calibration_data] = execute_calibration(robot, motion_capture)
%CALIBRATE Execute the calibration process, using the generated state during planning.
%          Make the robot go to random position in a defined cube and check position
%          with the motion capture in order to detect differences between
%          theoretical robot model and the real hardware one.
%   relative_joint_speed: robot relative speed
%   authors: Florent Moissenet, UNIGE
%            David Gonzalez, HEPIA

%% Get state
disp("Start executing calibration...");
calibration_filename = 'etc/calibration_state.mat';

if ~isfile(calibration_filename)
    error("No state found. Please launch 'plan_calibration' to generate a start state.");
end

calibration_state = load(calibration_filename).calibration_state;

%% Loop through the generated points at random and capture both eef position and motion capture eef position
disp("Starting execution...");

if ~exist('robot', 'var')
    robot = RobotManipulatorKukaLBRIIWA();
end

for i = calibration_state.current_i:calibration_state.n
    calibration_state.current_i = i;
    save(calibration_filename, 'calibration_state');
    disp("Calibration state saved in '" + calibration_filename + "'");

    p = calibration_state.cartesian_points(i, :);
    disp("Checking pose n°" + num2str(i) + ": " + mat2str(p) + "...");

    disp("  executing...")
    joint_trajectory = calibration_state.joint_trajectories{i};
    robot.execute_joint_trajectory(joint_trajectory);
    disp("  executed")
    pause(0.1)

    disp("  getting robot pose...")
    calibration_state.robot_eef_pose(i, :) = robot.get_current_pose();
    calibration_state.robot_joints(i, :) = joint_trajectory(end, :);
    disp("  getting camera pose...")
    [~, camera_robot_flange_pose, ~, ~] = motion_capture.capture(robot.motion_capture_flange_marker_names, robot.motion_capture_flange_6dof_names, 100, 0, 1);
    robot_flange_pose = transform_objects_camera_to_robot(camera_robot_flange_pose, calibration_state.transform_c2r);
    calibration_state.camera_eef_pose(i, :) = robot_flange_pose;

    disp("  returning home...");
    robot.execute_joint_trajectory(flip(joint_trajectory, 1));
    disp("  returned home");
    disp("  done: " + num2str(i) + " / " + num2str(calibration_state.n));
end

%% Save to file
calibration_data = struct();
calibration_data.robot_eef_pose = calibration_state.robot_eef_pose;
calibration_data.camera_eef_pose = calibration_state.camera_eef_pose;
calibration_data.robot_joints = calibration_state.robot_joints;
calibration_data.required_points = calibration_state.cartesian_points;

if ~isfolder('data')
    mkdir('data');
end

calibration_data_date = datetime('now', 'TimeZone', 'local', 'Format', 'yyyy-MM-dd-HH-mm-ss-SSS');
calibration_data_date.TimeZone = 'Z'; % Convert to UTC in place
filename = "data/calibration-" + string(calibration_data_date) + '.mat';
save(filename, 'calibration_data');

disp("Calibration data saved in '" + filename + "'");

%% Delete finished state
delete(calibration_filename);

end
