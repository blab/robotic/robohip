function [] = nonBlocking_movePTPTrajectoryJoint(t_Kuka, joint_trajectory, rel_vel, bota_use, torque_threshold)
% Set the relative joint speed, send a joint trajectory to the robot and
% make it execute it, not waiting for the end of the execution.
%   Input format: nx7 in cartesian space
%   author: David Gonzalez, HEPIA

if isempty(t_Kuka)
    return;
end

theCommand = ['jRelVel_',num2str(rel_vel),'_'];
writeline(t_Kuka, theCommand);
message = char(readline(t_Kuka));

theCommand = ['botaUse_',num2str(bota_use),'_', num2str(torque_threshold)];
writeline(t_Kuka, theCommand);
message = char(readline(t_Kuka));


theCommand = 'doPTPReleaseBrake';
writeline(t_Kuka, theCommand);
message = char(readline(t_Kuka));

rowSize = size(joint_trajectory,1);
colSize = size(joint_trajectory,2);

theCommand='doPTPTrajJS_';
for r=1:rowSize
    for c=1:colSize
        theCommand = [theCommand, sprintf('%0.5f', joint_trajectory(r,c))];
        if c < colSize
            theCommand = [theCommand, ','];
        end
    end
    
    if r < rowSize
        theCommand = [theCommand, ';'];
    end
end
writeline(t_Kuka, theCommand);
checkAcknowledgment(char(readline(t_Kuka)));

end

