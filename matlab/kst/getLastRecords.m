function [ records ] = getLastRecords( t )
%% This function is used to get the records of the last played trajectory

%% Syntax:
% [ records ] = getLastRecords( t )

%% About:
% This function is used to get the records of the last played trajectory

%% Arreguments:
% t: is the TCP/IP connection
% records: nx28
%          1: relative unix timestamp with milliseconds, where 0 is the beginning of the execution;
%          2-8: joint position in radians;
%          9-14: cartesian position (m) and orientation (rad);
%          15-21: torque of each joint (Nm);
%          22-28: external torque of each joint (Nm);

records = [];

theCommand = 'getLastRecords';
writeline(t, theCommand);
recordsStr = char(readline(t));

if ~isempty(recordsStr)
    recordsStrs = split(recordsStr, ';');
    for i = 1:size(recordsStrs, 1)
        recordStr = split(recordsStrs(i), ',');
        for j = 1:size(recordStr, 1)
            records(i, j) = str2double(recordStr(j));
        end
    end
end

end
