function [ record ] = getLastRecord( t )
%% This function is used to get the last record of the last/currently played trajectory

%% Syntax:
% [ records ] = getLastRecord( t )

%% About:
% This function is used to get the last record of the last/currently played trajectory

%% Arreguments:
% t: is the TCP/IP connection
% records: 1x28
%          1: relative unix timestamp with milliseconds, where 0 is the beginning of the execution;
%          2-8: joint position in radians;
%          9-14: cartesian position (m) and orientation (rad);
%          15-21: torque of each joint (Nm);
%          22-28: external torque of each joint (Nm);

record = [];

theCommand = 'getLastRecord';
writeline(t, theCommand);
recordsStr = char(readline(t));

if ~isempty(recordsStr)
    recordStr = split(recordsStr, ',');
    for i = 1:size(recordStr, 1)
        record(1, i) = str2double(recordStr(i));
    end
end

end
