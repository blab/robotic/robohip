function [] = movePTPTrajectoryJoint(t_Kuka, joint_trajectory, rel_vel)
% Set the relative joint speed, send a joint trajectory to the robot and
% make it execute it.
%   Input format: nx7 in cartesian space
%   author: David Gonzalez, HEPIA

nonBlocking_movePTPTrajectoryJoint(t_Kuka, joint_trajectory, rel_vel)

while ~isMotionFinished(t_Kuka)
    pause(0.5);
end

end

