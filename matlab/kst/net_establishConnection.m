function [ t ] = net_establishConnection( ip )
%% This function is used to establish a connection with the server on KUKA

%% Syntax:
% function [ t ] = net_establishConnection( ip )
% This function is used to establish a connection with the server on KUKA.

%% Arregument:
% ip: a string variable, the IP of the robot, example '172.31.1.147',
% To figure out your robot IP, do the following:
% from the teach pendant, click on 'Station' button, then the 'information'
% button, from the information page check out your KUKA iiwa IP.

%% Return value:
% t: is a TCP/IP object, this object is empty when the connection could not be established.

% Copy right, Mohammad SAFEEA, 3rd of May 2017

PORT = 30001;

t = [];
if ~exist('ip', 'var') || isempty(ip)
    error('Connection to robot IIWA could not be establised: no IP specified');
end

try
    % Huge message timeout because it avoid a bug where messages are lost
    % if the timeout is reached at the same time the server send a message.
    t = tcpclient(ip, PORT, "Timeout", 3600, "ConnectTimeout", 5);
    configureTerminator(t, 10);
    pause(1);
catch exception
    error(strcat('Connection to robot IIWA (', ip, ':', num2str(PORT), ') could not be establised: ', exception.message));
end

end

