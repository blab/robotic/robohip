function [finished] = isMotionFinished(t_Kuka)
%ISMOTIONFINISHED Ask the server if the motion is finished.
%                 If no motion is in progress, it returns true.
theCommand = 'Motion_finished';
writeline(t_Kuka, theCommand);

message = char(readline(t_Kuka));
finished = strcmpi(message, 'true') || strcmpi(message, 'on') || strcmpi(message, '1');

end

