function [] = net_turnOffServer( t )
%% This function is used to turn off the server on the robot
% function [ output_args ] = net_turnOffServer( t )
% t: is the TCP/IP connection object
% Copy right, Mohammad SAFEEA, 3rd of May 2017

if isempty(t)
    return;
end

theCommand='end';
writeline(t, theCommand);
%fclose(t);
clear t;
%message=char(readline(t));
end

