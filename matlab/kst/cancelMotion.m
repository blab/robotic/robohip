function [] = cancelMotion(t_Kuka)
%ISMOTIONFINISHED Cancel a currently executing motion.
%                 If no motion is in progress, it does nothing.
theCommand = 'Cancel_motion';
writeline(t_Kuka, theCommand);
char(readline(t_Kuka));

end

