function [issue] = botaIssue(t_Kuka)
%ISMOTIONFINISHED Ask the server if a bota issue occured.
%                 If an issue occured, it returns true.
theCommand = 'Bota_Issue';
writeline(t_Kuka, theCommand);

message = char(readline(t_Kuka));
issue = strcmpi(message, 'true') || strcmpi(message, 'on') || strcmpi(message, '1');
disp('test ' + issue)

end

