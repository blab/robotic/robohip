function [] = movePTPTrajectoryCartesianLineEefRelBase(t_Kuka, cartesian_trajectory, translation_vel, rotation_vel)
% Set the cartesian speed, send a cartesian trajectory to the robot and
% make it execute it.
%   Input format: nx6 in cartesian space: x, y, z, rx, ry, rz
%   author: David Gonzalez, HEPIA

if isempty(t_Kuka)
    return;
end

theCommand = ['EEFVel_', num2str(translation_vel), '_', num2str(rotation_vel)];
writeline(t_Kuka, theCommand);
message = char(readline(t_Kuka));

rowSize = size(cartesian_trajectory,1);
colSize = size(cartesian_trajectory,2);

theCommand='doPTPTrajCSLineRelBase_';
for r=1:rowSize
    for c=1:colSize
        theCommand = [theCommand, sprintf('%0.5f', cartesian_trajectory(r,c))];
        if c < colSize
            theCommand = [theCommand, ','];
        end
    end
    
    if r < rowSize
        theCommand = [theCommand, ';'];
    end
end
writeline(t_Kuka, theCommand); % start the point to point motion.
checkAcknowledgment(char(readline(t_Kuka)));

while ~isMotionFinished(t_Kuka)
    pause(0.5);
end

end

