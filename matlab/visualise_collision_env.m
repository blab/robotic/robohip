function [axes] = visualise_collision_env(working_space_margin, configuration, robot_rbt, env, parent_axes)
%VISUALISE_COLLISION_ENV Visualise the robot and its collision environnement (collision objects)
%   author: David Gonzalez, HEPIA

if ~exist('parent_axes', 'var')
    axes = show(robot_rbt, configuration);
else
    % robotics toolbox 'show' function doesn't support UIAxes,
    % so we create the plot on a normal figure and copy the axes over
    cla(parent_axes);
    axes = parent_axes;
    robot_fig = figure();
    robot_axes = show(robot_rbt, configuration);
    hold(robot_axes, 'on');
end

hold(axes, 'on');
zlim(axes, [-0.1, 1.5]);
ylim(axes, [-1.0, 1.0]);
xlim(axes, [-0.5, 2.0]);
if isequal(axes.View, [0, 90]) || isequal(axes.View, [135, 8])
    view(axes, 225, 5);
end

for i = 1:size(env, 2)
    if ~exist('parent_axes', 'var')
        show(env{i}, 'Parent', axes);
    else
        show(env{i});
    end
end

if exist('parent_axes', 'var')
    % Copy the normal figure and destroy it to avoid display
    copyobj(robot_axes.Children, axes);
    delete(robot_fig);
end

working_space = WorkingSpace(working_space_margin);
[XI, YI, ZI] = working_space.get_inner_sphere();
[XO, YO, ZO] = working_space.get_outer_sphere();
mesh_inner = surface(axes, XI, YI, ZI);
set(mesh_inner, 'facecolor', 'g', 'facealpha', 0.2, 'edgecolor', 'none');
mesh_outer = surface(axes, XO, YO, ZO);
set(mesh_outer, 'facecolor','g', 'facealpha', 0.2, 'edgecolor', 'none');

hold(axes, 'off');

end