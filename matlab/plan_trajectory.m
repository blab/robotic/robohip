function [last_joint_position] = plan_trajectory(trajectory_name, robot_rbt, collision_env)
%PLAN_TRAJECTORY Plan a single trajectory
%   Load a cartesian trajectory, invoke the inverse kinematics on each
%   point and save the joint trajectory to the plans folder.
%   The initual configuration is taken from the saved initial configuration
%   for the first plan, and from the previous plan for the other ones.
%   Input format: nx6 (x, y, z, rx, ry, rz) in meter and radian
%   Output format in file: nx7 in joint space
%   Return: last position 1x7 in joint space
%   author: David Gonzalez, HEPIA

disp("Planning for '" + trajectory_name + "'");

%% Delete old plan
output_file = strcat('plans/', trajectory_name, '.mat');
if isfile(output_file)
    delete(output_file)
end

%% Load cartesian trajectory
cartesian_trajectory = load_cartesian_trajectory(trajectory_name);

%% Get plan index and last planning for initial configuration
trajectory_names = get_names_in_directory('trajectories');
trajectory_index = get_index(trajectory_name, trajectory_names);

if trajectory_index == 1
    initial_configuration = get_saved_initial_configuration();
elseif ~isempty(get_parent_plan_name(trajectory_name))

    %previous_plan_name = get_previous_plan_name(trajectory_name);
    %previous_plan_filename = strcat('plans/', previous_plan_name, '.mat');
    parent_plan_name = get_parent_plan_name(trajectory_name);
    if strcmp(parent_plan_name, 'initial')
        initial_configuration = get_saved_initial_configuration();
    else
        parent_plan_filename = strcat('plans/', parent_plan_name, '.mat');
        initial_configuration = load(parent_plan_filename).joint_trajectory(end, :);
    end
else
    error("  No initial configuration available")
end

%% Planning
[joint_trajectory] = solve_trajectory(cartesian_trajectory, initial_configuration, 0, robot_rbt, collision_env);
last_joint_position = joint_trajectory(end, :);

%% Save planning
if ~isfolder('plans')
    mkdir('plans');
end

save(output_file, 'joint_trajectory')

%% Delete invalid plan
% Delete all next plans because they are obseletes, since they must follow each other
for i = (trajectory_index + 1):size(trajectory_names, 2)
    plan_path = strcat('plans/', trajectory_names{i}, '.mat');
    if isfile(plan_path)
        disp(strcat("Obsolete plan deletion: ", plan_path))
        delete(plan_path);
    end
end

disp("Planning success for '" + trajectory_name + "'");

end

