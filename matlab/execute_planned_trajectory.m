function [] = execute_planned_trajectory(trajectory_name, output_filename, enable_camera_capture, robot, motion_capture)
%EXECUTE_PLANNED_TRAJECTORY Load a planned trajectory and execute it
%   Load a trajectory in joint space and make the robot follow it.
%   Also save the movement records from the robot.
%   Input format: nx7 in joint space
%   Joint speed in percent, from 0 to 1
%   author: David Gonzalez, HEPIA

execute_planned_trajectory_async(trajectory_name, output_filename, enable_camera_capture, robot, motion_capture);
robot.wait_motion_finished();
save_execution_data(robot, motion_capture)

disp("Execution success for '" + trajectory_name + "'");

end
