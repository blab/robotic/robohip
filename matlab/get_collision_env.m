function [env] = get_collision_env()
%GET_COLLISION_ENV Get all collision objects in the robot environnement
% https://ch.mathworks.com/help/robotics/ref/rigidbody.html
env = {};

% OBJECT LIBRAIRY
iobject = 0;
% Table (1)
iobject = iobject+1;
CollisionSet.Object(iobject).sizeX   = 2.030;
CollisionSet.Object(iobject).sizeY   = 0.980;
CollisionSet.Object(iobject).sizeZ   = 0.045;
CollisionSet.Object(iobject).offsetX = 0.610;
CollisionSet.Object(iobject).offsetY = -0.245;
CollisionSet.Object(iobject).offsetZ = -0.0425;
% Smartpad (2)
iobject = iobject+1;
CollisionSet.Object(iobject).sizeX = 0.250;
CollisionSet.Object(iobject).sizeY = 0.200;
CollisionSet.Object(iobject).sizeZ = 0.530;
CollisionSet.Object(iobject).offsetX = 1.420;
CollisionSet.Object(iobject).offsetY = 0.345;
CollisionSet.Object(iobject).offsetZ = -0.110;
% LinmotX (3)
iobject = iobject+1;
CollisionSet.Object(iobject).sizeX = 0.515;
CollisionSet.Object(iobject).sizeY = 0.160;
CollisionSet.Object(iobject).sizeZ = 0.150;
CollisionSet.Object(iobject).offsetX = -0.190;
CollisionSet.Object(iobject).offsetY = -0.270;
CollisionSet.Object(iobject).offsetZ = 0.055;
% LinmotY (4)
iobject = iobject+1;
CollisionSet.Object(iobject).sizeX = 0.160;
CollisionSet.Object(iobject).sizeY = 0.515;
CollisionSet.Object(iobject).sizeZ = 0.150;
CollisionSet.Object(iobject).offsetX = 0.290;
CollisionSet.Object(iobject).offsetY = -0.320;
CollisionSet.Object(iobject).offsetZ = 0.055;
% LinmotZ (5)
iobject = iobject+1;
CollisionSet.Object(iobject).sizeX = 0.160;
CollisionSet.Object(iobject).sizeY = 0.150;
CollisionSet.Object(iobject).sizeZ = 0.515;
CollisionSet.Object(iobject).offsetX = 0.140;
CollisionSet.Object(iobject).offsetY = -0.810;
CollisionSet.Object(iobject).offsetZ = -0.110;
% Tripteron (6)
iobject = iobject+1;
CollisionSet.Object(iobject).sizeX = 0.440;
CollisionSet.Object(iobject).sizeY = 0.440;
CollisionSet.Object(iobject).sizeZ = 0.440;
CollisionSet.Object(iobject).offsetX = 0.130;
CollisionSet.Object(iobject).offsetY = -0.520;
CollisionSet.Object(iobject).offsetZ = 0.200;

% APPLY OBJECTS IN COLLISION DETECTION PROCESS
ienv = 1;
for iobject = [1,2,3,4,5,6]
    CollisionSet.Object(iobject).Collision = collisionBox(CollisionSet.Object(iobject).sizeX, ...
                                                          CollisionSet.Object(iobject).sizeY, ...
                                                          CollisionSet.Object(iobject).sizeZ);
    CollisionSet.Object(iobject).Collision.Pose = trvec2tform([CollisionSet.Object(iobject).offsetX, ...
                                                               CollisionSet.Object(iobject).offsetY, ...
                                                               CollisionSet.Object(iobject).offsetZ]);
    env{ienv} = CollisionSet.Object(iobject).Collision;
    ienv = ienv+1;
end

end