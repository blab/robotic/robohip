function [joint_trajectory] = load_planned_trajectory(trajectory_name)
%LOAD_PLANNED_TRAJECTORY Load a planned trajectory and return it
%   Output format: nx7 in joint space
%   author: David Gonzalez, HEPIA

joint_trajectory = load(strcat('plans/', trajectory_name, '.mat')).joint_trajectory;

end
