function [joint_trajectory] = cut_joint_trajectory_at_position(initial_joint_trajectory, joint_position)
%CUT_TRAJECTORY_AT_POSITION Splice the given joint trajectory at the given
%                           position so that the end of the trajectory is 
%                           closest to the given joint position.
%   Input format: nx7 in joint space
%   Position format: 1x7 in joint space
%   author: David Gonzalez, HEPIA

min_distance_i = -1;
min_distance = realmax;

for i = 1:size(initial_joint_trajectory, 1)
    dist = sqrt(sum((initial_joint_trajectory(i, :) - joint_position).^2));
    if dist < min_distance
        min_distance_i = i;
        min_distance = dist;
    end
end

if min_distance_i ~= -1
    %We take 1 point less to be sure to go in the good direction
    joint_trajectory = initial_joint_trajectory(1:min_distance_i-1, :); 
else
    joint_trajectory = initial_joint_trajectory;
end


end
