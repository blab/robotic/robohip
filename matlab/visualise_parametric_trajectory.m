function [points] = visualise_parametric_trajectory(parametric_equation_x, parametric_equation_y, parametric_equation_z, ...
                                                    t_min, t_max, t_step, data_marker_names, ...
                                                    working_space_margin, robot, collision_env, motion_capture, axes)
%VISUALISE_PARAMETRIC_TRAJECTORY Visualise the result of a computed 
%                                parametric equation
%   parametric_equation_x: parametric equation for x axis in string form
%   parametric_equation_y: parametric equation for y axis in string form
%   parametric_equation_z: parametric equation for z axis in string form
%   t_min: start value for t
%   t_max: end value for t
%   t_step: value increase between each t for each point
%   data_marker_names: list of marker names to search in marker list for equation data
%   working_space_margin: Working space margin to avoid being to close to hardware limit
%   Return: trajectory points as nx3
%   authors: David Gonzalez, HEPIA

[camera_robot_base_positions, ~, ~, ~] = motion_capture.capture(robot.motion_capture_base_marker_names, {}, 100, 1, 1);
save_motion_capture_data(camera_robot_base_positions, [], robot.motion_capture_base_marker_names, {}, 'camera_capture_robot_base');

[camera_data_marker_positions, ~, ~, ~] = motion_capture.capture(data_marker_names, {}, 100, 1, 1);
save_motion_capture_data(camera_data_marker_positions, [], data_marker_names, {}, 'camera_capture_trajgen_data');
transform_c2r = compute_transformation_camera_to_robot(camera_robot_base_positions);
data_marker_positions = transform_objects_camera_to_robot(camera_data_marker_positions, transform_c2r);

points = compute_parametric_trajectory(parametric_equation_x, parametric_equation_y, parametric_equation_z, t_min, t_max, t_step, data_marker_positions);

working_space = WorkingSpace(working_space_margin);
trajectory_insides = working_space.is_points_inside(points);

if ~exist('axes', 'var')
    axes = visualise_collision_env(working_space_margin, robot.get_home_configuration(), robot.get_rbt(), collision_env);
else
    axes = visualise_collision_env(working_space_margin, robot.get_home_configuration(), robot.get_rbt(), collision_env, axes);
end
hold(axes, 'on')
plot3(axes, data_marker_positions(:, 1)', data_marker_positions(:, 2)', data_marker_positions(:, 3)', '.m', 'MarkerSize', 24);
plot3(axes, ...
      points(trajectory_insides, 1)', points(trajectory_insides, 2)', points(trajectory_insides, 3)', '.b', ...
      points(~trajectory_insides, 1)', points(~trajectory_insides, 2)', points(~trajectory_insides, 3)', '.r');
hold(axes, 'off')
axes.Visible = 'on';

end
