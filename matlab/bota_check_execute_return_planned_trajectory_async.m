function [joint_trajectory] = bota_check_execute_return_planned_trajectory_async(trajectory_name, output_filename, enable_camera_capture, robot, motion_capture)
%BOTA_CHECK_EXECUTE_RETURN_PLANNED_TRAJECTORY_ASYNC Load a planned trajectory and execute it in reverse, accounting for the starting position being in the middle of it.
%   Load a trajectory in joint space and make the robot follow it in reverse, accounting for the starting position being in the middle of it.
%   Only execute if the BOTA had an issue with a moment. 
%   Also save the movement records from the robot.
%   Input format: nx7 in joint space
%   Joint speed in percent, from 0 to 1
%   author: Ludovic Pfeiffer, HEPIA

disp("Checking if trajectory had a bota issue...");
issue = robot.iiwa.botaIssue();
if(issue)
    disp("Bota Issue: executing return of planned trajectory for '" + trajectory_name + "'...");
    
    joint_trajectory = load_return_planned_trajectory(trajectory_name, robot);
    execute_loaded_trajectory_async(joint_trajectory, output_filename, enable_camera_capture, robot, motion_capture);
else
    disp("No issue found, continuing...");
end

end
