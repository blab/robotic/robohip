function [] = execute_loaded_trajectory_async(joint_trajectory, output_filename, enable_camera_capture, robot, motion_capture)
%EXECUTE_LOADED_TRAJECTORY_ASYNC Execute the given trajectory, optionally record the movement using cameras
%   Input format: nx7 in joint space
%   Joint speed in percent, from 0 to 1
%   author: David Gonzalez, HEPIA

if enable_camera_capture
    motion_capture.start_capture(robot.motion_capture_flange_marker_names, robot.motion_capture_flange_6dof_names, -1);
end

robot.toggle_recording(output_filename);
robot.execute_joint_trajectory_async(joint_trajectory);

end

