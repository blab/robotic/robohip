classdef MotionCaptureQualisys < handle
%MOTIONCAPTUREQUALISYS
%   author: David Gonzalez, HEPIA
properties
    sim = true;
    capture_frequency = 0; % default value, change in UI instead
    connected = false;
    
    sim_marker_names = {}; % default value, change in UI instead
    sim_six_dof_names = {}; % default value, change in UI instead
    sim_markers = []; % default value, change in UI instead
    sim_six_dofs = []; % default value, change in UI instead
    
    current_tmp_filename = '';
    current_capture_future = [];
    current_sample_count = -1;
    current_marker_names = {};
    current_six_dof_names = {};
    current_markers = [];
    current_six_dofs = [];
end

methods
    function self = MotionCaptureQualisys()
        self.set_capturing_flag(false);
    end
    
    function delete(self)
        self.disconnect();
        self.cancel_capture();
    end
    
    function [sim] = is_sim(self)
        sim = self.sim;
    end
    
    function [] = set_sim(self, sim)
        self.sim = logical(sim);
    end
    
    function [connected] = is_connected(self)
        connected = self.connected;
    end
    
    function [] = connect(self)
        if ~self.is_connected()
            QCM('connect', '127.0.0.1', 'frameinfo', '3d 6d')
            self.connected = true;
            self.set_sim(false);
        end
    end
    
    function [] = disconnect(self)
        if self.is_connected()
            QCM('disconnect')
            self.connected = false;
            self.set_sim(true);
        end
    end
    
    function [qcm_3d_labels] = get_available_3d_labels(self)
        if self.sim
            qcm_3d_labels = self.sim_marker_names;
        else
            if self.is_connected()
                qcm_3d_labels = QCM('3dlabels');
            else
                qcm_3d_labels = {};
            end
        end
    end
    
    function [qcm_6dof_labels] = get_available_6dof_labels(self)
        if self.sim
            qcm_6dof_labels = self.sim_six_dof_names;
        else
            if self.is_connected()
                qcm_6dof_labels = QCM('6doflabels');
            else
                qcm_6dof_labels = {};
            end
        end
    end
    
    function [frameinfo, the3ds, the6dofs] = get_available_values(self)
        if self.sim
            frameinfo = [];
            the3ds = self.sim_markers;
            the6dofs = self.sim_six_dofs;
            pause(1 / self.capture_frequency);
        else
            if self.is_connected()
                [frameinfo, the3ds, the6dofs] = QCM();
            else
                frameinfo = [];
                the3ds = [];
                the6dofs = [];
                pause(1 / self.capture_frequency);
            end
        end
    end

    function [capturing] = is_capturing(self)
        % We are force to use file, as 'self' is copied inside the thread by Matlab,
        % and thus, making a class variable not updatable externally.
        capturing = ~isempty(self.current_tmp_filename) && isfile(self.current_tmp_filename);
    end

    function [] = set_capturing_flag(self, newVal)
        % We are force to use file, as 'self' is copied inside the thread by Matlab,
        % and thus, making a class variable not updatable externally.
        if newVal
            self.current_tmp_filename = tempname(tempdir);
            fclose(fopen(self.current_tmp_filename, 'w'));
        else
            if ~isempty(self.current_tmp_filename) && isfile(self.current_tmp_filename)
                delete(self.current_tmp_filename);
            end
            self.current_tmp_filename = '';
        end
    end
    
    function [] = start_capture(self, marker_names, six_dof_names, sample_count)
        if self.is_capturing()
            error("Cannot launch a new motion capture when one is in progress");
        end

        disp("Starting capture...");
        
        if ~iscell(marker_names)
            marker_names = {marker_names};
        end
        self.current_marker_names = marker_names(~cellfun('isempty', marker_names)); % Remove empty cell
        if ~iscell(six_dof_names)
            six_dof_names = {six_dof_names};
        end
        self.current_six_dof_names = six_dof_names(~cellfun('isempty', six_dof_names)); % Remove empty cell
        
        self.set_capturing_flag(true);
        self.current_sample_count = sample_count;
        self.current_markers = [];
        self.current_six_dofs = [];
        self.current_capture_future = parfeval(@self.capture_data, 2, self.is_connected());
        disp("Started capture");
    end

    function [current_markers, current_six_dofs] = capture_data(self, connect)
        if connect
            self.connected = false;
            self.connect();
        end

        marker_count = size(self.current_marker_names, 2);
        six_dof_count = size(self.current_six_dof_names, 2);

        if ~isempty(self.current_marker_names)
            qcm_3d_labels = self.get_available_3d_labels();
            marker_indexes = get_indexes(self.current_marker_names, qcm_3d_labels);

            not_found_index = get_index(-1, marker_indexes);
            if not_found_index ~= -1
                error(strcat('Marker "', self.current_marker_names{not_found_index}, '" not found in QTM'));
            end
        end
        if ~isempty(self.current_six_dof_names)
            qcm_6dof_labels = self.get_available_6dof_labels();
            six_dof_indexes = get_indexes(self.current_six_dof_names, qcm_6dof_labels);

            not_found_index = get_index(-1, six_dof_indexes);
            if not_found_index ~= -1
                error(strcat('6DOF "', self.current_six_dof_names{not_found_index}, '" not found in QTM'));
            end
        end

        current_markers = zeros(marker_count, 0, 3);
        current_six_dofs = zeros(six_dof_count, 0, 12);
        i = 1;
        while ((self.current_sample_count < 1) && self.is_capturing()) || ((self.current_sample_count >= 1) && (i <= self.current_sample_count))
            [~, the3ds, the6dofs] = self.get_available_values();

            for m_i = 1:marker_count
                if marker_indexes(m_i) > 0
                    marker_value = the3ds(marker_indexes(m_i), :) / 1000;
                    current_markers(m_i, i, :) = marker_value;
                end
            end
            for s_i = 1:six_dof_count
                if six_dof_indexes(s_i) > 0
                    six_dof_value = the6dofs(six_dof_indexes(s_i), :);
                    current_six_dofs(s_i, i, :) = [six_dof_value(1, 1:3) / 1000, six_dof_value(1, 4:12)];
                end
            end

            i = i + 1;
        end

        % Avoid the thread to terminate early as this cause the future to
        % return nothing when waiting on it after thread terminaison.
        while self.is_capturing()
            pause(1);
        end
    end
    
    function [markers, six_dofs, marker_names, six_dof_names] = end_capture(self, check_nan, mean_samples)
        disp("Ending capture...");
        marker_names = self.current_marker_names;
        six_dof_names = self.current_six_dof_names;
        
        if ~self.is_capturing()
            markers = self.current_markers;
            six_dofs = self.current_six_dofs;
            return
        end
        
        self.set_capturing_flag(false);
        [idx, self.current_markers, self.current_six_dofs] = fetchNext(self.current_capture_future, 3.0);
        if isempty(idx)
            cancel(self.current_capture_future);
            error("Capture thread didn't terminate. Be aware that it may be runaway. In this case, please restart Matlab.")
        end
        self.current_capture_future = [];
        
        strerror_nan_common_note = ' If you have just launched QTM, please wait a few seconds.';
        if sum(isnan(self.current_markers), 'all') ~= 0
            disp(mat2str(cell2mat(marker_names)));
            disp(self.current_markers);

            if check_nan
                error(strcat('NaN values in markers during capture. See console for more informations.', strerror_nan_common_note));
            else
                warning(strcat('NaN values in markers during capture.', strerror_nan_common_note));
            end
        end
        if sum(isnan(self.current_six_dofs), 'all') ~= 0
            disp(mat2str(cell2mat(six_dof_names)));
            disp(self.current_six_dofs);

            if check_nan
                error(strcat('NaN values in 6dofs during capture. See console for more informations.', strerror_nan_common_note));
            else
                warning(strcat('NaN values in 6dofs during capture.', strerror_nan_common_note));
            end
        end
        
        if mean_samples
            markers = permute(mean(self.current_markers, 2, 'omitnan'), [1, 3, 2]);
            six_dofs = permute(mean(self.current_six_dofs, 2, 'omitnan'), [1, 3, 2]);
        else
            markers = self.current_markers;
            six_dofs = self.current_six_dofs;
        end

        disp("Ended capture");
    end
    
    function [markers, six_dofs, marker_names, six_dof_names] = capture(self, marker_names, six_dof_names, sample_count, check_nan, mean_samples)
        self.start_capture(marker_names, six_dof_names, sample_count);
        [markers, six_dofs, marker_names, six_dof_names] = self.end_capture(check_nan, mean_samples);
    end

    function [] = cancel_capture(self)
        self.set_capturing_flag(false);
        if ~isempty(self.current_capture_future)
            disp("Canceling capture...");
            cancel(self.current_capture_future);
            self.current_capture_future = [];
            disp("Canceled capture");
        end
    end
end
    
end
