%% author: Florent Moissenet, UNIGE; David Gonzalez, HEPIA
%

close all;
clear;
clc;

QCM('connect', '127.0.0.1', 'frameinfo', '3d')

labels6d = QCM('6doflabels');
disp(labels6d);

% Get dimensions
[frameInfos, the3ds] = QCM();

% Set variables
nframe = 100; % nb frames (100 Hz)
frameInfo = nan(size(frameInfos,1),1,nframe);
the3d = nan(size(the3ds,1),3,nframe);
clear frameInfos the3ds;

% Get data
for i = 1:nframe
    [frameInfo(:,:,i), the3d(:,:,i)] = QCM();
end

figure();
for i = 1:nframe
    for imarker = size(the3d,1)-4:size(the3d,1)
        if imarker == size(the3d,1)-4
            plot3(the3d(imarker,1,i),the3d(imarker,2,i),the3d(imarker,3,i),'Marker','x');
            hold on; axis equal;
            xlim([-1000 1000]);
            ylim([-1000 1000]);
            zlim([-100 500]);
        end
        plot3(the3d(imarker,1,i),the3d(imarker,2,i),the3d(imarker,3,i),'Marker','x');
    end
    pause(0.01);
    clf;
    hold off;
end

QCM('disconnect')
