function [moved_points, k] = move_points_inside_working_space(points, n, volume, working_space_margin)
%MOVE_POINTS_INSIDE_WORKING_SPACE For each points, determine if they are
%                                 inside the working space (including margin).
%                                 If not, they are removed.
%   author: David Gonzalez, HEPIA

working_space = WorkingSpace(working_space_margin);
point_insides = working_space.is_points_inside(points);

k = 0;
for i = 1:size(points, 1)
    if point_insides(i)
        moved_points(k+1, :) = points(i, :);
        k = k+1;
    end
end

if n ~= k
    warning(num2str(n - k) + " points have been removed: " + num2str(n) + " -> " + num2str(k))
end

end

