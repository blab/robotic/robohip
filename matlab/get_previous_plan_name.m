function [previous_plan_name] = get_previous_plan_name(trajectory_name)
%GET_PREVIOUS_PLAN_NAME Get the previous plan name of the given trajectory if the plan exists

previous_plan_name = '';

trajectory_names = get_names_in_directory('trajectories');
trajectory_index = get_index(trajectory_name, trajectory_names);
previous_trajectory_index = trajectory_index - 1;
if previous_trajectory_index > 0
    previous_trajectory_name = trajectory_names(previous_trajectory_index);
    previous_trajectory_name = previous_trajectory_name{1};
    previous_plan_filename = strcat('plans/', previous_trajectory_name, '.mat');
    if isfile(previous_plan_filename)
        previous_plan_name = previous_trajectory_name;
    end
end

end

