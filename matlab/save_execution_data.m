function [] = save_execution_data(robot, motion_capture)
%SAVEEXECUTIONDATA Save the records of the robot and the capture data of the motion capture
%   author: David Gonzalez, HEPIA

output_filename = robot.get_recording_filename();

if ~isempty(output_filename)
    save_last_records(robot);
    
    if motion_capture.is_capturing()
         [markers, six_dofs, marker_names, six_dof_names] = motion_capture.end_capture(0, 0);
         save_motion_capture_data(markers, six_dofs, marker_names, six_dof_names, output_filename + '-camera');
    end
end

end

