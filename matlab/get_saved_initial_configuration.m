function [initial_configuration] = get_saved_initial_configuration()
%GET_INITIAL_POSITION Get the saved initial joint position in 1x7 format

initial_configuration = zeros(1, 7);
if isfile('etc/initial_configuration.mat')
    initial_configuration = load('etc/initial_configuration.mat').initial_configuration;
end

end

