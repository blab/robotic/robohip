function [] = visualise_planned_trajectory(trajectory_name, robot_rbt)
%VISUALISE_PLANNED_TRAJECTORY Visualise a planned trajectory in a plot.
%This is not a simulation.
%   author: David Gonzalez, HEPIA

%% Load planned trajectory (in row format)
joint_trajectory = load(strcat('plans/', trajectory_name, '.mat')).joint_trajectory;

%% Interpolate
framerate = 30; % in herz
exec_time = 20; % in second
r = rateControl(framerate);
interpolated_joint_trajectory_size = [0, linspace(exec_time / 2, exec_time, size(joint_trajectory, 1) - 1)];
numFrames = exec_time * framerate;
interpolated_joint_trajectory = pchip(interpolated_joint_trajectory_size, joint_trajectory', linspace(0, exec_time, numFrames))';

%% Load robot
if ~exist('robot_rbt', 'var')
    robot_rbt = load_modified_robot_rbt();
end

%% Show
figure('Units', 'normalized', 'Position', [0 0 1 1]);
ax = show(robot_rbt, joint_trajectory(1, :), 'PreservePlot', false);
ax.ZLim = [0, 0.8];
ax.YLim = [-1, 1];
ax.XLim = [-0.5, 1];
view(ax, 45, 5)
hold on

for k = 1:size(interpolated_joint_trajectory, 1)
    show(robot_rbt, interpolated_joint_trajectory(k, :), 'PreservePlot', false);
    waitfor(r);
end
hold off

end

