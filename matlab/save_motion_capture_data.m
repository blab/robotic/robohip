function [] = save_motion_capture_data(markers, six_dofs, marker_names, six_dof_names, filename)
%SAVEMOTIONCAPTUREDATA Save to disk the given results of motion capture.

if ~exist('filename', 'var') || isempty(filename)
    camera_capture_date = datetime('now', 'TimeZone', 'local', 'Format', 'yyyy-MM-dd-HH-mm-ss-SSS');
    camera_capture_date.TimeZone = 'Z'; % Convert to UTC in place
    filename = 'motion_capture-' + string(camera_capture_date);
end

motion_capture_data = struct();
motion_capture_data.markers = markers;
motion_capture_data.six_dofs = six_dofs;
motion_capture_data.marker_names = marker_names;
motion_capture_data.six_dof_names = six_dof_names;

if ~isfolder('data')
    mkdir('data');
end

output_filename = strcat('data/', filename, '.mat');
save(output_filename, 'motion_capture_data');
disp("Motion capture data saved in '" + output_filename + "'");

end
