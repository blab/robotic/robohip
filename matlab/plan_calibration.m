function [] = plan_calibration(n, height, height_bottom_offset, volume_marker_names, working_space_margin, try_count, robot, motion_capture, collision_env)
%CALIBRATE Plan the calibration process.
%          Make the robot go to random position in a defined cube and check position
%          with the motion capture in order to detect differences between
%          theoretical robot model and the real hardware one.
%   n: number of points to test
%   height: Height of the volume (meters)
%   height_bottom_offset: Offset of the start of the volume from the marker (meters)
%   volume_marker_names: list of four marker names to search in marker list for volume position
%   working_space_margin: Working space margin to avoid being to close to hardware limit
%   authors: Florent Moissenet, UNIGE
%            David Gonzalez, HEPIA

%% Create state and check if one already exist
disp("Start planning calibration...");
calibration_filename = 'etc/calibration_state.mat';

calibration_state = struct();

if isfile(calibration_filename)
    calibration_state = load(calibration_filename).calibration_state;

    if calibration_state.planned
        answer = questdlg('State found on disk ? Would you like to replace it ?', 'State found', 'Yes', 'Cancel', 'Cancel');
        switch answer
            case 'Yes'
                delete(calibration_filename);
            otherwise
                disp("Calibration planning canceled");
                return;
        end
    end
end

if ~isfile(calibration_filename)
    calibration_state.transform_c2r = [];
    calibration_state.n = 0;
    calibration_state.joint_trajectories = {};
    calibration_state.cartesian_points = [];
    calibration_state.robot_eef_pose = [];
    calibration_state.camera_eef_pose = [];
    calibration_state.robot_joints = [];
    calibration_state.current_i = 1;
    calibration_state.planned = false;
    
    %% Compute transformations
    disp("Get robot base positions...")
    [camera_robot_base_positions, ~, ~, ~] = motion_capture.capture(robot.motion_capture_base_marker_names, {}, 100, 1, 1);
    save_motion_capture_data(camera_robot_base_positions, [], robot.motion_capture_base_marker_names, {}, 'camera_capture_robot_base');
    
    disp("Computing transform camera -> robot...")
    calibration_state.transform_c2r = compute_transformation_camera_to_robot(camera_robot_base_positions);
    
    %% Compute cube with height and bottom offset
    disp("Get volume positions...")
    [camera_volume_marker_positions, ~, ~, ~] = motion_capture.capture(volume_marker_names, {}, 100, 1, 1);
    save_motion_capture_data(camera_volume_marker_positions, [], volume_marker_names, {}, 'camera_capture_cal_volume');
    volume_minmax_positions = transform_objects_camera_to_robot(camera_volume_marker_positions, calibration_state.transform_c2r);
    
    [n, volume, points] = compute_points_in_volume(n, volume_minmax_positions, height, height_bottom_offset);
    [points, n] = move_points_inside_working_space(points, n, volume, working_space_margin);
    calibration_state.n = n;
    
    % Calibration loop initialisation
    calibration_state.robot_eef_pose = zeros(calibration_state.n, 6);
    calibration_state.camera_eef_pose = zeros(calibration_state.n, 6);
    calibration_state.robot_joints = zeros(calibration_state.n, 7);
    
    %% Plan all points, using a random initial guess to generate different final configuration (rotation are ignored)
    calibration_state.cartesian_points = points;
end

% Interpolate each joint configuration, always starting from the home configuration
iiwa_rbt = robot.get_rbt();
rng shuffle;

for i = calibration_state.current_i:calibration_state.n
    calibration_state.current_i = i;
    save(calibration_filename, 'calibration_state');
    disp("Calibration intermediary state saved in '" + calibration_filename + "'");

    done = false;
    for t = 1:try_count
        try
            disp("Planning point " + num2str(i) + ", try number " + num2str(t) + " / " + num2str(try_count));

            [joint_configs] = solve_trajectory(...
                [calibration_state.cartesian_points(i, :), round(2*rand(1)-1)*pi/2*rand(), round(2*rand(1)-1)*pi/2*rand(), round(2*rand(1)-1)*pi/2*rand()], ...
                [0, 0, 0, 0, 0, 0, 0], 1, iiwa_rbt, collision_env ...
            );
            [joint_trajectory] = interpolate_trajectory(joint_configs, iiwa_rbt, collision_env);
            calibration_state.joint_trajectories{end+1} = joint_trajectory;
            
            done = true;
            disp("  done: " + num2str(i) + " / " + num2str(calibration_state.n));
            break;
        catch
            % just retry with a new orientation
        end
    end
    if ~done
        error("No valid path found to point " + num2str(i) + " " + mat2str(calibration_state.cartesian_points(i, :)));
    end
end

calibration_state.current_i = 1;
calibration_state.planned = true;
save(calibration_filename, 'calibration_state');
disp("Calibration final state saved in '" + calibration_filename + "'");

end
