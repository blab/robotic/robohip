function [interpolated_joint_trajectory] = interpolate_trajectory(joint_trajectory, robot_rbt, collision_env)
%INTERPOLATE_TRAJECTORY Add more intermediate points to the given
%                       trajectory and check collision along the way
%   Input format: nx7 in joint space

disp("Interpolating trajectory and avoiding collision...");

interpolated_joint_trajectory = [];

rng shuffle;

rrt_planer = manipulatorRRT(robot_rbt, collision_env);
rrt_planer.SkippedSelfCollisions = "adjacent";
rrt_planer.ValidationDistance = 0.01;

for i = 1:(size(joint_trajectory, 1) - 1)
    try
        [detailed_path, solution_info] = plan(rrt_planer, joint_trajectory(i, :), joint_trajectory(i+1, :));
        if ~solution_info.IsPathFound
            throw(MException('manipulatorplanning:noPathFound',...
                             'No path found. Either there is a collision or there is no possible path.'))
        end
        
        interpolated_joint_trajectory = [interpolated_joint_trajectory; interpolate(rrt_planer, detailed_path)];
    catch exc
        str_point1 = strcat("joint configuration ", num2str(i), " ", mat2str(joint_trajectory(i, :)));
        str_point2 = strcat("joint configuration ", num2str(i+1), " ", mat2str(joint_trajectory(i+1, :)));
        error(strcat("Error between ", str_point1, " and ", str_point2, ": ", exc.message));
    end
end

end

