-- Path execution script
-- David Gonzalez, HEPIA

JOINT_COUNT = 7
JOINT_DIFF_NEXT_TARGET = 0.001

joints = {}
trajectory = {}
trajectory_i = 1

function sysCall_init()
    for i = 1, JOINT_COUNT, 1
    do
        joints[i] = sim.getObjectHandle("LBR_iiwa_14_R820_joint" .. i)
        print(i .. " -> " .. sim.getObjectName(joints[i]))
    end
    print(joints)
end

function sysCall_actuation()
    local trajectory_pack = sim.getStringSignal("trajectory")
    if trajectory_pack ~= nil
    then
        sim.clearStringSignal("trajectory")
        trajectory = sim.unpackFloatTable(trajectory_pack)
        trajectory_i = 1
        sim.setIntegerSignal("trajectory_i", (trajectory_i - 1) / JOINT_COUNT)
        
        setCurrentJointTarget()
    end
    
    if trajectory ~= nil
    then
        if trajectory_i < table.getn(trajectory)
        then
            -- Test if all joint positions are sufficiently near their target
            for i = 1, JOINT_COUNT, 1
            do
                local joint_pos = sim.getJointPosition(joints[i])
                if math.abs(joint_pos - trajectory[trajectory_i + i - 1]) > JOINT_DIFF_NEXT_TARGET
                then
                    return
                end
            end
            
            -- If we are here, all joint are sufficiently near
            -- so we can go to the next
            trajectory_i = trajectory_i + JOINT_COUNT
            sim.setIntegerSignal("trajectory_i", (trajectory_i - 1) / JOINT_COUNT)
            
            if trajectory_i < table.getn(trajectory)
            then
                setCurrentJointTarget()
            else
                print("finish: " .. trajectory_i .. " - " .. table.getn(trajectory))
                sim.pauseSimulation()
            end
        end
    else
        print("finish: no trajectory")
        sim.pauseSimulation()
    end
end

function sysCall_resume()
    trajectory_i = 1
    setCurrentJointTarget()
end

function sysCall_cleanup()
    
end

function setCurrentJointTarget()
    if trajectory ~= nil
    then
        for i = 1, JOINT_COUNT, 1
        do
            sim.setJointTargetPosition(joints[i], trajectory[trajectory_i + i - 1])
        end
    end
end
