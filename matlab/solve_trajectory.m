function [joint_trajectory] = solve_trajectory(cartesian_trajectory, initial_configuration, random_initial_guess, robot_rbt, collision_env)
%SOLVE_TRAJECTORY Solve a cartesian trajectory into a joint trajectory
%   Input format: nx6 (x, y, z, rx, ry, rz) in meter and radian
%   Initial configuration format: 1x7 in joint space
%   Output format: nx7 in joint space
%   author: David Gonzalez, HEPIA

% Control the max distance allowed between joint configuration before
% detecting reconfiguration.
% The idea is that each point should be almost equidistant, both in 3D and joint space.
% If a reconfiguration happen, then the distance will be far greater,
% far greater that the mean for the entire trajectory.
MAX_DISTANCE_FACTOR = 6;

% https://ch.mathworks.com/help/robotics/ug/inverse-kinematics-algorithms.html
solver_parameters = struct("EnforceJointLimits", 1);
ik = inverseKinematics('RigidBodyTree', robot_rbt, "SolverAlgorithm", "BFGSGradientProjection", "SolverParameters", solver_parameters);

%% Planning
disp("Invoking Inverse kinematic on each point and checking collisions...");

rng shuffle;
path_point_count = size(cartesian_trajectory, 1);
weights = [1 1 1 1 1 1];
initial_guess = initial_configuration;
joint_trajectory = []; % no prealloc since we can have incomplete trajectory
solution_info_pose_error_norms = zeros(path_point_count, 1);
distances_with_previous = zeros(path_point_count, 1);

% If no rotation is specified, add it and tell the solver to ignore the rotation
if size(cartesian_trajectory, 2) == 3
    cartesian_trajectory = [cartesian_trajectory, repmat([0, 0, 0], size(cartesian_trajectory, 1), 1)];
    weights = [0 0 0 1 1 1];
end

% Add the initial config to the trajectory to have a startup
joint_trajectory(1, :) = initial_configuration;

for i = 1:path_point_count
    translation = [cartesian_trajectory(i, 1) cartesian_trajectory(i, 2) cartesian_trajectory(i, 3)];
    rot = [cartesian_trajectory(i, 4), cartesian_trajectory(i, 5), cartesian_trajectory(i, 6)];
    transform_matrix = makehgtform('translate', translation, 'xrotate', rot(1), 'yrotate', rot(2), 'zrotate', rot(3));
    
    [joint_position, solution_info] = ik('iiwa_link_ee_kuka', transform_matrix, weights, initial_guess);
    solution_info_pose_error_norms(i, 1) = solution_info.PoseErrorNorm;
    distances_with_previous(i, 1) = sqrt(sum((joint_position - initial_guess).^2));

    if solution_info.Status == "success"
        [isColliding, ~, ~] = checkCollision(robot_rbt, joint_position, collision_env, SkippedSelfCollisions="adjacent");
        if isColliding
            error(strcat(...
                "Solver detected a collision at point " + i + " " + mat2str(cartesian_trajectory(i, :)),...
                " with found configuration " + mat2str(joint_position)...
            ));
        end
        
        joint_trajectory(i+1, :) = joint_position;
        if random_initial_guess
            initial_guess = randomConfiguration(robot_rbt);
        else
            % We use the computed joint position as the initial guess of the next iteration,
            % so that the path is coherent.
            initial_guess = joint_position;
        end
    else
        warning(formattedDisplayText(solution_info));
        error("Solver failed at point " + i + ": " + mat2str(cartesian_trajectory(i, :)) + ". See console for details.");
    end
    
    % Show progress each 10%
    if mod(i, round(path_point_count / 10)) == 0
        disp("  " + num2str(i) + "/" + num2str(path_point_count) + " - " + num2str(round(i * 100 / path_point_count)) + "%");
    end
end

%% Reconfiguration detection
if (path_point_count > 1) && (~random_initial_guess)
    disp("Detecting reconfiguration...")

    mean_distances_with_previous = mean(distances_with_previous);
    max_distance_allowed = mean_distances_with_previous * MAX_DISTANCE_FACTOR;
    disp("  mean distance: " + mean_distances_with_previous);
    disp("  max distance allowed: " + max_distance_allowed);
    for i = 1:size(distances_with_previous, 1)
        if distances_with_previous(i) > max_distance_allowed
            msg = strcat(...
                "Solver detected reconfiguration (distance: " + distances_with_previous(i),...
                ") at point " + i + ": " + mat2str(cartesian_trajectory(i, :))...
            );

            if i > 1
                error(msg);
            else
                warning(msg);
                disp("Reconfiguration detection at point 1 is particular: ");
                disp("  if this is the first plan, then your initial configuration is too far from the first point;");
                disp("  if this is the other ones, then your trajectories don't follow each other correctly or are not close enough.");
            end
        end
    end
end

%% Metrics
if (path_point_count > 1) && (~random_initial_guess)
    disp(strcat("Pose error (m): ", num2str(mean(solution_info_pose_error_norms)), " +/- ", num2str(std(solution_info_pose_error_norms))))
end

end

