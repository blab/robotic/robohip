classdef TeleOperator < handle
% TELEOPERATOR UI and interface for operator
%   author: David Gonzalez, HEPIA
properties (Constant=false)
    robot = [];
    motion_capture = [];

    uifig = [];
    textsize = 14;
    title_textsize = 18;
    uitabs = [];
    uitab_main = [];
    uitab_advopts = [];
    uitab_cal = [];
    uiaxes_robot_cal = [];
    uitab_trajgen = [];
    uiaxes_robot_trajgen = [];
    uitab_exec = [];
    title_label = [];
    uiaxes_frequency = [];
    cancel_btn = [];
    record_labels = {};
    record_uiaxes = {};
    record_size = 0;
    cancel_requested = false;
    fct_start_time = 0;
    fct_tic_value = 0;
    tic_value = 0;

    pool = [];
end
methods
    function self = TeleOperator()
        self.pool = gcp('nocreate');
        if isempty(self.pool)
            self.pool = parpool('Processes');
        end
        self.robot = RobotManipulatorKukaLBRIIWA();
        self.motion_capture = MotionCaptureQualisys();
        self.create_ui();
    end

    function delete(self)
        self.robot.cancel_motion();
        self.robot.disconnect();
        self.robot.unload_simulation();
        self.motion_capture.cancel_capture();
    end
    
    function [opt] = load_options(self)
        opt = struct();
        if isfile('etc/teleop_config.mat')
            opt = load('etc/teleop_config.mat').opt;
        end
        
        if ~isfield(opt, 'session_name')
            opt.session_name = '%d-sessionname-%t-%c';
        end
        if ~isfield(opt, 'relative_joint_speed')
            opt.relative_joint_speed = self.robot.relative_joint_speed;
        end
        if ~isfield(opt, 'execution_cycle_count')
            opt.execution_cycle_count = 1;
        end
        if ~isfield(opt, 'execution_update_frequency')
            opt.execution_update_frequency = 2;
        end
        if ~isfield(opt, 'bota_use')
            opt.bota_use = 0;
        end
        if ~isfield(opt, 'automatic_return')
            opt.automatic_return = 0;
        end  
        if ~isfield(opt, 'automatic_return_threshold')
            opt.automatic_return_threshold = 0;
        end
        if ~isfield(opt, 'torque_threshold')
            opt.torque_threshold = self.robot.torque_threshold;
        end
        if ~isfield(opt, 'working_space_margin')
            opt.working_space_margin = 0.0;
        end
        if ~isfield(opt, 'enable_camera_capture')
            opt.enable_camera_capture = 1;
        end
        if ~isfield(opt, 'camera_capture_robot_flange_6dof_names')
            opt.camera_capture_robot_flange_6dof_names = self.robot.motion_capture_flange_6dof_names;
        end
        if ~isfield(opt, 'camera_capture_robot_base_marker_names')
            opt.camera_capture_robot_base_marker_names = self.robot.motion_capture_base_marker_names;
        end
        if ~isfield(opt, 'camera_capture_robot_flange_marker_names')
            opt.camera_capture_robot_flange_marker_names = self.robot.motion_capture_flange_marker_names;
        end
        if ~isfield(opt, 'joint_length_additions')
            opt.joint_length_additions = self.robot.joint_length_additions;
        end
        if ~isfield(opt, 'camera_capture_frequency')
            opt.camera_capture_frequency = self.motion_capture.capture_frequency;
        end
        if ~isfield(opt, 'camera_capture_simulated_marker_names')
            opt.camera_capture_simulated_marker_names = self.motion_capture.sim_marker_names;
        end
        if ~isfield(opt, 'camera_capture_simulated_six_dofs_names')
            opt.camera_capture_simulated_six_dofs_names = self.motion_capture.sim_six_dof_names;
        end
        if ~isfield(opt, 'camera_capture_simulated_markers')
            opt.camera_capture_simulated_markers = self.motion_capture.sim_markers;
        end
        if ~isfield(opt, 'camera_capture_simulated_six_dofs')
            opt.camera_capture_simulated_six_dofs = self.motion_capture.sim_six_dofs;
        end
        if ~isfield(opt, 'calibration_n')
            opt.calibration_n = 1000;
        end
        if ~isfield(opt, 'calibration_height')
            opt.calibration_height = 0.3;
        end
        if ~isfield(opt, 'calibration_height_bottom_offset')
            opt.calibration_height_bottom_offset = 0.5;
        end
        if ~isfield(opt, 'calibration_volume_marker_names')
            opt.calibration_volume_marker_names = self.motion_capture.sim_marker_names(1, 5:8);
        end
        if ~isfield(opt, 'calibration_plan_try_count')
            opt.calibration_plan_try_count = 10;
        end
        if ~isfield(opt, 'trajgen_name')
            trajgen_name_date = datetime('now', 'TimeZone', 'local', 'Format', 'yyyy-MM-dd-HH-mm-ss-SSS');
            trajgen_name_date.TimeZone = 'Z'; % Convert to UTC in place
            opt.trajgen_name = string(trajgen_name_date) + '-trajectory';
        end
        if ~isfield(opt, 'trajgen_parametric_equation_x')
            opt.trajgen_parametric_equation_x = '';
        end
        if ~isfield(opt, 'trajgen_parametric_equation_y')
            opt.trajgen_parametric_equation_y = '';
        end
        if ~isfield(opt, 'trajgen_parametric_equation_z')
            opt.trajgen_parametric_equation_z = '';
        end
        if ~isfield(opt, 'trajgen_t_min')
            opt.trajgen_t_min = 0;
        end
        if ~isfield(opt, 'trajgen_t_max')
            opt.trajgen_t_max = 1;
        end
        if ~isfield(opt, 'trajgen_t_step')
            opt.trajgen_t_step = 0.1;
        end
        if ~isfield(opt, 'trajgen_data_marker_names')
            opt.trajgen_data_marker_names = {'P01', 'P02', 'P03', 'P04'};
        end
        if ~isfield(opt, 'trajgen_rotation_type')
            opt.trajgen_rotation_type = '';
        end
        if ~isfield(opt, 'trajgen_rotation_fixed')
            opt.trajgen_rotation_fixed = [0, pi/2, 0];
        end
        
        self.robot.relative_joint_speed = opt.relative_joint_speed;
        self.robot.bota_use = opt.bota_use;
        self.robot.torque_threshold = opt.torque_threshold;
        self.robot.motion_capture_base_marker_names = opt.camera_capture_robot_base_marker_names;
        self.robot.motion_capture_flange_marker_names = opt.camera_capture_robot_flange_marker_names;
        self.robot.motion_capture_flange_6dof_names = opt.camera_capture_robot_flange_6dof_names;
        self.robot.joint_length_additions = opt.joint_length_additions;
        self.motion_capture.capture_frequency = opt.camera_capture_frequency;
        self.motion_capture.sim_marker_names = opt.camera_capture_simulated_marker_names;
        self.motion_capture.sim_six_dof_names = opt.camera_capture_simulated_six_dofs_names;
        self.motion_capture.sim_markers = opt.camera_capture_simulated_markers;
        self.motion_capture.sim_six_dofs = opt.camera_capture_simulated_six_dofs;
    end

    function [] = save_config(self, save_name, save_value)
        opt = self.load_options();
        opt.(save_name) = save_value;

        if ~isfolder('etc')
            mkdir('etc');
        end
        save('etc/teleop_config.mat', 'opt');
    end

    function [output_filename] = format_output_filename(~, session_name, trajectory_name, count_str, suffix)
        output_filename = strrep(strrep(session_name, '%c', count_str), '%t', trajectory_name);
        output_filename_date = datetime('now', 'TimeZone', 'local', 'Format', 'yyyy-MM-dd-HH-mm-ss-SSS');
        output_filename_date.TimeZone = 'Z'; % Convert to UTC in place
        output_filename = strrep(output_filename, '%d', string(output_filename_date)) + suffix;
    end

    function [env] = get_collision_env(~)
        env = get_collision_env();
    end

    function [] = on_cancel(self)
        self.robot.cancel_motion();
        self.motion_capture.cancel_capture();
    end

    function [] = create_ui(self)
        if ~isfolder('trajectories')
            mkdir('trajectories');
        end

        self.uifig = uifigure('Name', 'ROBIOM Teleoperator', 'Units', 'normalized', 'Position', [0, 0.10/2, 1, 0.90]);
        self.uifig.CloseRequestFcn = @(fig, event) self.on_close();
        uigrid_tab = uigridlayout('Parent', self.uifig, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x'});

        self.uitabs = uitabgroup(uigrid_tab);
        self.uitab_main = uitab(self.uitabs, 'Title', 'Main');
        self.uitab_advopts = uitab(self.uitabs, 'Title', 'Advance options');
        self.uitab_cal = uitab(self.uitabs, 'Title', 'Calibration');
        self.uitab_trajgen = uitab(self.uitabs, 'Title', 'Trajectory generation');
        self.uitab_exec = uitab(self.uitabs, 'Title', 'Execution');

        uigrid = uigridlayout('Parent', self.uitab_main, 'RowHeight', {'fit','fit','fit', '1x'}, 'ColumnWidth', {'1x','1x'});
        uipanel_menu = uipanel('Parent', uigrid, 'Title', 'Menu', 'FontSize', self.title_textsize);
        uipanel_menu.Layout.Column = [1 2];
        uigrid_menu = uigridlayout('Parent', uipanel_menu, 'RowHeight', {'fit', 'fit'}, 'ColumnWidth', {'2x','1x'});

        uibutton(uigrid_menu, 'state', 'Text', 'Connect to hardware robot', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ValueChangedFcn', @(btn, event) self.toggle_robot_connection(btn));
        uibutton(uigrid_menu, 'push', 'Text', 'Open trajectories directory', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.open_trajectories_directory());
        uibutton(uigrid_menu, 'state', 'Text', 'Connect to motion capture hardware', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ValueChangedFcn', @(btn, event) self.toggle_motion_capture_connection(btn));
        uibutton(uigrid_menu, 'push', 'Text', 'Open operator documentation', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.open_guide());

        uibutton(uigrid_menu, 'push', 'Text', 'Refresh', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.update_uipanels());

        uipanel_opt = uipanel('Parent', uigrid, 'Title', 'Options', 'FontSize', self.title_textsize);
        uipanel_opt.Layout.Column = [1 2];

        uipanel_auto_return = uipanel('Parent', uigrid, 'Title', 'Automatic return', 'FontSize', self.title_textsize);
        uipanel_auto_return.Layout.Column = [1 2];
        uipanel('Parent', uigrid, 'Title', 'Planning', 'FontSize', self.title_textsize);
        uipanel('Parent', uigrid, 'Title', 'Execution', 'FontSize', self.title_textsize);

        self.update_uipanels();
        self.update_ui_advance_options();
        self.update_ui_calibration();
        self.update_ui_trajectory_generation();
        self.update_ui_execution();
        
        drawnow;
        if isvalid(self.uifig)
            self.uifig.WindowState = 'maximized';
        end
    end

    function [] = update_uipanels(self)
        uifig_children = allchild(self.uitab_main);
        uigrid = uifig_children(1);
        uigrid_children = allchild(uigrid);
        uipanel_opt = uigrid_children(2);
        uipanel_auto_return = uigrid_children(3);
        uipanel_planning = uigrid_children(4);
        uipanel_execution = uigrid_children(5);

        self.update_uipanel_options(uipanel_opt);
        self.update_uipanel_autoreturn(uipanel_auto_return);
        self.create_uipanel_planning(uipanel_planning);
        self.create_uipanel_execution(uipanel_execution);
    end

    function [] = update_uipanel_autoreturn(self, uipanel_auto_return)
        opt = self.load_options();
        uigrid_autoreturn = uigridlayout('Parent', uipanel_auto_return, 'RowHeight', {'fit'}, 'ColumnWidth', {'1x','2x', '1x', '1x'});

        uigrid_autoreturn_end = uigridlayout('Parent', uigrid_autoreturn, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x'}, ...
                                           'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uicheckbox(uigrid_autoreturn_end, 'Text', 'Automatically return after trajectory completion', 'FontSize', self.textsize, ...
                   'WordWrap', 'on', 'Value', opt.automatic_return, ...
                   'ValueChangedFcn', @(src, event) self.save_config('automatic_return', event.Value));
        

        uigrid_autoreturn_threshold = uigridlayout('Parent', uigrid_autoreturn, 'RowHeight', {'fit'}, 'ColumnWidth', {'2x', 'fit', 'fit'}, ...
                                           'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uicheckbox(uigrid_autoreturn_threshold, 'Text', 'Automatically return if resistive torque threshold reached', 'FontSize', self.textsize, ...
                   'WordWrap', 'on', 'Value', opt.automatic_return_threshold, ...
                   'ValueChangedFcn', @(src, event) self.save_config('automatic_return_threshold', event.Value));
        uilabel(uigrid_autoreturn_threshold, 'Text', 'Torque threshold (mNm) :', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_torque_threshold = uispinner(uigrid_autoreturn_threshold, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('torque_threshold', event.Value));
        uispinner_torque_threshold.Value = opt.torque_threshold;
        uispinner_torque_threshold.Limits = [-1000000, 1000000];
        uispinner_torque_threshold.Step = 1;

    end

    function [] = update_uipanel_options(self, uipanel_opt)
        opt = self.load_options();
        uigrid_opt = uigridlayout('Parent', uipanel_opt, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x','1x','1x','1x','1x'});

        uigrid_session_name = uigridlayout('Parent', uigrid_opt, 'RowHeight', {'fit','1x'}, 'ColumnWidth', {'1x'}, ...
                                           'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_session_name, 'Text', 'Session name:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uieditfield(uigrid_session_name, 'Value', opt.session_name, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('session_name', event.Value));

        uigrid_relative_joint_speed = uigridlayout('Parent', uigrid_opt, 'RowHeight', {'fit','1x'}, 'ColumnWidth', {'1x'}, ...
                                                   'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_relative_joint_speed, 'Text', 'Relative joint speed (in percent):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_relative_joint_speed = uispinner(uigrid_relative_joint_speed, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('relative_joint_speed', event.Value));
        uispinner_relative_joint_speed.Value = opt.relative_joint_speed;
        uispinner_relative_joint_speed.Limits = [0.01, 1];
        uispinner_relative_joint_speed.Step = 0.01;

        uigrid_execution_cycle_count = uigridlayout('Parent', uigrid_opt, 'RowHeight', {'fit','1x'}, 'ColumnWidth', {'1x'}, ...
                                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_execution_cycle_count, 'Text', 'Execution cycle count:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_execution_cycle_count = uispinner(uigrid_execution_cycle_count, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('execution_cycle_count', event.Value));
        uispinner_execution_cycle_count.Value = opt.execution_cycle_count;
        uispinner_execution_cycle_count.Limits = [1, Inf];
        uispinner_execution_cycle_count.Step = 1;
        uispinner_execution_cycle_count.RoundFractionalValues = 'on';     

        uicheckbox(uigrid_opt, 'Text', 'Enable camera capture for execution', 'FontSize', self.textsize, ...
                   'WordWrap', 'on', 'Value', opt.enable_camera_capture, ...
                   'ValueChangedFcn', @(src, event) self.save_config('enable_camera_capture', event.Value));
        uicheckbox(uigrid_opt, 'Text', 'Enable BOTA', 'FontSize', self.textsize, ...
                   'WordWrap', 'on', 'Value', opt.bota_use, ...
                   'ValueChangedFcn', @(src, event) self.save_config('bota_use', event.Value));
    end

    function [] = create_uipanel_planning(self, uipanel)
        self.delete_children(uipanel);
        trajectory_names = get_names_in_directory('trajectories');
        plan_names = get_names_in_directory('plans');
        orphaned_plan_names = array_diff(plan_names, trajectory_names);
        uigrid = self.create_uigrid(uipanel, 2 + size(trajectory_names, 2) + size(orphaned_plan_names, 2), 1);
        
        uibutton(uigrid, 'push', 'Text', 'Plan all', 'FontSize', self.textsize, ...
                 'ButtonPushedFcn', @(btn, event) self.launch_planning_all());

        uibutton(uigrid, 'push', 'Text', 'Save initial configuration', 'FontSize', self.textsize, ...
                 'ButtonPushedFcn', @(btn, event) self.launch_save_initial_configuration());

        for i = 1:size(trajectory_names, 2)
            uibtn = uibutton(uigrid, 'push', 'Text', trajectory_names{i}, 'FontSize', self.textsize, ...
                             'ButtonPushedFcn', @(btn, event) self.launch_planning(trajectory_names{i}));

            if i > 1
                if isempty(get_previous_plan_name(trajectory_names{i}))
                    uibtn.Enable = 'off';
                end
            end
        end
    end

    function [] = create_uipanel_execution(self, uipanel)
        self.delete_children(uipanel);
        trajectory_names = get_names_in_directory('trajectories');
        plan_names = get_names_in_directory('plans');
        orphaned_plan_names = array_diff(plan_names, trajectory_names);
        uigrid = self.create_uigrid(uipanel, 2 + size(trajectory_names, 2) + size(orphaned_plan_names, 2), 1);
        
        uibutton(uigrid, 'push', 'Text', 'Execute all', 'FontSize', self.textsize, ...
                 'ButtonPushedFcn', @(btn, event) self.launch_execution_all());

        if isfile('etc/initial_configuration.mat')
            uibutton(uigrid, 'push', 'Text', 'Go to saved initial position', 'FontSize', self.textsize, ...
                     'ButtonPushedFcn', @(btn, event) self.launch_go_to_initial_configuration());
        else
            uilabel(uigrid, 'Text', ''); % Simulate empty space
        end

        for i = 1:size(trajectory_names, 2)
            uigrid_btns = uigridlayout('Parent', uigrid, 'RowHeight', {'1x'}, 'ColumnWidth', {'2x','1x'}, 'Padding', [0, 0, 0, 0]);
            exec_btn = uibutton(uigrid_btns, 'push', 'Text', trajectory_names{i}, 'FontSize', self.textsize, ...
                                'ButtonPushedFcn', @(btn, event) self.launch_execution(trajectory_names{i}));
            exec_ret_btn = uibutton(uigrid_btns, 'push', 'Text', 'Return', 'FontSize', self.textsize, ...
                                    'ButtonPushedFcn', @(btn, event) self.launch_execution_return(trajectory_names{i}));
            if ~isfile(strcat('plans/', trajectory_names{i}, '.mat'))
                exec_btn.Enable = 'off';
                exec_ret_btn.Enable = 'off';
            end
        end

        for i = 1:size(orphaned_plan_names, 2)
            uigrid_btns = uigridlayout('Parent', uigrid, 'RowHeight', {'1x'}, 'ColumnWidth', {'2x','1x'}, 'Padding', [0, 0, 0, 0]);
            uibutton(uigrid_btns, 'push', 'Text', orphaned_plan_names{i}, 'FontSize', self.textsize, ...
                     'ButtonPushedFcn', @(btn, event) self.launch_execution(orphaned_plan_names{i}));
            uibutton(uigrid_btns, 'push', 'Text', 'Return', 'FontSize', self.textsize, ...
                     'ButtonPushedFcn', @(btn, event) self.launch_execution_return(orphaned_plan_names{i}));
        end
    end

    function [uigrid] = create_uigrid(~, parent, row_count, col_count)
        row_heights(1:row_count) = {'fit'};
        col_widths(1:col_count) = {'1x'};
        uigrid = uigridlayout('Parent', parent, 'RowHeight', row_heights, 'ColumnWidth', col_widths);
        uigrid.Scrollable = 'on';
    end

    function [] = on_close(self)
        try
            self.on_cancel();
            self.robot.disconnect();
            self.robot.unload_simulation();
        catch exception
            fprintf(2, exception.message + "\n");
            uialert(self.uifig, exception.message, 'Error');
        end

        delete(self.uifig);
    end

    function [] = toggle_robot_connection(self, uibtn)
        uiprogress = uiprogressdlg(self.uifig, 'Title', 'Connecting...', 'Indeterminate', 'on', 'Cancelable', 'off');
        drawnow;

        try
            if self.robot.is_connected()
                self.robot.disconnect();
            else
                self.robot.unload_simulation();
                self.robot.connect();
            end
        catch exception
            fprintf(2, getReport(exception, 'extended', 'hyperlinks', 'off') + "\n");
            uialert(self.uifig, getReport(exception, 'extended'), 'Error', 'Interpreter', 'html');
        end

        if self.robot.is_connected()
            uibtn.Value = true;
            uibtn.Text = 'Disconnect from hardware robot';
        else
            uibtn.Value = false;
            uibtn.Text = 'Connect to hardware robot';
        end

        close(uiprogress);
    end

    function [] = toggle_motion_capture_connection(self, uibtn)
        uiprogress = uiprogressdlg(self.uifig, 'Title', 'Connecting...', 'Indeterminate', 'on', 'Cancelable', 'off');
        drawnow;

        try
            if self.motion_capture.is_connected()
                self.motion_capture.disconnect();
            else
                self.motion_capture.connect();
            end
        catch exception
            fprintf(2, getReport(exception, 'extended', 'hyperlinks', 'off') + "\n");
            uialert(self.uifig, getReport(exception, 'extended'), 'Error', 'Interpreter', 'html');
        end

        if self.motion_capture.is_connected()
            uibtn.Value = true;
            uibtn.Text = 'Disconnect from motion capture hardware';
        else
            uibtn.Value = false;
            uibtn.Text = 'Connect to motion capture hardware';
        end

        close(uiprogress);
    end

    function [] = open_trajectories_directory(~)
        winopen('trajectories');
    end

    function [] = open_guide(~)
        winopen('../doc/guide.pdf');
    end

    function close_dialog_callback(self, src)
        self.update_uipanels();
        delete(src);
    end

    function [] = attach_table_add_row_col(self, table)
        function [] = add_table_row(~,~)
            if iscell(table.Data)
                c = cell(1, size(table.Data, 2));
                c(:,:) = {''};
                table.Data = [table.Data; c];
            else
                table.Data = [table.Data; zeros(1, size(table.Data, 2))];
            end
        end
        function [] = add_table_col(~,~)
            if iscell(table.Data)
                c = cell(size(table.Data, 1), 1);
                c(:,:) = {''};
                table.Data = [table.Data, c];
            else
                table.Data = [table.Data, zeros(size(table.Data, 1), 1)];
            end
        end

        cm = uicontextmenu(self.uifig);
        uimenu(cm, 'Text', 'Add row', 'MenuSelectedFcn', @add_table_row);
        uimenu(cm, 'Text', 'Add col', 'MenuSelectedFcn', @add_table_col);
        table.ContextMenu = cm;
    end

    function [] = update_ui_advance_options(self)
        opt = self.load_options();
        self.delete_children(self.uitab_advopts);

        uigrid = uigridlayout('Parent', self.uitab_advopts, 'ColumnWidth', {'1x'}, 'RowHeight', {'fit', '1x'});

        uipanel_robot = uipanel('Parent', uigrid, 'Title', 'Robot', 'FontSize', self.title_textsize);
        uigrid_robot = uigridlayout('Parent', uipanel_robot, 'RowHeight', {'fit'}, 'ColumnWidth', {'1x','1x','1x'});

        uigrid_robot_working_space_margin = uigridlayout('Parent', uigrid_robot, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                         'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_robot_working_space_margin, 'Text', 'Robot working space margin (in meter):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_robot_working_space_margin = uispinner(uigrid_robot_working_space_margin, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('working_space_margin', event.Value));
        uispinner_robot_working_space_margin.Value = opt.working_space_margin;
        uispinner_robot_working_space_margin.Limits = [0.0, WorkingSpace().MaxMargin];
        uispinner_robot_working_space_margin.Step = 0.001;

        uigrid_robot_flange_names = uigridlayout('Parent', uigrid_robot, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_robot_flange_names, 'Text', '6DOF name of robot flange in motion capture system:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_robot_flange_names = uitable(uigrid_robot_flange_names, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_robot_flange_6dof_names, 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_robot_flange_6dof_names', table.Data));
        self.attach_table_add_row_col(uitable_robot_flange_names);

        uigrid_robot_base_marker_names = uigridlayout('Parent', uigrid_robot, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                      'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_robot_base_marker_names, 'Text', 'Robot base marker names in motion capture system:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_robot_base_marker_names = uitable(uigrid_robot_base_marker_names, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_robot_base_marker_names, 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_robot_base_marker_names', table.Data));
        self.attach_table_add_row_col(uitable_robot_base_marker_names);

        uigrid_robot_flange_marker_names = uigridlayout('Parent', uigrid_robot, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                        'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uigrid_robot_flange_marker_names.Layout.Column = [1 3];
        uilabel(uigrid_robot_flange_marker_names, 'Text', 'Robot flange marker names in motion capture system:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_robot_flange_marker_names = uitable(uigrid_robot_flange_marker_names, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_robot_flange_marker_names, 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_robot_flange_marker_names', table.Data));
        self.attach_table_add_row_col(uitable_robot_flange_marker_names);

        uigrid_robot_joint_length_additions = uigridlayout('Parent', uigrid_robot, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                      'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uigrid_robot_joint_length_additions.Layout.Column = [1 2];
        uilabel(uigrid_robot_joint_length_additions, 'Text', 'Robot joints, flange and tool additional lengths:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_robot_joint_length_additions = uitable(uigrid_robot_joint_length_additions, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.joint_length_additions, 'CellEditCallback', ...
                @(table, event) self.save_config('joint_length_additions', table.Data));
        self.attach_table_add_row_col(uitable_robot_joint_length_additions);

        uipanel_motion_capture = uipanel('Parent', uigrid, 'Title', 'Motion capture', 'FontSize', self.title_textsize);
        uigrid_motion_capture = uigridlayout('Parent', uipanel_motion_capture, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'});

        uigrid_motion_capture_general = uigridlayout('Parent', uigrid_motion_capture, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x','1x','1x','1x'});
        uigrid_frequency = uigridlayout('Parent', uigrid_motion_capture_general, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_frequency, 'Text', 'Capture frequency:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_motion_capture_frequency = uispinner(uigrid_frequency, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('camera_capture_frequency', event.Value));
        uispinner_motion_capture_frequency.Value = opt.camera_capture_frequency;
        uispinner_motion_capture_frequency.Limits = [1, 1000];
        uispinner_motion_capture_frequency.Step = 1;
        uispinner_motion_capture_frequency.RoundFractionalValues = 'on';

        uigrid_motion_capture_sim = uigridlayout('Parent', uigrid_motion_capture, 'RowHeight', {'fit','1x'}, 'ColumnWidth', {'1x','2x','1x','6x'});
        uilabel(uigrid_motion_capture_sim, 'Text', 'Simulated marker names:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uilabel(uigrid_motion_capture_sim, 'Text', 'Simulated marker data (in millimeter):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uilabel(uigrid_motion_capture_sim, 'Text', 'Simulated 6DOF names:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uilabel(uigrid_motion_capture_sim, 'Text', 'Simulated 6DOF data (in millimeter):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_motion_capture_sim_marker_names = uitable(uigrid_motion_capture_sim, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_simulated_marker_names', 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_simulated_marker_names', table.Data'));
        self.attach_table_add_row_col(uitable_motion_capture_sim_marker_names);
        uitable_motion_capture_sim_marker_data = uitable(uigrid_motion_capture_sim, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_simulated_markers, 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_simulated_markers', table.Data));
        self.attach_table_add_row_col(uitable_motion_capture_sim_marker_data);
        uitable_motion_capture_sim_6dof_names = uitable(uigrid_motion_capture_sim, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_simulated_six_dofs_names', 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_simulated_six_dofs_names', table.Data'));
        self.attach_table_add_row_col(uitable_motion_capture_sim_6dof_names);
        uitable_motion_capture_sim_6dof_data = uitable(uigrid_motion_capture_sim, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.camera_capture_simulated_six_dofs, 'CellEditCallback', ...
                @(table, event) self.save_config('camera_capture_simulated_six_dofs', table.Data));
        self.attach_table_add_row_col(uitable_motion_capture_sim_6dof_data);

        drawnow;
    end

    function [] = update_ui_calibration(self)
        opt = self.load_options();
        self.delete_children(self.uitab_cal);

        uigrid_axes = uigridlayout('Parent', self.uitab_cal, 'ColumnWidth', {'fit', '1x'}, 'RowHeight', {'1x'});
        uigrid = uigridlayout('Parent', uigrid_axes, 'ColumnWidth', {'1x'}, ...
                              'RowHeight', {'fit', 'fit', 'fit', 'fit', '1x', 'fit', 'fit', 'fit'});
        self.uiaxes_robot_cal = axes(uigrid_axes, 'XGrid', 'on', 'YGrid', 'on', 'ZGrid', 'on', 'Box', 'on', ...
                                     'PlotBoxAspectRatioMode', 'manual', 'DataAspectRatioMode', 'manual');
        self.uiaxes_robot_cal.Visible = 'off';
        view(self.uiaxes_robot_cal, 225, 5);

        uigrid_n = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_n, 'Text', 'Point count:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_n = uispinner(uigrid_n, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('calibration_n', event.Value));
        uispinner_n.Value = opt.calibration_n;
        uispinner_n.Limits = [1, 100000];
        uispinner_n.Step = 1;

        uigrid_height = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                     'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_height, 'Text', 'Volume height (in meter):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_height = uispinner(uigrid_height, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('calibration_height', event.Value));
        uispinner_height.Value = opt.calibration_height;
        uispinner_height.Limits = [-5, 5];
        uispinner_height.Step = 0.0001;

        uigrid_volume_height_bottom_offset = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                   'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_volume_height_bottom_offset, 'Text', 'Volume height bottom offset (in meter):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_height_bottom_offset = uispinner(uigrid_volume_height_bottom_offset, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('calibration_height_bottom_offset', event.Value));
        uispinner_height_bottom_offset.Value = opt.calibration_height_bottom_offset;
        uispinner_height_bottom_offset.Limits = [-5, 5];
        uispinner_height_bottom_offset.Step = 0.0001;

        uigrid_volume_marker = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                            'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_volume_marker, 'Text', 'Volume marker names:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_volume_marker = uitable(uigrid_volume_marker, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.calibration_volume_marker_names, 'CellEditCallback', ...
                @(table, event) self.save_config('calibration_volume_marker_names', table.Data));
        self.attach_table_add_row_col(uitable_volume_marker);

        uilabel(uigrid, 'Text', ''); % Simulate empty space

        uibutton(uigrid, 'push', 'Text', 'Visualise calibration volume', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.launch_calibration_visualisation());
        uibutton(uigrid, 'push', 'Text', 'Plan calibration', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.launch_calibration_planning());
        uibutton(uigrid, 'push', 'Text', 'Execute calibration', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.launch_calibration_execution());
    end

    function [] = update_ui_trajectory_generation(self)
        opt = self.load_options();
        self.delete_children(self.uitab_trajgen);

        uigrid_axes = uigridlayout('Parent', self.uitab_trajgen, 'ColumnWidth', {'fit', '1x'}, 'RowHeight', {'1x'});
        uigrid = uigridlayout('Parent', uigrid_axes, 'ColumnWidth', {'1x'}, ...
                              'RowHeight', {'fit', 'fit', 'fit', 'fit', 'fit', 'fit', 'fit', '1x', 'fit'});
        self.uiaxes_robot_trajgen = axes(uigrid_axes, 'XGrid', 'on', 'YGrid', 'on', 'ZGrid', 'on', 'Box', 'on', ...
                                         'PlotBoxAspectRatioMode', 'manual', 'DataAspectRatioMode', 'manual');
        self.uiaxes_robot_trajgen.Visible = 'off';
        view(self.uiaxes_robot_trajgen, 225, 5);

        uigrid_traj_name = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                        'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_traj_name, 'Text', 'Trajectory name:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uieditfield(uigrid_traj_name, 'Value', opt.trajgen_name, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_name', event.Value));

        uigrid_parametric_equation_x = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_parametric_equation_x, 'Text', 'Parametric equation for x:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uieditfield(uigrid_parametric_equation_x, 'Value', opt.trajgen_parametric_equation_x, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_parametric_equation_x', event.Value));

        uigrid_parametric_equation_y = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_parametric_equation_y, 'Text', 'Parametric equation for y:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uieditfield(uigrid_parametric_equation_y, 'Value', opt.trajgen_parametric_equation_y, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_parametric_equation_y', event.Value));

        uigrid_parametric_equation_z = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_parametric_equation_z, 'Text', 'Parametric equation for z:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uieditfield(uigrid_parametric_equation_z, 'Value', opt.trajgen_parametric_equation_z, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_parametric_equation_z', event.Value));

        uigrid_t = uigridlayout('Parent', uigrid, 'RowHeight', {'fit'}, 'ColumnWidth', {'1x','1x','1x'}, ...
                                'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);

        uigrid_t_min = uigridlayout('Parent', uigrid_t, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_t_min, 'Text', 'Minimum for t:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_t_min = uispinner(uigrid_t_min, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_t_min', event.Value));
        uispinner_t_min.Value = opt.trajgen_t_min;
        uispinner_t_min.Step = 0.01;

        uigrid_t_max = uigridlayout('Parent', uigrid_t, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                    'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_t_max, 'Text', 'Maximum for t:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_t_max = uispinner(uigrid_t_max, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_t_max', event.Value));
        uispinner_t_max.Value = opt.trajgen_t_max;
        uispinner_t_max.Step = 0.01;

        uigrid_t_step = uigridlayout('Parent', uigrid_t, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                     'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_t_step, 'Text', 'Step size for t:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uispinner_t_step = uispinner(uigrid_t_step, 'ValueChangedFcn', ...
            @(btn, event) self.save_config('trajgen_t_step', event.Value));
        uispinner_t_step.Value = opt.trajgen_t_step;
        uispinner_t_step.Step = 0.01;

        uigrid_data_marker_names = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                                'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_data_marker_names, 'Text', 'Marker names:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_data_marker_names = uitable(uigrid_data_marker_names, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.trajgen_data_marker_names, 'CellEditCallback', ...
                @(table, event) self.save_config('trajgen_data_marker_names', table.Data));
        self.attach_table_add_row_col(uitable_data_marker_names);

        uigrid_rotation_type = uigridlayout('Parent', uigrid, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                            'ColumnSpacing', 0, 'Padding', [0, 0, 0, 0]);
        uilabel(uigrid_rotation_type, 'Text', 'Rotation type:', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitabg_rotation_type = uitabgroup(uigrid_rotation_type);
        uitab_rotation_free = uitab(uitabg_rotation_type, 'Title', 'Free rotation', 'UserData', C_ROTATION_FREE);
        uigrid_rotation_free = uigridlayout('Parent', uitab_rotation_free, 'RowHeight', {'fit'}, 'ColumnWidth', {'1x'}, ...
                                            'ColumnSpacing', 0);
        uitab_rotation_fixed = uitab(uitabg_rotation_type, 'Title', 'Fixed rotation', 'UserData', C_ROTATION_FIXED);
        uigrid_rotation_fixed = uigridlayout('Parent', uitab_rotation_fixed, 'RowHeight', {'fit','fit'}, 'ColumnWidth', {'1x'}, ...
                                             'ColumnSpacing', 0);
        uitabg_rotation_type.SelectionChangedFcn = @(tg, event) self.save_config('trajgen_rotation_type', event.NewValue.UserData);
        if opt.trajgen_rotation_type == C_ROTATION_FREE
            uitabg_rotation_type.SelectedTab = uitab_rotation_free;
        elseif opt.trajgen_rotation_type == C_ROTATION_FIXED
            uitabg_rotation_type.SelectedTab = uitab_rotation_fixed;
        end

        uilabel(uigrid_rotation_free, 'Text', 'No additional option', 'FontSize', self.textsize, 'WordWrap', 'on');
        uilabel(uigrid_rotation_fixed, 'Text', 'Rotation (euler, in radians):', 'FontSize', self.textsize, 'WordWrap', 'on');
        uitable_rotation_fixed = uitable(uigrid_rotation_fixed, "ColumnEditable", true, "ColumnName", [], ...
                "Data", opt.trajgen_rotation_fixed, 'CellEditCallback', ...
                @(table, event) self.save_config('trajgen_rotation_fixed', table.Data));
        self.attach_table_add_row_col(uitable_rotation_fixed);

        uilabel(uigrid, 'Text', ''); % Simulate empty space

        uigrid_btns = uigridlayout('Parent', uigrid, 'RowHeight', {'fit'}, 'ColumnWidth', {'1x','1x'}, ...
                                   'Padding', [0, 0, 0, 0]);
        uibutton(uigrid_btns, 'push', 'Text', 'Visualise and check trajectory', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.launch_trajectory_visualisation());
        uibutton(uigrid_btns, 'push', 'Text', 'Generate trajectory', 'FontSize', self.textsize, 'WordWrap', 'on', ...
                 'ButtonPushedFcn', @(btn, event) self.launch_trajectory_generation());
    end

    function [] = update_ui_execution(self)
        self.delete_children(self.uitab_exec);
        uigrid_root = uigridlayout('Parent', self.uitab_exec, 'ColumnWidth', {'1x'}, 'RowHeight', {'fit', '1x', '7x', 'fit'});

        self.title_label = uilabel(uigrid_root, 'Text', 'Standby', 'FontSize', self.title_textsize);

        uigrid_hz = uigridlayout('Parent', uigrid_root, 'ColumnWidth', {'fit', '1x', '4x'}, 'RowHeight', {'1x'});
        uilabel(uigrid_hz, 'Text', 'Update frequency', 'FontSize', self.textsize);
        uislider(uigrid_hz, 'Limits', [1, 100], 'Value', 2, ...
                 'ValueChangingFcn', @(~,event) self.save_config('execution_update_frequency', event.Value));
        self.uiaxes_frequency = axes(uigrid_hz, 'XGrid', 'on', 'YGrid', 'on', 'ZGrid', 'on', 'Box', 'on');
        exec_freq_plot = plot(self.uiaxes_frequency, [0, 0], [0, 0], 'Marker', 'o', 'Color', 'black', 'MarkerEdgeColor', 'black');
        exec_freq_plot.XDataSource = 'exec_freq_plot_x';
        exec_freq_plot.YDataSource = 'exec_freq_plot_y';
        assignin('base', 'exec_freq_plot_x', 0);
        assignin('base', 'exec_freq_plot_y', 0);

        uitabg = uitabgroup(uigrid_root);
        uitab_position = uitab(uitabg, 'Title', 'Joint configurations and cartesian positions');
        uitab_force = uitab(uitabg, 'Title', 'Forces and moments');
        uitab_misc = uitab(uitabg, 'Title', 'Miscellaneous');
        uigrid_position = uigridlayout('Parent', uitab_position, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x'});
        uigrid_force = uigridlayout('Parent', uitab_force, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x'});
        uigrid_misc = uigridlayout('Parent', uitab_misc, 'RowHeight', {'1x'}, 'ColumnWidth', {'1x'});

        self.cancel_btn = uibutton(uigrid_root, 'push', 'Text', 'Cancel', 'FontSize', self.title_textsize);
        self.cancel_btn.Enable = 'off';

        self.record_labels = {};
        self.record_uiaxes = {};
        assignin('base', 'exec_plot_timestamps', []);

        self.create_execution_record_display(uigrid_position, 'Kuka LBR IIWA Joint configuration [rad]', 7, 1);
        self.create_execution_record_display(uigrid_position, 'Kuka LBR IIWA Cartesian pose [meter, rad]', 6, 8);
        self.create_execution_record_display(uigrid_misc, 'Kuka LBR IIWA Rotation Matrix', 9, 14);
        self.create_execution_record_display(uigrid_force, 'Kuka LBR IIWA Torque []', 7, 23);
        self.create_execution_record_display(uigrid_force, 'Kuka LBR IIWA External torque []', 7, 30);
        self.record_size = 37;

        drawnow;
    end

    function [] = create_execution_record_display(self, parent, title_text, data_count, data_idx_start)
        uigrid = uigridlayout('Parent', parent, 'ColumnWidth', {'1x', '2x'}, 'RowHeight', {'fit'});
        uigrid_data = uigridlayout('Parent', uigrid, 'ColumnWidth', {'fit', 'fit', 'fit', 'fit', 'fit', 'fit', 'fit'}, 'RowHeight', {'fit'});

        data_title_label = uilabel(uigrid_data, 'Text', title_text, 'FontSize', self.textsize);
        data_title_label.Layout.Column = [1 7];

        for i = 1:data_count
            self.record_labels{end+1} = uilabel(uigrid_data, 'Text', '', 'FontSize', self.textsize);
        end

        colors = {'red', 'green', 'blue', 'cyan', 'magenta', 'yellow', 'black', '#EDB120', '#7E2F8E'};
        self.record_uiaxes{end+1} = axes(uigrid, 'XGrid', 'on', 'YGrid', 'on', 'ZGrid', 'on', 'Box', 'on');
        hold(self.record_uiaxes{end}, 'on');
        for i = 1:data_count
            exec_plot_name = "exec_plot_data_" + num2str(data_idx_start + i);
            assignin('base', exec_plot_name, []);

            exec_plot = plot(self.record_uiaxes{end}, [0, 0], [0, 0], 'Marker', 'o', 'Color', colors{i}, 'MarkerEdgeColor', colors{i});
            exec_plot.XDataSource = 'exec_plot_timestamps';
            exec_plot.YDataSource = exec_plot_name;
        end
        hold(self.record_uiaxes{end}, 'off');
    end

    function [] = update_execution_record_display(self, elapsed_time)
        if ~isempty(self.uiaxes_frequency)
            exec_freq_plot_x = evalin('base', 'exec_freq_plot_x');
            assignin('base', 'exec_freq_plot_x', [exec_freq_plot_x, exec_freq_plot_x(1, end) + 1]);
            assignin('base', 'exec_freq_plot_y', [evalin('base', 'exec_freq_plot_y'), elapsed_time]);
            refreshdata(self.uiaxes_frequency, 'base');
        end

        if ~isempty(self.record_labels)
            record = self.robot.get_last_record();

            if ~isempty(record)
                assignin('base', 'exec_plot_timestamps', [evalin('base', 'exec_plot_timestamps'), record(1, 1) + (self.fct_start_time * 1000)]);
    
                for i = 2:size(record, 2)
                    self.record_labels{i-1}.Text = num2str(record(1, i), '%.2f');
    
                    exec_plot_name = "exec_plot_data_" + num2str(i);
                    assignin('base', exec_plot_name, [evalin('base', exec_plot_name), record(1, i)]);
                end
                
                for i = 1:size(self.record_uiaxes, 2)
                    refreshdata(self.record_uiaxes{i}, 'base');
                end
                drawnow;
            end
        end
    end

    function [] = launch_motion_async(self, loading_texts, fns)
        self.cancel_requested = false;
        self.fct_start_time = 0;
        self.fct_tic_value = tic();

        function [] = exec_cancel(self)
            disp("Cancel asking...");
            if ~self.cancel_requested
                self.on_cancel();
            end
            self.cancel_requested = true;
            self.cancel_btn.Enable = 'off';
        end

        function [] = exec_quit(self)
            if self.cancel_requested
                self.title_label.Text = 'Canceled';
            else
                self.title_label.Text = 'Done';
            end
            exec_cancel(self);
        end

        self.cancel_btn.ButtonPushedFcn = @(~,~) exec_cancel(self);
        self.cancel_btn.Enable = 'on';
        %self.uitabs.SelectedTab = self.uitab_exec;
        assignin('base', 'exec_freq_plot_x', 0);
        assignin('base', 'exec_freq_plot_y', 0);
        assignin('base', 'exec_plot_timestamps', []);
        for i = 1:self.record_size
            exec_plot_name = "exec_plot_data_" + num2str(i);
            assignin('base', exec_plot_name, []);
        end
        self.tic_value = tic();

        function [] = wait_or_next(self, i)
            try
                elapsed_time = toc(self.tic_value);
                
                if (~self.robot.is_motion_finished()) && (~self.cancel_requested)
                    t = timer;
                    t.StartDelay = round(max(0, (1.0 / self.load_options().execution_update_frequency) - elapsed_time), 3);
                    t.TimerFcn = @(~,~) wait_or_next(self, i);
                    start(t);

                    self.tic_value = tic();

                    self.update_execution_record_display(elapsed_time);
                else
                    disp("Motion finished or canceled");
                    save_execution_data(self.robot, self.motion_capture);
                    
                    next_i = i + 1;
                    if (next_i <= size(fns, 2)) && ~self.cancel_requested
                        start_next(self, next_i)
                    else
                        exec_quit(self);
                    end
                end
            catch exception
                fprintf(2, getReport(exception, 'extended', 'hyperlinks', 'off') + "\n");
                uialert(self.uifig, getReport(exception, 'extended'), 'Error', 'Interpreter', 'html');
                exec_quit(self);
                self.on_cancel();
            end
        end

        function [] = start_next(self, i)
            try
                disp("Executing n°" + num2str(i) + " function");

                if i <= size(loading_texts, 2)
                    self.title_label.Text = loading_texts{i};
                    drawnow
                end

                self.fct_start_time = self.fct_start_time + toc(self.fct_tic_value);
                fn = fns{i};
                fn(self.load_options());
                self.fct_tic_value = tic();

                wait_or_next(self, i);
            catch exception
                fprintf(2, getReport(exception, 'extended', 'hyperlinks', 'off') + "\n");
                uialert(self.uifig, getReport(exception, 'extended'), 'Error', 'Interpreter', 'html');
                exec_quit(self);
                self.on_cancel();
            end
        end

        start_next(self, 1);
    end

    function [] = launch(self, loading_texts, fns)
        texts_size = size(loading_texts, 2);

        progressText = 'Loading...';
        if texts_size > 0
            progressText = loading_texts{1};
        end
        cancelableOpt = 'off';
        if texts_size > 1
            cancelableOpt = 'on';
        end
        uiprogress = uiprogressdlg(self.uifig, 'Title', progressText, 'Indeterminate', 'on', 'Cancelable', cancelableOpt);
        drawnow;

        try
            opt = self.load_options();

            for i = 1:size(fns, 2)
                if i <= texts_size
                    uiprogress.Title = loading_texts{i};
                    drawnow
                end

                fn = fns{i};
                fn(opt);
                if uiprogress.CancelRequested
                    self.on_cancel();
                    break
                end
            end
        catch exception
            fprintf(2, getReport(exception, 'extended', 'hyperlinks', 'off') + "\n");
            self.on_cancel();
            uialert(self.uifig, getReport(exception, 'extended'), 'Error', 'Interpreter', 'html');
        end

        self.update_uipanels();
        close(uiprogress);
    end

    function [] = launch_save_initial_configuration(self)
        self.launch({'Saving initial configuration from robot...'}, { @(~) save_initial_configuration(self.robot) } );
    end

    function [] = launch_go_to_initial_configuration(self)
        self.launch_motion_async({'Going to saved initial configuration...'}, { @(~) go_to_initial_configuration_async(self.robot) } );
    end

    function [] = launch_planning(self, trajectory_name)
        self.launch({strcat('Planning for "', trajectory_name, '" trajectory...')}, { @(~) plan_trajectory(trajectory_name, self.robot.get_rbt(), self.get_collision_env()) } );
    end
    
    function [] = launch_planning_all(self)
        trajectory_names = get_names_in_directory('trajectories');
        trajectory_names_size = size(trajectory_names, 2);

        plan_texts = cell(1, trajectory_names_size);
        plan_fns = cell(1, trajectory_names_size);
        for i = 1:trajectory_names_size
            plan_texts{i} = strcat('Planning trajectory "', trajectory_names{i}, '...');
            plan_fns{i} = @(~) plan_trajectory(trajectory_names{i}, self.robot.get_rbt(), self.get_collision_env());
        end
        
        self.launch(plan_texts, plan_fns);
    end

    function [] = launch_execution(self, trajectory_name)
        opt = self.load_options();
        total_exec_count = opt.execution_cycle_count;% + (opt.execution_cycle_count * opt.automatic_return);
        if opt.automatic_return || opt.automatic_return_threshold
            total_exec_count = total_exec_count + opt.execution_cycle_count;
        end
        exec_texts = cell(1, total_exec_count);
        exec_fns = cell(1, total_exec_count);
        i = 1;
        for c = 1:opt.execution_cycle_count
            exec_texts{i} = strcat('Executing planned trajectory "', trajectory_name, {'", cycle '}, string(c), '...');
            exec_fns{i} = @(opt) execute_planned_trajectory_async( ...
                trajectory_name, ...
                self.format_output_filename(opt.session_name, trajectory_name, num2str(c), ''), ...
                opt.enable_camera_capture, ...
                self.robot, self.motion_capture);
                i = i + 1;
            if opt.automatic_return
                exec_texts{i} = strcat('Returning planned trajectory "', trajectory_name, {'", cycle '}, string(c), '...');
                exec_fns{i} = @(opt) execute_return_planned_trajectory_async( ...
                    trajectory_name, ...
                    self.format_output_filename(opt.session_name, trajectory_name, num2str(c), '-return'), ...
                    opt.enable_camera_capture, ...
                    self.robot, self.motion_capture);
                i = i + 1;
            end 
            if opt.automatic_return_threshold
                exec_texts{i} = strcat('Checking bota issue: potentially returning planned trajectory "', trajectory_name, {'", cycle '}, string(c), '...');
                exec_fns{i} = @(opt) bota_check_execute_return_planned_trajectory_async( ...
                    trajectory_name, ...
                    self.format_output_filename(opt.session_name, trajectory_name, num2str(c), '-return-bota-issue'), ...
                    opt.enable_camera_capture, ...
                    self.robot, self.motion_capture);
                i = i + 1;
            end
        end
        
        self.launch_motion_async(exec_texts, exec_fns);
    end
    
    function [] = launch_execution_all(self)
        opt = self.load_options();
        plan_names = get_names_in_directory('plans');
        plan_names_size = size(plan_names, 2);
        total_exec_count = 1 + opt.execution_cycle_count * plan_names_size + (opt.execution_cycle_count * plan_names_size * opt.automatic_return) + (opt.execution_cycle_count * plan_names_size * opt.automatic_return_threshold);

        exec_texts = cell(1, total_exec_count);
        exec_fns = cell(1, total_exec_count);
        i = 1;

        exec_texts{i} = 'Going to initial configuration...';
        exec_fns{i} = @(~) go_to_initial_configuration_async(self.robot);
        i = i + 1;
        for p = 1:size(plan_names, 2)
            for c = 1:opt.execution_cycle_count
                exec_texts{i} = strcat('Executing planned trajectory "', plan_names{p}, {'", cycle '}, string(c), '...');
                exec_fns{i} = @(opt) execute_planned_trajectory_async( ...
                    plan_names{p}, ...
                    self.format_output_filename(opt.session_name, plan_names{p}, num2str(c), ''), ...
                    opt.enable_camera_capture, ...
                    self.robot, self.motion_capture);
                    i = i + 1;
                if opt.automatic_return
                    exec_texts{i} = strcat('Returning planned trajectory "', plan_names{p}, {'", cycle '}, string(c), '...');
                    exec_fns{i} = @(opt) execute_return_planned_trajectory_async( ...
                        plan_names{p}, ...
                        self.format_output_filename(opt.session_name, plan_names{p}, num2str(c), '-return'), ...
                        opt.enable_camera_capture, ...
                        self.robot, self.motion_capture);
                    i = i + 1;
                end
                if opt.automatic_return_threshold
                    exec_texts{i} = strcat('Checking bota issue: potentially returning planned trajectory "', plan_names{p}, {'", cycle '}, string(c), '...');
                    exec_fns{i} = @(opt) bota_check_execute_return_planned_trajectory_async( ...
                        plan_names{p}, ...
                        self.format_output_filename(opt.session_name, plan_names{p}, num2str(c), '-return-bota-issue'), ...
                        opt.enable_camera_capture, ...
                        self.robot, self.motion_capture);
                    i = i + 1;
                end
            end
        end
        
        self.launch_motion_async(exec_texts, exec_fns);
    end

    function [] = launch_execution_return(self, trajectory_name)
        self.launch_motion_async( ...
            {strcat('Returning from planned trajectory "', trajectory_name, '"...')}, ...
            { @(opt) execute_return_planned_trajectory_async( ...
                trajectory_name, ...
                self.format_output_filename(opt.session_name, trajectory_name, '0', '-return'), ...
                opt.enable_camera_capture, ...
                self.robot, self.motion_capture) } ...
        );
    end
    
    function [] = launch_calibration_visualisation(self)
        self.launch( ...
            {'Updating plot...'}, ...
            { @(opt) visualise_calibration_space(opt.calibration_n, opt.calibration_height, opt.calibration_height_bottom_offset, ...
                opt.calibration_volume_marker_names, opt.working_space_margin, ...
                self.robot, self.get_collision_env(), self.motion_capture, self.uiaxes_robot_cal); } ...
        );
    end

    function [] = launch_calibration_planning(self)
        self.launch( ...
            {'Planning calibration...'}, ...
            { @(opt) plan_calibration(opt.calibration_n, opt.calibration_height, opt.calibration_height_bottom_offset, ...
                opt.calibration_volume_marker_names, opt.working_space_margin, opt.calibration_plan_try_count, ...
                self.robot, self.motion_capture, self.get_collision_env()); } ...
        );
    end

    function [] = launch_calibration_execution(self)
        self.launch({'Executing calibration...'}, { @(opt) execute_calibration(self.robot, self.motion_capture); });
    end

    function [] = launch_trajectory_visualisation(self)
        self.launch( ...
            {'Updating plot...'}, ...
            { @(opt) visualise_parametric_trajectory(opt.trajgen_parametric_equation_x, opt.trajgen_parametric_equation_y, opt.trajgen_parametric_equation_z, ...
                opt.trajgen_t_min, opt.trajgen_t_max, opt.trajgen_t_step, opt.trajgen_data_marker_names, ...
                opt.working_space_margin, self.robot, self.get_collision_env(), self.motion_capture, self.uiaxes_robot_trajgen); } ...
        );
    end

    function [] = launch_trajectory_generation(self)
        function [] = fct(self, opt)
            fixed_rotation = [];
            if opt.trajgen_rotation_type == C_ROTATION_FIXED
                fixed_rotation = opt.trajgen_rotation_fixed;
            end
            
            generate_parametric_trajectory(opt.trajgen_parametric_equation_x, opt.trajgen_parametric_equation_y, opt.trajgen_parametric_equation_z, ...
                opt.trajgen_t_min, opt.trajgen_t_max, opt.trajgen_t_step, ...
                opt.trajgen_data_marker_names, fixed_rotation, ...
                opt.working_space_margin, opt.trajgen_name, self.robot, self.motion_capture);

            self.update_uipanels();
        end
        self.launch({'Generating trajectory...'}, { @(opt) fct(self, opt) });
    end

    function [] = delete_children(~, object)
        children = allchild(object);
        for i = 1 : length(children)
            delete(children(i));
        end
    end
end

end

function v = C_ROTATION_FREE
    v = 0;
end
function v = C_ROTATION_FIXED
    v = 1;
end

