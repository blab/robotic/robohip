function [points] = compute_parametric_trajectory(parametric_equation_x, parametric_equation_y, parametric_equation_z, t_min, t_max, t_step, data)
%COMPUTE_PARAMETRIC_TRAJECTORY Compute all points of the trajectory using the
%                              given parametric equations and parameter 
%                              for t, with additional data
%   parametric_equation_x: parametric equation for x axis in string form
%   parametric_equation_y: parametric equation for y axis in string form
%   parametric_equation_z: parametric equation for z axis in string form
%   t_min: start value for t
%   t_max: end value for t
%   t_step: value increase between each t for each point
%   data: additional data to pass to the parametric equations
%   Return: the computed points
%   authors: David Gonzalez, HEPIA

fx = str2func(['@(t, axis, p1, p2, p3, p4, v1, v2, v3, v4) ', parametric_equation_x]);
fy = str2func(['@(t, axis, p1, p2, p3, p4, v1, v2, v3, v4) ', parametric_equation_y]);
fz = str2func(['@(t, axis, p1, p2, p3, p4, v1, v2, v3, v4) ', parametric_equation_z]);

t_count = floor((t_max - t_min) / t_step) + 1;
points = zeros(t_count, 3);

i = 1;
for t = t_min:t_step:t_max
    points(i, 1) = fx(t, 1, data(1, 1), data(2, 1), data(3, 1), data(4, 1), data(1, :), data(2, :), data(3, :), data(4, :));
    points(i, 2) = fy(t, 2, data(1, 2), data(2, 2), data(3, 2), data(4, 2), data(1, :), data(2, :), data(3, :), data(4, :));
    points(i, 3) = fz(t, 3, data(1, 3), data(2, 3), data(3, 3), data(4, 3), data(1, :), data(2, :), data(3, :), data(4, :));
    i = i + 1;
end

end

