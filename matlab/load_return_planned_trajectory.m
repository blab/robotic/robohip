function [joint_trajectory] = load_return_planned_trajectory(trajectory_name, robot)
%LOAD_RETURN_PLANNED_TRAJECTORY Load a planned trajectory, cut it and return its reverse
%   Output format: nx7 in joint space
%   author: David Gonzalez, HEPIA

joint_trajectory = load_planned_trajectory(trajectory_name);
joint_trajectory = cut_joint_trajectory_at_position(joint_trajectory, robot.get_current_joint_configuration());
joint_trajectory = flip(joint_trajectory, 1);

end
