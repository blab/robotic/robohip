classdef RobotManipulatorKukaLBRIIWA < handle
% ROBOTMANIPULATORKUKALBRIIWA Allow seamless manipulation of a robot with a defined API, 
%                             either with real or simulated hardware.
%                             Making other robot should be easy, since it can respect
%                             that same interface.
%   author: David Gonzalez, HEPIA
properties (Constant=false)
    joint_count = 7;
    ip = "192.168.147.2";
    %sim_joint_diff_next_target = 0.001;
    
    iiwa = [];
    vrep = [];
    vrep_client_id = -1;
    vrep_joints = {};
    vrep_joint_original_speeds = {1.9199, 1.9199, 2.2340, 2.2340, 3.5605, 3.2114, 3.2114}; % taken from joint "Upper velocity limit" field [rad/s]
    %sim_future = [];
    sim_tmp_filename = '';
    sim_target_trajectory_i = 0;
    
    sim = true;
    relative_joint_speed = 0.1; % default value, change in UI instead
    recording_filename = '';
    recording_start_time = 0;
    motion_capture_base_marker_names = {}; % default value, change in UI instead
    motion_capture_flange_marker_names = {}; % default value, change in UI instead
    motion_capture_flange_6dof_names = {}; % default value, change in UI instead
    joint_length_additions = []; % default value, change in UI instead

    torque_threshold = 0; % default value, change in UI instead

    bota_use = 0; % default value, change in UI instead
end
methods
    function self = RobotManipulatorKukaLBRIIWA()
        addpath(genpath('kst'));
        addpath(genpath('sim'));
        
        transform_matrix_flange = eye(4);
        self.iiwa = KST(self.ip, KST.LBR14R820, KST.Medien_Flansch_Touch_elektrisch, transform_matrix_flange);

        self.set_simulating_flag(false);
    end
    
    function delete(self)
        self.disconnect();
        self.unload_simulation();
        self.set_simulating_flag(false);
    end
    
    function [sim] = is_sim(self)
        sim = self.sim;
    end
    
    function [] = set_sim(self, sim)
        self.sim = logical(sim);
    end

    function [configuration] = get_home_configuration(~)
         configuration = [0, 0, 0, 0, 0, 0, 0];
    end
    
    function [rbt] = get_rbt(self)
        rbt = load_modified_robot_rbt(self.joint_length_additions);
    end
    
    function [connected] = is_connected(self)
        connected = ~isempty(self.iiwa.t_Kuka);
    end

    function [issue] = bota_issue(self)
        issue = self.iiwa.botaIssue();
    end
    
    function [] = connect(self)
        if ~self.is_connected()
            disp("Connecting to hardware kuka lbr iiwa...");
            if self.iiwa.net_establishConnection() == 0
                error('Connexion failed, execution aborted. See console for details.');
            end
            disp("Connected to hardware kuka lbr iiwa");
            self.set_sim(false);
        end
    end
    
    function [] = disconnect(self)
        if self.is_connected()
            self.cancel_motion();
            disp("Disconnecting from hardware kuka lbr iiwa...");
            self.iiwa.net_turnOffServer();
            disp("Disconnected from hardware kuka lbr iiwa");
            self.set_sim(true);
        end
    end
    
    function [loaded] = is_simulation_loaded(self)
        loaded = ~isempty(self.vrep) && (self.vrep_client_id ~= -1);
    end

    function [capturing] = is_simulating(self)
        % We are force to use file, as 'self' is copied inside the thread by Matlab,
        % and thus, making a class variable not updatable externally.
        capturing = ~isempty(self.sim_tmp_filename) && isfile(self.sim_tmp_filename);
    end

    function [] = set_simulating_flag(self, newVal)
        % We are force to use file, as 'self' is copied inside the thread by Matlab,
        % and thus, making a class variable not updatable externally.
        if newVal
            self.sim_tmp_filename = tempname(tempdir);
            fclose(fopen(self.sim_tmp_filename, 'w'));
        else
            if ~isempty(self.sim_tmp_filename) && isfile(self.sim_tmp_filename)
                delete(self.sim_tmp_filename);
            end
            self.sim_tmp_filename = '';
        end
    end
    
    function [] = load_simulation(self)
        if ~self.is_simulation_loaded()
            % Copy necessary files
            coppeliasim_install_binding_path = "C:\Program Files\CoppeliaRobotics\CoppeliaSimEdu\programming\remoteApiBindings\";
            coppeliasim_dest_binding_path = "sim\";

            if ~isfolder(coppeliasim_dest_binding_path)
                mkdir(coppeliasim_dest_binding_path);
            end

            coppeliasim_remoteapi_dll_filename = "remoteApi.dll";
            coppeliasim_remapi_m_filename = "remApi.m";
            coppeliasim_apiproto_m_filename = "remoteApiProto.m";
            coppeliasim_remoteapi_dll_path = coppeliasim_install_binding_path + "lib\lib\Windows\" + coppeliasim_remoteapi_dll_filename;
            coppeliasim_remapi_m_path = coppeliasim_install_binding_path + "matlab\matlab\" + coppeliasim_remapi_m_filename;
            coppeliasim_apiproto_m_path = coppeliasim_install_binding_path + "matlab\matlab\" + coppeliasim_apiproto_m_filename;
            coppeliasim_remoteapi_dll_destpath = "sim\" + coppeliasim_remoteapi_dll_filename;
            coppeliasim_remapi_m_destpath = "sim\" + coppeliasim_remapi_m_filename;
            coppeliasim_apiproto_m_destpath = "sim\" + coppeliasim_apiproto_m_filename;

            if ~isfile(coppeliasim_remoteapi_dll_destpath)
                copyfile(coppeliasim_remoteapi_dll_path, coppeliasim_remoteapi_dll_destpath);
                disp(coppeliasim_remoteapi_dll_filename + " copied");
            end
            if ~isfile(coppeliasim_remapi_m_destpath)
                copyfile(coppeliasim_remapi_m_path, coppeliasim_remapi_m_destpath);
                disp(coppeliasim_remapi_m_filename + " copied");
            end
            if ~isfile(coppeliasim_apiproto_m_destpath)
                copyfile(coppeliasim_apiproto_m_path, coppeliasim_apiproto_m_destpath);
                disp(coppeliasim_apiproto_m_filename + " copied");
            end

            % Connect to CoppeliaSim
            disp("Loading library for simulation...");
            self.vrep = remApi('remoteApi');
            self.vrep.simxFinish(-1);
            disp("Library loaded for simulation");

            disp("Connecting to simulation...");
            self.vrep_client_id = self.vrep.simxStart('127.0.0.1', 19997, true, true, 5000, 5);
            if (self.vrep_client_id == -1)
                error("Cannot connect to a running instance of CoppeliaSim at 127.0.0.1:19997")
            end
            disp("Connected to simulation");

            self.vrep_joints = {};
            for i = 1:self.joint_count
                joint_name = ['LBR_iiwa_14_R820_joint', num2str(i)];
                [errCode, self.vrep_joints{i}] = self.vrep.simxGetObjectHandle(self.vrep_client_id, joint_name, self.vrep.simx_opmode_oneshot_wait);
                if errCode ~= 0
                    self.unload_simulation();
                    error("loading simulated joint failed, error code " + num2str(errCode))
                end
            end
        end
    end
    
    function [] = unload_simulation(self)
        if self.is_simulation_loaded()
            disp("Disconnecting from simulation...");
            %self.vrep.simxStopSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
            self.vrep.simxFinish(self.vrep_client_id);
            self.vrep.delete();
            disp("Disconnected from simulation");
        end
        self.vrep = [];
        self.vrep_client_id = -1;
        self.vrep_joints = {};
        
        %if ~isempty(self.sim_future)
        %    cancel(self.sim_future);
        %end
        %self.sim_future = [];
        self.set_simulating_flag(false);
    end

    function [] = run_simulation(self, joint_trajectory)
        %function [] = set_going_to_joint_pos(self, pos)
        %    self.vrep.simxPauseCommunication(self.vrep_client_id, 1);
        %    for ipos = 1:self.joint_count
        %        errCodePos = self.vrep.simxSetJointTargetPosition(self.vrep_client_id, self.vrep_joints{ipos}, pos(1, ipos), self.vrep.simx_opmode_oneshot);
        %        if errCodePos ~= 0 && errCodePos ~= 1
        %            self.vrep.simxPauseSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
        %            error("setting simulated joint speed failed, error code " + num2str(errCodePos))
        %        end
        %    end
        %    self.vrep.simxPauseCommunication(self.vrep_client_id, 0);
        %end

        % Set joint speed
        self.vrep.simxPauseCommunication(self.vrep_client_id, 1);
        for i = 1:self.joint_count
            errCode = self.vrep.simxSetObjectFloatParameter( ...
                self.vrep_client_id, self.vrep_joints{i}, self.vrep.sim_jointfloatparam_upper_limit, ...
                self.vrep_joint_original_speeds{i} * self.relative_joint_speed, self.vrep.simx_opmode_oneshot ...
            );
            if errCode ~= 0 && errCode ~= 1
                self.vrep.simxPauseSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
                error("setting simulated joint speed failed, error code " + num2str(errCode))
            end
        end
        self.vrep.simxPauseCommunication(self.vrep_client_id, 0);

        % Upload trajectory
        self.sim_target_trajectory_i = size(joint_trajectory, 1);
        joint_trajectory_flatten = reshape(joint_trajectory', 1, []);
        errCode = self.vrep.simxSetStringSignal(self.vrep_client_id, 'trajectory', self.vrep.simxPackFloats(joint_trajectory_flatten), self.vrep.simx_opmode_oneshot_wait);
        if errCode ~= 0
            self.vrep.simxPauseSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
            error("simulated trajectory upload failed, error code " + num2str(errCode))
        end

        errCode = self.vrep.simxStartSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
        if errCode ~= 0
            error("starting simulation failed, error code " + num2str(errCode))
        end

        %it = 1;
        %set_going_to_joint_pos(self, joint_trajectory(it, :));

        % Enable position streaming
        %for i = 1:self.joint_count
        %    self.vrep.simxGetJointPosition(self.vrep_client_id, self.vrep_joints{i}, self.vrep.simx_opmode_streaming);
        %end

        %while (it <= size(joint_trajectory, 1)) && self.is_simulating()
        %    % Test if all joint positions are sufficiently near their target
        %    all_near = true;
        %    for i = 1:self.joint_count
        %        [errCode, joint_angle] = self.vrep.simxGetJointPosition(self.vrep_client_id, self.vrep_joints{i}, self.vrep.simx_opmode_buffer);
        %        if errCode ~= 0 && errCode ~= 1
        %            self.vrep.simxPauseSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
        %            error("error getting joint position, error code " + num2str(errCode))
        %        end
        %
        %        if abs(joint_angle - joint_trajectory(it, i)) > self.sim_joint_diff_next_target
        %            all_near = false;
        %            break;
        %        end
        %    end
        %
        %    % If we are here, all joints are sufficiently near, so we can go to the next
        %    if all_near
        %        it = it + 1;
        %        if it <= size(joint_trajectory, 1)
        %            set_going_to_joint_pos(self, joint_trajectory(it, :));
        %        end
        %    end
        %end

        % Disable position streaming
        %for i = 1:self.joint_count
        %    self.vrep.simxGetJointPosition(self.vrep_client_id, self.vrep_joints{i}, self.vrep.simx_opmode_discontinue);
        %end

        %self.set_simulating_flag(false);
        %errCode = self.vrep.simxPauseSimulation(self.vrep_client_id, self.vrep.simx_opmode_oneshot_wait);
        %if errCode ~= 0
        %    error("pausing simulation failed, error code " + num2str(errCode))
        %end
    end
    
    function [joint_configuration] = get_current_joint_configuration(self)
        joint_configuration = zeros(1, self.joint_count);
        
        if self.sim
            self.load_simulation();

            for i = 1:self.joint_count
                [errCode, joint_angle] = self.vrep.simxGetJointPosition(self.vrep_client_id, self.vrep_joints{i}, self.vrep.simx_opmode_oneshot_wait);
                if errCode ~= 0
                    self.unload_simulation();
                    error("error getting joint position, error code " + num2str(errCode))
                end
                joint_configuration(1, i) = double(joint_angle);
            end
        else
            if self.is_connected()
                joint_configuration = double(cell2mat(self.iiwa.getJointsPos()));
            end
        end
    end
    
    function [pose] = get_current_pose(self)
        if self.sim
            pose = zeros(1, 6);
        else
            if self.is_connected()
                pose = double(cell2mat(self.iiwa.getEEFPos()));
            else
                pose = zeros(1, 6);
            end
        end
    end
    
    function [finished] = is_motion_finished(self)
        if self.sim
            finished = ~self.is_simulating();
            if ~finished
                [errCode, trajectory_i] = self.vrep.simxGetIntegerSignal(self.vrep_client_id, 'trajectory_i', self.vrep.simx_opmode_oneshot_wait);
                if trajectory_i == self.sim_target_trajectory_i || errCode ~= 0
                    finished = true;
                    self.set_simulating_flag(false);
                end
            end
            
            %if finished && ~isempty(self.sim_future)
            %    fetchOutputs(self.sim_future);
            %    self.sim_future = [];
            %end
        else
            if self.is_connected()
                finished = self.iiwa.isMotionFinished();
            else
                finished = true;
            end
        end
    end
    
    function [] = wait_motion_finished(self)
        while ~self.is_motion_finished()
            pause(0.5);
        end
        disp("Motion finished");
    end
    
    function [] = execute_joint_trajectory_async(self, joint_trajectory)
        if self.sim
            if self.is_simulating()
                answer = questdlg('A simulation seems already in progress. Do you want to force a new simulation ?', 'Simulation in progress', 'Yes', 'Cancel', 'Cancel');
                switch answer
                    case 'Yes'
                        self.set_simulating_flag(false);
                    otherwise
                        disp("new simulation canceled");
                        return;
                end
            end

            self.load_simulation();

            self.set_simulating_flag(true);
            % calllib cannot be called inside a thread
            %self.sim_future = parfeval(backgroundPool, @self.run_simulation, 0, joint_trajectory);
            self.run_simulation(joint_trajectory);
        else
            if self.is_connected()
                self.iiwa.nonBlocking_movePTPTrajectoryJoint(joint_trajectory, self.relative_joint_speed, self.bota_use, self.torque_threshold);
            end
        end
    end
    
    function [] = execute_joint_trajectory(self, joint_trajectory)
        self.execute_joint_trajectory_async(joint_trajectory);
        self.wait_motion_finished();
    end

    function [] = toggle_recording(self, recording_filename)
        self.recording_filename = recording_filename;
        self.recording_start_time = posixtime(datetime());
    end

    function [recording_filename] = get_recording_filename(self)
        recording_filename = self.recording_filename;
    end
    
    function [records] = get_last_records(self)
        % records: nx28
        %   1: relative unix timestamp with milliseconds, where 0 is the beginning of the execution;
        %   2-8: joint position in radians;
        %   9-14: cartesian position (m) and orientation (rad);
        %   15-23: rotation matrix (index pair: 00, 01, 02, 10, 11, 12, 20, 21, 22)
        %   24-30: torque of each joint (Nm);
        %   31-37: external torque of each joint (Nm);
        if self.sim
            records = [];
        else
            if self.is_connected()
                records = self.iiwa.getLastRecords();
            else
                records = [];
            end
        end
        self.recording_filename = '';
    end

    function [record] = get_last_record(self)
        % records: 1x28
        %   1: relative unix timestamp with milliseconds, where 0 is the beginning of the execution;
        %   2-8: joint position in radians;
        %   9-14: cartesian position (m) and orientation (rad);
        %   15-23: rotation matrix (index pair: 00, 01, 02, 10, 11, 12, 20, 21, 22)
        %   24-30: torque of each joint (Nm);
        %   31-37: external torque of each joint (Nm);
        if self.sim
            record = [
                posixtime(datetime()) - self.recording_start_time, ...
                self.get_current_joint_configuration(), ...
                self.get_current_pose(), ...
                0, 0, 0, 0, 0, 0, 0, 0, 0, ...
                0, 0, 0, 0, 0, 0, 0, ...
                0, 0, 0, 0, 0, 0, 0, ...
            ];
        else
            if self.is_connected()
                record = self.iiwa.getLastRecord();
            else
                record = [];
            end
        end
    end

    function [] = cancel_motion(self)
        if self.sim
            if self.is_simulation_loaded
                disp("Canceling motion...");
                self.set_simulating_flag(false);
                %if ~isempty(self.sim_future)
                %    cancel(self.sim_future);
                %end
                self.execute_joint_trajectory_async([]);
                disp("Canceled motion");
            end
        else
            if self.is_connected()
                disp("Canceling motion...");
                self.iiwa.cancelMotion();
                disp("Canceled motion");
            end
        end
    end
end

end
