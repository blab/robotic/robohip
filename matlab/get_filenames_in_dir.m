function [filenames] = get_filenames_in_dir(dirname, prefix, suffix)
%GET_FILENAMES_IN_DIRGet file inside directory, respecting a certain prefix and/or suffix

listing = dir(dirname);
filenames = {};
for i = 3:size(listing)
    if ((exist('prefix', 'var') && startsWith(listing(i).name, prefix)) || (~exist('prefix', 'var'))) && ...
       ((exist('suffix', 'var') && endsWith(listing(i).name, suffix)) || (~exist('suffix', 'var')))
        filenames{end+1} = strcat(dirname, '/', listing(i).name);
    end
end

end

