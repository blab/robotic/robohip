function [value_indexes] = get_indexes(values, arr)
%GET_INDEXES Get the indexes of the given values inside arr, -1 if not found
value_indexes = zeros(1, size(values, 2));

if iscell(values)
    for i = 1:size(values, 2)
        value_indexes(1, i) = get_index(values{i}, arr);
    end
else
    for i = 1:size(values, 2)
        value_indexes(1, i) = get_index(values(i), arr);
    end
end

end

