function [joint_trajectory] = execute_planned_trajectory_async(trajectory_name, output_filename, enable_camera_capture, robot, motion_capture)
%EXECUTE_PLANNED_TRAJECTORY_ASYNC Load a planned trajectory and execute it
%   Load a trajectory in joint space and make the robot follow it.
%   Input format: nx7 in joint space
%   Joint speed in percent, from 0 to 1
%   author: David Gonzalez, HEPIA

disp("Executing planned trajectory for '" + trajectory_name + "'...");

joint_trajectory = load_planned_trajectory(trajectory_name);
execute_loaded_trajectory_async(joint_trajectory, output_filename, enable_camera_capture, robot, motion_capture);

end
