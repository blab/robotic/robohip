---
documentclass: article
papersize: a4
fontsize: 12p
header-includes: |
    \usepackage[english,french]{babel}
    \usepackage[T1]{fontenc}
    \usepackage[utf8]{inputenc}
    \usepackage[left=2cm, right=2cm, top=2cm, bottom=2cm]{geometry}
    \usepackage{hyperref}
    \usepackage{fancyhdr}
    \usepackage{lastpage}
    \usepackage{listings}
    \usepackage{float}
    \usepackage{graphicx}

    \pagestyle{fancy}
    \lhead{Robiom}
    \chead{}
    \rhead{Guide opérateur}
    \renewcommand{\footrulewidth}{0.5pt}
    \lfoot{\today}
    \cfoot{}
    \rfoot{Page \thepage /\pageref{LastPage}}

    \lstset{
        basicstyle=\ttfamily\small,
        stringstyle=\ttfamily\color{green!50!black},
        keywordstyle=\bfseries\color{blue},
        commentstyle=\itshape\color{red!50!black},
        showstringspaces=true,
        tabsize=4,
        frame=single,
        numbers=left,
        numberstyle=\tiny,
        firstnumber=1,
        stepnumber=1,
        numbersep=5pt,
        breaklines=true,
        captionpos=b,
        belowskip=1.2 \baselineskip
    }

    \hypersetup{
        colorlinks=true,
        linkcolor=.,
        urlcolor=blue,
        allbordercolors={0 0 0}
    }
---

\title
{
    {\Huge Robiom} \\
    {\Huge Guide opérateur}
}
\author
{
    {\LARGE David Gonzalez} \\
    {\LARGE Florent Moissenet}
}
\date{\today}
\maketitle

\thispagestyle{empty}

\tableofcontents

\newpage

# Fonctionnement

## Schéma block

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=1.0\textwidth,scale=0.5]{general_structure}
    \end{center}
    \caption{Structure generale}
    \label{general_structure}
\end{figure}

\newpage

# Installation

## Logiciels et versions

* [Matlab](https://www.mathworks.com/products/matlab.html): R2022b (9.13.0.2049777)
    * [Robotics System Toolbox](https://www.mathworks.com/solutions/robotics.html): 4.1
    * [Parallel Toolbox](https://www.mathworks.com/products/parallel-computing.html): 7.7
    * [KST](https://github.com/Modi1987/KST-Kuka-Sunrise-Toolbox): 1.7 (lourdes modifications apportées)
* [Kuka Sunrise Workbench](https://www.kuka.com/fr-ch/produits-et-prestations/syst%C3%A8mes-de-robots/logiciels/logiciel-syst%C3%A8me/sunriseos): 1.10.0.8
* [CoppeliaSim](https://www.coppeliarobotics.com/): 4.1.0 Edu
* [Qualisys QTM](https://www.qualisys.com/software/qualisys-track-manager/): ?
* [Qualisys QTM plugin for Matlab](https://www.qualisys.com/software/integrations/matlab/): ?

## Configuration IP de la machine cliente

Pour que la communication avec le robot et les caméras functionnent,
il faut choisir 1 ou 2 port(s) Ethernet pour y configurer les IPs suivantes:

* Robot Kuka:
    * X66: `172.31.1.150` / `255.255.0.0`
    * KONI: `192.168.147.3` / `255.255.255.0`
* Camera Qualisys: `192.168.1.1` / `255.255.255.128`

## Matlab

* Installez le logiciel normalement
    * pendant l'installation, dans l'onglet `Product`, cochez également `Robotics System Toolbox` et `Parallel Toolbox`
    * cette étape peut-être faite après l'installation à l'aide du menu `Addons`

## Kuka Sunrise Workbench

* Installez le logiciel normalement
* Lancez le logiciel normalement, laissez le workspace a son emplacement par défaut (cocher la case)
* Importer le projet existant:
    * menu: `Fichier` -> `Importer`
    * séléctionnez: Général -> Projets existants dans l'espace de travail
    * répertoire racine: dossier `sunrise` à la racine du projet Robiom
    * laissez toutes les autres options telle quelle: NE PAS COPIER
    * cliquez sur `Terminer`

### Transfère du projet sur le robot

* Cliquez sur l'icone `Synchroniser le projet` (rectangle en 3D gris avec deux flèches, une dans chaque sens, comme le symbole de la touche du clavier `Tab`)
* Si les règles de sécurité ont changés, alors il faut réactiver la sécurité sur le robot.
    * après redémarrage du robot, sur la SmartPad, menu: `Security` -> `Activation`
    * cliquez sur le bouton `Activate`
    * il va vous demander un mot de passe: `argus`

## CoppeliaSim

* Installez le logiciel normalement
* Lancez le logiciel normalement, il devrait se fermer automatiquement
* Allez dans `C:\Program Files\CoppeliaRobotics\CoppeliaSimEdu\system` et ouvrez en edition `usrset.txt`
* Ajoutez en début de fichier `allowOldEduRelease=7501`

Note: si le logiciel ne se lance pas (aucun affichage graphique), alors il faut l'exécuter en mode administrateur.

## Qualisys QTM et plugin Matlab

* Installez le logiciel normalement, il n'est pas nécessaire d'installer plus que les deux options de base
* Lancez le logiciel normalement et entrez la clé de license quand demandé
* Ouvrez le projet correspondant
* Lorsque ouvert, allez dans `Help` -> `About Qualisys Track Manager`, bouton `Licenses`
    * Bouton `Add Plug-In License`, seléctionnez `Matlab Plugin` et entrez la clé de license

\newpage

# Opération

## Fonctionnement général

L'interface principale est divisée en plusieurs parties:

* Panneau `Menu` avec la gestion de la connexion au robot et l'accès aux fonctionnalités annexes
* Panneaux `Options`, `Planning` et `Execution` avec les options et commandes concernant la planification et l'Exécution de trajectoires

### Connexion au robot

Le bouton `Connect to hardware robot` permet de se connecter à l'application serveur sur le robot.
Pour cela, il faut que l'application `MatlabToolboxServer` soit lancée depuis le SmartPad du robot.

Lorsque la connexion est établie, le bouton reste enfoncé (gris foncé) et change le label à `Disconnect from hardware robot`.

La déconnexion se fait par ce même bouton.
Lorsque déconnecté, le bouton repassera en gris normal (comme les autres) avec le label original.

Si vous n'êtes pas connecté au robot, alors l'interface exécutera le tous en simulation.

## Planification et exécution de trajectoires

### Configuration initiale (Position de départ)

Pour pouvoir effectuer le planning, une position de départ doit être enregistrée.

Pour cela, placer le robot dans la position souhaiter avec la méthode de votre choix:

* avec le SmartPad en mode T1
* avec l'application robot `PreciseHandGuiding`

Lorsque le robot est en position, cliquez sur le bouton `Save initial configuration` dans le panneau `Planning`.
Le fichier est enregistré dans `./matlab/etc/initial_configuration.mat`.

Si vous n'êtes pas connecté au robot, alors tout cela se fera en simulation.

### Fichiers de trajectoire et planifications

Les fichiers de trajectoires doivent être placés dans le dossier `./matlab/trajectories/` et ordonné dans l'ordre naturel lexicographique.
Le bouton `Open trajectories directory` vous ouvrira se dossier dans l'explorateur de fichier.

Le planneur considère que les trajectoires se suivent.
Cela implique que lorsqu'on lance la planification d'une trajectoire,
la dernière position du plan précédent est utilisée comme configuration initiale.
Seule la première trajectoire fait exception à la règle et utilise la configuration initiale enregistrée séparément.

Sachant que les trajectoires se suivent, la planification doit également être fait dans l'ordre.
Si une planification d'une trajectoire intermédiaire est relancée, alors tous les plans qui suivent sont supprimés.

Le bouton `Plan All` permet de plannifier les trajectoires dans l'ordre affiché.

Le format est un fichier `.mat` avec deux variables:

* `O_flange`: 3x1xN position cartesienne en 3D;
* `Q_flange`: 4x1xN rotation en quaternion avec ordre `XYZ`. La variable est optionnel.
              Si celle-ci n'est pas présent, alors la rotation sera ignorée et le solver prendra la meilleure rotation pour un point donné.

Attention: si vous rajoutez des fichiers de trajectoires,
il faut que l'application soit relancée pour que les nouveaux fichiers soient pris en compte.

### Exécution

Lorsque tous les plans ont été fait, les boutons d'exécution apparaissent.
Le logiciel ne contraint pas l'ordre dans lequel vous exécutez les plans.
Ceci dit, il est FORTEMENT recommandé de suivre l'ordre suivant afin d'éviter des comportements étranges:

* position initiale, bouton `Go to saved initial position`
* trajectoires dans l'ordre lexicographique, tel que présenté par l'interface
    * le bouton `Return` permet de jouer la trajectoire, mais à l'envers: de la fin vers le début,
      permettant de revenir à la configuration initiale exacte de la trajectoire.
      Ceci prenant en compte que la trajectoire peut être exécutée partiellement,
      et donc le retour se fera depuis le point le plus proche de la configuration courante.

Pendant l'exécution, des données sont recuillies sur le robot et sur le système camera si activé.
Ces données sont ensuite enregistrées dans `./matlab/data/` avec le nom de la session (voir option ci-dessous).

Le bouton `Execute All` permet d'exécuter les trajectoires planifiées dans l'ordre affiché.
Dans ce cas, un cycle d'exécution est l'exécution de toutes les trajectoires séquentiellement.
Si l'option `Automatically return after trajectory execution` est activée,
alors le retour sera inclus après chaque trajéctoire.

Si vous n'êtes pas connecté au robot, alors tout cela se fera en simulation.

#### Options

* `session name`: nom de la session qui servira a nommer les fichiers.
Les symboles suivants seront remplacer par leur valeur respective
    * `%t`: nom de la trajectoire
    * `%c`: numéro d'itération de l'exécution
    * `%d`: date et heure courant en format ISO (yyyy-MM-dd-HH-mm-ss-SSS) en timezone UTC
* `relative joint speed`: vitesse de mouvement en valeur normalisée:
    * 1: vitesse maximum, 0: vitesse nulle
* `execution cycle count`: nombre d'exécution de chaque trajectoire
* `Automatically return after trajectory execution`: joue automatiquement la trajectoire inverse après l'exécution d'une trajectoire.
    * ceci intègre la trajectoire inverse dans le cycle courant.
    * voir le bouton `Return` ci-dessus pour les détails de fonctionnement.
* `enable camera capture`: active ou non la capture de la position du robot par les cameras

## Calibration

L'interface de calibration est accessible avec le bouton `Calibratation...` sur l'interface principale.
Celle-ci est composée d'une multitude d'options, puis de 3 boutons.

### Fonctionnement

La calibration fonctionne en 2 temps:

Le premier temps consiste à planifier entièrement les mouvements à effectuer.
Elle se décompose ainsi:

1. définition du volume (si besoin avec des markers dans QTM)
2. calcule de points dans le volume en forme de grille 3D
    * le nombre de points sera arrondi à la racine cubique supérieure la plus proche pour assurer le bon fonctionnement
    * une grille 3D est ici utilisée pour s'assurer que chaque point est bien séparé des autres, ainsi que par simplicité
3. déplacement des points qui se trouve à l'extérieur du volume de travail du robot dans l'espace de travail
    * ce déplacement se fait à l'aide d'une boucle afin d'assurer que les points reste dans le volume défini
4. applique la fonction de cinématique inverse sur chaque point de manière indépendante
    * afin d'assurer que le robot prenne bien une configuration des joints finale différente pour chaque point,
      une configuration initiale (initial guess) aléatoire est donnée
    * la rotation est non spécifié, le solver décidera donc de la rotation qui l'arrange le mieux,
      la rotation finale de la flange du robot est à considérer comme aléatoire
5. pour chaque configuration, applique une fonction d'interpolation afin d'être sûr que le robot n'entre pas en collision avec l'environnement
6. première sauvegarde de l'état de la calibration

Le deuxième temps consiste à l'exécution de cette planification, composé comme suit:

1. charge l'état courant de la calibration
2. pour chaque point:
    1. sauvegarde de l'état courant de la calibration
    2. exécution la trajectoire interpolée du point
    3. récupèration de la position 3D de la flange du robot vue du robot
    4. récupèration de la configuration des joints du robot
    5. récupèration de la position 3D de la flange du robot vue des caméras
    6. retour à la position 0 (home configuration)
3. sauvegarde des données de calibration
4. suppression de l'état de a calibration

À chaque étape (par point) de la calibration, l'état est sauvegardé,
permettant, en cas d'erreur, la reprise de la calibration depuis le point d'échec.

### Options et actions

* `point count`: nombre de points pour la calibration, arrondi à la racine cubique supérieure la plus proche
* `volume height`: hauteur de volume de calibration
* `volume height bottom offset`: espace entre le marker et le début du volume, en hauteur
* `volume data source`: choisir ici la source des données des markers définissant le volume:
    * `QTM`: la source est live, donnez ici les noms des markers
    * `User provided`: la source est statique: donnez ici les valeurs des markers (attention à l'espace de référence)
    * `capture file`: la source est statique: donnez ici les fichiers de capture (voir note ci-dessous)

Lorsque les paramètres ont été réglés, vous pouvez:

* visualiser le volume de calibration et sa transformation, et adapter les paramètres le cas échéant
* planifier la calibration (attention: peut être long suivant le nombre de point)
* exécuter la calibration

Note: lorsque la source de la définition du volume est `QTM`, alors à chaque visualisation ou planning,
les valeurs des markers sont sauvegardées dans un fichier, pouvant par la suite est réutilisé avec l'option `capture file`.

### Légende du graphique

* points noirs: positions des markers
* points violets: positions des points représentant le volume calculé
* points bleus: position des points de calibration

## Génération de trajectoires

L'interface de génération de trajectoire est accessible avec le bouton `Trajectory generation...` sur l'interface principale.
Celle-ci est composée d'une multitude d'options, puis de 2 boutons.

### Fonctionnement

La génération de trajectoire est basé sur les équations paramétriques.
Ceci a pour avantage de laisser l'utilisateur le contrôle du type de trajectoire ainsi que d'où elle commence,
où elle se termine et avec quelle précision.

L'interface propose d'entrer des formules pour chaque axe (x, y et z).
Chacune de ces formules ont accès à des variables prédéfinies:

* `t`: variable paramétrique permettant de se déplacer sur la courbe
* `axis`: nombre représentant l'axe concerné: x = 1, y = 2, z = 3.
* `p1`: premier point venant de la source de données des markers.
        Pour chaque formule, le vecteur a déjà été décomposé,
        signifant que pour la formule pour l'axe x, la variable `p1` correspond à la composante x de `p1`
* `p2`: idem que `p1`, mais pour le point 2
* `p3`: idem que `p1`, mais pour le point 3
* `p4`: idem que `p1`, mais pour le point 4
* `v1`: premier point venant de la source de données des markers sous forme vectorielle (avec tout les composants).
* `v2`: idem que `v1`, mais pour le point 2
* `v3`: idem que `v1`, mais pour le point 3
* `v4`: idem que `v1`, mais pour le point 4

### Formules

* Ligne dans l'espace 3D basée sur deux points:
    * formule: `p1 + t * vecdir(v1, v2, axis)`
    * point 1: départ de la ligne
    * point 2: fin de la ligne
    * paramètres de `t` (min / max / step): 0 / 0.1 / 0.01
    * source: [Math StackExchange](https://math.stackexchange.com/questions/404440/what-is-the-equation-for-a-3d-line)

* Cercle dans l'espace 3D basé sur deux points:
    * formule: `p1 + norm(v2-v1) * cos(t) * vecdir(v1, v2, axis) + norm(v3-v1) * sin(t) * vecdir(v1, v3, axis)`
    * point 1: centre du cercle
    * point 2: point définissant le rayon en cos et le premier vecteur directeur
    * point 3: point définissant le rayon en sin et le deuxième vecteur directeur
    * paramètres de `t` (min / max / step): 0 / 6.28 / 0.1
    * source: [Math StackExchange](https://math.stackexchange.com/questions/73237/parametric-equation-of-a-circle-in-3d-space):

Note: `vecdir` est une fonction qui calcule le vecteur de direction entre deux points, retournant la valeur dans l'axe spécifié.

### Options et actions

* `trajectory name`: nom de la trajectoire
* `parametric equation for x/y/z`: expression mathématique permettant de calculer respectivement x, y, et z (voir fonctionnement ci-dessus)
* `minimum for t`: point de départ
* `maximum for t`: point d'arrêt
* `step size for t`: taille du pas pour t, le nombre de point se calcule avec: `count = (max-min) / step`
* `rotation type`: choisir comment la rotation est définie:
    * `free`: le solver choisira la rotation la plus adaptée (aléatoire)
    * `fixed`: la rotation sera fixée pour tous les points

Lorsque les paramètres ont été réglés, vous pouvez:

* visualier et vérifier que votre trajectoire est correcte et se trouve bien dans l'espace de travail.
  Si une partie des points est rouge, cela signifie que votre trajectoire est en dehors de l'espace de travail.
  Si tel n'est pas le cas, adaptez les paramètres
* générer la trajectoire. Celle-ci apparaît ensuite dans l'interface principale comme n'importe quelle autre trajectoire.

Note: lorsque la source de la définition du volume est `QTM`, alors à chaque visualisation,
les valeurs des markers sont sauvegardées dans un fichier, pouvant par la suite est réutilisé avec l'option `capture file`.

### Légende du graphique

* points violets: positions des markers
* points bleus: position des points de la trajectoire
* points rouges: position des points de la trajectoire en dehors de l'espace de travail du robot

\newpage

# Procédures KUKA

## Réinstallation du projet sur le robot

En cas de problème persistent ou de mise à jour des règles de sécurité,
une réinstallation du projet peut être nécessaire.

Procédure:

* démarrer `Sunrise Workbench`
* clicquez droit sur le projet `KST` dans le volet gauche
    * menu `Sunrise`
    * sous-menu `Synchroniser le projet`
* lors de la synchronisation, vérifier le sens de synchronisation:
    * la coche à gauche doit être active (synchronisation vers le contrôleur)

## Connexion au robot en Bureau à distance

* Avec une application RDP:
    * IP:
        * X66: `172.31.1.147`
        * KONI: `192.168.147.2`
    * User:     `KukaUser`
    * Password: `68kuka1secpw59`
* Suite à une connexion avec succès, il vous affichera l'interface du SmartPad
    * pour en sortir et voir le bureau, utilisez les touches du clavier: `Alt+Tab`

## Arrêt/Redémarrage

* Sur le bureau, vous avez des icones pour chaque action

## Activation des règles de sécurités

Dans le menu: `Security` -> `Activation`, cliquez sur le bouton `Activate`.
Il va vous demander un mot de passe: `argus`.

## Configuration du port ethernet KONI sur le controlleur KUKA

Loggez-vous sur le KUKA en bureau à distance ou à l'aide d'un écran/clavier/souris, puis:

1. faite afficher le bureau à l'aide de `Alt-Tab` ou la touche `Windows`
2. cliquez sur l'icone verte KRC et cliquez sur `Stop KRC`
3. lancez une ligne de commande et tapez (attention à la casse):
    * `C:\KUKA\Hardware\Manager\KUKAHardwareManager.exe -assign OptionNIC -os WIN`
4. configurer en IP statique la carte réseau dans le `Centre réseau et partage`
    * IP: `192.168.147.2`, masque: `255.255.255.0`
5. Redémarrez le robot

## Axe désaxé

Il est possible qu'un axe soit en dehors de ces limites et qu'il n'est plus possible d'y revenir.

Pour remettre à 0 le mastering (calibration), il faut effectuer une procédure délicate:

1. sur le SmartPAD, passer en mode `KRF`. Il devrait être disponible si au moins un axe est en dehors de ces limites selon les règles de sécurité;
2. repositionnez l'axe manuellement dans son centre avec les boutons du SmartPAD;
3. sur votre PC, ouvrez l'application `Connexion bureau à distance` et se connecter au robot (IP: 172.31.1.147);
4. login avec le nom d'utilisateur `KukaUser` et mot de passe `68kuka1secpw59`;
5. ici, l'interface du SmartPAD devrait être affiché, ouvrez le bureau Windows en tappant les deux touches du clavier `ALT+TAB`;
6. lancez ensuite une console `cmd` et tapez `puttytel -vio`. cliquer `oui` s'il vous demande de "flush buffer";
7. tapez dans la nouvelle console `SRThinMMAdmin_setPositionReference x,0`, ou `x` est le numéro de l'axe en partant de 0.

Si tout s'est bien passé, alors quelque chose comme ça devrait être afficher:

\begin{figure}[H]
    \begin{center}
        \includegraphics[width=1.0\textwidth,scale=0.5]{robot_system_axis_reset}
    \end{center}
    \caption{Structure generale}
    \label{general_structure}
\end{figure}

Il y a plein d'erreur, mais vérifiez sur le SmartPAD si la commande à fonctionner et que votre axe est à présent sur 0.
Vous pouvez ensuite master (calibrer) les axes normalement.
