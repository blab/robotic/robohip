# Documentation

* [GitHub: KST](https://github.com/Modi1987/KST-Kuka-Sunrise-Toolbox)

# Kuka Sunrise Toolbox (KST)

## Modification

### Compatibilité

La toolbox a été codé à l'aide d'une fonction nommée [tcpip](https://www.mathworks.com/help/instrument/tcpip.html).
Celle-ci possède 2 problèmes:

1. il faut posséder la toolbox, soumis à licence;
2. la fonction est dépréciée et sera supprimée à l'avenir.

A l'aide du document [Transition your code to tcpclient interface](https://www.mathworks.com/help/instrument/transition-your-code-to-tcpclient-interface.html),
les modifications suivantes ont été faites:

* remplacer `fprintf(t)` par `writeline(t)`;
* remplacer `fgets(t)` par `char(readline(t))`;
    * `char` permet d'assurer la compatibilité avec le code existant, en terme de traitement de chaîne de caractère.
* remplacer `fclose(t)` par `clear t`;
* commenter `fopen(t)`;
* divers modifications dans le fichier `net_establishConnection.m`:
    * remplacer `tcpip` par [tcpclient](https://www.mathworks.com/help/instrument/tcpclient.html);
    * passage de `warning` à `error` pour les exceptions;
    * faire afficher le message d'erreur en cas d'exception, auparavant ignoré, rendant le débuggage compliqué;
* dans le fichier `KST.m`, fonction `net_establishConnection`:
    * suppression du code referent à `attachToolToFlange`, car faire cela 2 fois de suite cause le serveur à bloquer (freeze)
* ajout de la possibilité d'exécuter des trajectoires dans l'espace cartésien et des joints:
    * ajout des commandes d'exécution dans `MatlabToolboxServer.java` et `PTPMotionClass.java`;
    * ajout des commandes de vitesses cartésien dans `MatlabToolboxServer.java`;
    * ajout des fichiers matlab correspondants `movePTPTrajectoryJoint.m` et `movePTPTrajectoryCartesianLineEefRelBase.m` dans le dossier `kst`;
    * ajout des fonctions correspondantes dans la classe `KST`.

Note: parfois, la variable `t` est nommée `t_Kuka`.

### Description robot et Media-Flange

Par défaut, la toolbox prend en charge la version 7 KG de notre robot et notre media-flange n'est pas décrit.

Donc, dans le fichier `KST.m`, la media-flange `Medien_Flansch_Touch_elektrisch` a été rajoutée avec comme valeur `0.061`.
Ceci est la dimension de la flange en mètre, pouvant être trouvé dans le document [LBR iiwa - Option Media Flange](LBR iiwa - Option Media Flange).
Note: dans le document, il s'agit de millimètre.

Par la suite, tous les fichiers tutoriel de la classe, préfixés de `KSTclass_Tutorial_*`, ont été modifiés,
afin de remplacer:

```
arg1=KST.LBR7R800;
arg2=KST.Medien_Flansch_elektrisch; % ou autre
```

par

```
arg1=KST.LBR14R820;
arg2=KST.Medien_Flansch_Touch_elektrisch;
```

## Performance réseau

Il existe un test de performance réseau afin de déterminer à quelle fréquence maximum peut-on fonctionner.
Ce fichier se nomme `KSTclass_Tutorial_plotNetHealth.m`.
Ce test nous donne le temps en millisecondes minimum entre chaque commande.

Sans appliquer de quelconques paramètres, celui-ci nous donne environ 30 millisecondes et donc environ 33 herz.
Il y a des piques allant jusqu'à environs 65 millisecondes.

![Performance réseau - latence](PlotNetHealth.png "Performance réseau - latence")

Le document [Improving Network Performance For KST](KST-Network-Perf.pdf) donne différentes manières d'améliorer ce delai.

# Installation PROfiNet

## Mise en route WorkVisual 3.0

* Ficher -> Fermer
* Outils -> Gestion des packs d'options -> select File -> `KUKA CD - WorkBench 1.10.0.8` -> `WorkVisual AddOn/Sunrise.kop`
* Outils -> Gestion des catalogues DTM -> Chercher les DTM installés
* Outils -> Gestion des catalogues DTM -> ">>"
* Structure de projet -> Click droit `Steuerung 1 (KRC4 8.3.0)` -> Définir actif
